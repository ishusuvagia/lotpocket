
#import "ObjectEntity.h"
#import "DealerModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DealerEntity : ObjectEntity

@property (nonatomic, strong) NSString *dealerId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *sellerName;
@property (nonatomic, strong) NSString *inventoryUrl;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, strong) NSString *dataSource;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *dealerType;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *zipCode;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *sellerPhone;
@property (nonatomic, assign) CGFloat distance;

+ (nonnull NSPredicate *)dealerIdPredicate:(nullable NSString *)dealerId;

@end

RLM_ARRAY_TYPE(DealerEntity)

NS_ASSUME_NONNULL_END
