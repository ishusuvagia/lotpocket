
#import "ObjectEntity.h"

@implementation ObjectEntity

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSString *)primaryKey
{
    return @"uniqueId";
}

+ (ObjectEntity *)objectInRealm:(RLMRealm *)realm withPredicate:(NSPredicate *)predicate
{
    return [[self objectsInRealm:realm withPredicate:predicate] firstObject];
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithObjectModel:(id)objectModel
{
    if ((self = [super init])) {
        _uniqueId = [NSUUID UUID].UUIDString;
    }
    return self;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)updateFromObjectModel:(id)objectModel
{
    // This will be implemented at subclass
}

- (NSDictionary *)toDictionary
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    RLMObjectSchema *schema = self.objectSchema;
    for (RLMProperty *property in schema.properties) {
        if ([self[property.name] isKindOfClass:[RLMArray class]]) {
            NSMutableArray *arrayObjects = [NSMutableArray new];
            RLMArray *currentArray = self[property.name];
            for (NSUInteger i = 0; i < currentArray.count; i++) {
                ObjectEntity *currentElement = (ObjectEntity *) currentArray[(NSUInteger) i];
                [arrayObjects addObject:[currentElement toDictionary]];
            }
            result[property.name] = arrayObjects;
        } else if ([self[property.name] isKindOfClass:[ObjectEntity class]]) {
            ObjectEntity *currentElement = (ObjectEntity *) self[property.name];
            result[property.name] = [currentElement toDictionary];
        } else {
            result[property.name] = self[property.name];
        }
    }
    return result;
}

@end
