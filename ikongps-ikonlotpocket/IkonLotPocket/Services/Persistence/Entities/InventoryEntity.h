
#import "ObjectEntity.h"
#import "FinancingOptionEntity.h"
#import "DealerEntity.h"
#import "InventoryModel.h"
#import "DeviceModel.h"
#import "RealmType.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryEntity : ObjectEntity

@property (nonatomic, strong) NSString *inventoryId;
@property (nonatomic, strong) NSString *vin;
@property (nonatomic, strong) NSString *heading;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) NSInteger miles;
@property (nonatomic, assign) NSInteger msrp;
@property (nonatomic, strong) NSString *dataSource;
@property (nonatomic, assign) NSInteger isCertified;
@property (nonatomic, strong) NSString *vdpUrl;
@property (nonatomic, assign) BOOL carfaxOneOwner;
@property (nonatomic, assign) BOOL carfaxCleanTitle;
@property (nonatomic, strong) NSString *exteriorColor;
@property (nonatomic, strong) NSString *interiorColor;
@property (nonatomic, assign) NSInteger daysOnMarket;
@property (nonatomic, assign) NSInteger last180DaysOnMarket;
@property (nonatomic, assign) NSInteger daysOnMarketActive;
@property (nonatomic, strong) NSString *sellerType;
@property (nonatomic, strong) NSString *inventoryType;
@property (nonatomic, strong) NSString *stockNumber;
@property (nonatomic, assign) NSTimeInterval lastSeenAt;
@property (nonatomic, strong) NSDate *lastSeenAtDate;
@property (nonatomic, assign) NSTimeInterval scrapedAt;
@property (nonatomic, strong) NSDate *scrapedAtDate;
@property (nonatomic, assign) NSTimeInterval firstSeenAt;
@property (nonatomic, strong) NSDate *firstSeenAtDate;
@property (nonatomic, assign) NSInteger referencePrice;
@property (nonatomic, assign) NSTimeInterval referencePriceDt;
@property (nonatomic, assign) NSInteger referenceMiles;
@property (nonatomic, assign) NSTimeInterval referenceMilesDt;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, assign) NSInteger year;
@property (nonatomic, strong) NSString *make;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *trim;
@property (nonatomic, strong) NSString *bodyType;
@property (nonatomic, strong) NSString *vehicleType;
@property (nonatomic, strong) NSString *transmission;
@property (nonatomic, strong) NSString *driveTrain;
@property (nonatomic, strong) NSString *fuelType;
@property (nonatomic, strong) NSString *engine;
@property (nonatomic, assign) CGFloat engineSize;
@property (nonatomic, strong) NSString *engineBlock;
@property (nonatomic, assign) NSInteger doors;
@property (nonatomic, assign) NSInteger cylinders;
@property (nonatomic, strong) NSString *madeIn;
@property (nonatomic, strong) NSString *steeringType;
@property (nonatomic, strong) NSString *antiBrakeSystem;
@property (nonatomic, strong) NSString *tankSize;
@property (nonatomic, strong) NSString *overallHeight;
@property (nonatomic, strong) NSString *overallLength;
@property (nonatomic, strong) NSString *overallWidth;
@property (nonatomic, strong) NSString *standardSeating;
@property (nonatomic, strong) NSString *highwayMiles;
@property (nonatomic, strong) NSString *cityMiles;
@property (nonatomic, strong) NSString *trimR;
@property (nonatomic, strong) RLMArray<FinancingOptionEntity *><FinancingOptionEntity> *financingOptions;
@property (nonatomic, strong) RLMArray<RealmString *><RealmString> *photoLinks;
@property (nonatomic, strong) RLMArray<RealmString *><RealmString> *options;
@property (nonatomic, strong) RLMArray<RealmString *><RealmString> *features;
@property (nonatomic, strong) RLMArray<RealmString *><RealmString> *exteriorFeatures;
@property (nonatomic, strong) RLMArray<RealmString *><RealmString> *standardFeatures;
@property (nonatomic, strong) RLMArray<RealmString *><RealmString> *interiorFeatures;
@property (nonatomic, strong) RLMArray<RealmString *><RealmString> *safetyFeatures;
@property (nonatomic, strong) NSString *sellerComments;
@property (nonatomic, strong) DealerEntity *dealer;

+ (nonnull NSPredicate *)inventoryIdPredicate:(nullable NSString *)inventoryId;
+ (nonnull NSPredicate *)dealerIdPredicate:(nullable NSString *)dealerId;
+ (nonnull NSPredicate *)vinPredicate:(nullable NSString *)vin;
+ (nonnull NSPredicate *)vinsPredicate:(nullable NSArray *)vins;
+ (nonnull NSPredicate *)headingSearchPredicate:(nullable NSString *)searchString;
- (nonnull NSString *)getInventoryTitle;
- (nonnull NSString *)getDetailedInventoryTitle;
- (nonnull NSString *)getConditionStatus;
- (nonnull NSArray *)getPhotoLinkUrls;

@end

RLM_ARRAY_TYPE(InventoryEntity)

NS_ASSUME_NONNULL_END
