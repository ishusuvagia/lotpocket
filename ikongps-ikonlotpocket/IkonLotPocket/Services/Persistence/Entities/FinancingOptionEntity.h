
#import "ObjectEntity.h"
#import "FinancingOptionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FinancingOptionEntity : ObjectEntity

@property (nonatomic, assign) NSInteger loanTerm;
@property (nonatomic, assign) CGFloat loanApr;
@property (nonatomic, assign) CGFloat downPaymentPercentage;

+ (nonnull NSPredicate *)financingOptionPredicate:(nullable FinancingOptionModel *)financingOptionModel;

@end

RLM_ARRAY_TYPE(FinancingOptionEntity)

NS_ASSUME_NONNULL_END
