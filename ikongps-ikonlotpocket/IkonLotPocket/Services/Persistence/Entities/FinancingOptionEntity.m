
#import "FinancingOptionEntity.h"

@implementation FinancingOptionEntity

//--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

+ (NSPredicate *)financingOptionPredicate:(FinancingOptionModel *)financingOptionModel;
{
    return [NSPredicate predicateWithFormat:@"loanTerm == %ld AND loanApr == %.2f AND downPaymentPercentage == %.2f",
            financingOptionModel.loanTerm, financingOptionModel.loanApr, financingOptionModel.downPaymentPercentage];
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithObjectModel:(FinancingOptionModel *)financingOptionModel
{
    if ((self = [super initWithObjectModel:financingOptionModel])) {
        [self updateFromObjectModel:financingOptionModel];
    }
    return self;
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (void)updateFromObjectModel:(FinancingOptionModel *)financingOptionModel
{
    self.loanTerm = financingOptionModel.loanTerm;
    self.loanApr = financingOptionModel.loanApr;
    self.downPaymentPercentage = financingOptionModel.downPaymentPercentage;
}

@end
