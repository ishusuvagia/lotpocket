
#import "DealerEntity.h"

@implementation DealerEntity

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSPredicate *)dealerIdPredicate:(NSString *)dealerId
{
    return [NSPredicate predicateWithFormat:@"dealerId == %@", dealerId];
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithObjectModel:(ObjectModel *)model
{
    if ((self = [super initWithObjectModel:model])) {
        if ([model isKindOfClass:DealerModel.class]) {
            DealerModel *dealerModel = (DealerModel *) model;
            self.dealerId = dealerModel.dealerId;
        } else if ([model isKindOfClass:InventoryDealerModel.class]) {
            InventoryDealerModel *dealerModel = (InventoryDealerModel *) model;
            self.dealerId = [@(dealerModel.dealerId) stringValue];
        }
        [self updateFromObjectModel:model];
    }
    return self;
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (void)updateFromObjectModel:(ObjectModel *)model
{
    if ([model isKindOfClass:DealerModel.class]) {
        DealerModel *dealerModel = (DealerModel *) model;
        self.sellerName = dealerModel.sellerName;
        self.inventoryUrl = dealerModel.inventoryUrl;
        self.dataSource = dealerModel.dataSource;
        self.status = dealerModel.status;
        self.dealerType = dealerModel.dealerType;
        self.street = dealerModel.street;
        self.city = dealerModel.city;
        self.state = dealerModel.state;
        self.zipCode = dealerModel.zipCode;
        self.country = dealerModel.country;
        self.latitude = dealerModel.latitude;
        self.longitude = dealerModel.longitude;
        self.sellerPhone = dealerModel.sellerPhone;
        self.distance = dealerModel.distance;
    } else if ([model isKindOfClass:InventoryDealerModel.class]) {
        InventoryDealerModel *dealerModel = (InventoryDealerModel *) model;
        self.website = dealerModel.website;
        self.name = dealerModel.name;
        self.dealerType = dealerModel.dealerType;
        self.street = dealerModel.street;
        self.city = dealerModel.city;
        self.state = dealerModel.state;
        self.zipCode = dealerModel.zipCode;
        self.country = dealerModel.country;
        self.latitude = dealerModel.latitude;
        self.longitude = dealerModel.longitude;
    }
}

@end
