
#import "ObjectEntity.h"
#import "DeviceModel.h"
#import "InventoryEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeviceEntity : ObjectEntity

@property (nonatomic, assign) NSInteger deviceId;
@property (nonatomic, assign) NSInteger fleetId;
@property (nonatomic, assign) NSInteger deviceType;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *batteryLevel;
@property (nonatomic, strong) NSDate *firstReport;
@property (nonatomic, strong) NSDate *lastReport;
@property (nonatomic, strong) NSDate *lastReportUtc;
@property (nonatomic, strong) NSString *heading;
@property (nonatomic, strong) NSString *imei;
@property (nonatomic, strong) NSString *inCommunication;
@property (nonatomic, assign) BOOL isInstalled;
@property (nonatomic, strong) NSNumber<RLMFloat> *latitude;
@property (nonatomic, strong) NSNumber<RLMFloat> *longitude;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *paidUntil;
@property (nonatomic, assign) NSInteger rowNumber;
@property (nonatomic, strong) NSString *simStatus;
@property (nonatomic, strong) NSString *serialNumber;
@property (nonatomic, assign) NSInteger speed;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *stockNumber;
@property (nonatomic, strong) NSString *vin;
@property (nonatomic, strong) NSString *exteriorColor;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *make;
@property (nonatomic, strong) NSString *year;
@property (nonatomic, strong) InventoryEntity *vehicle;

+ (nonnull NSPredicate *)deviceIdPredicate:(NSInteger)deviceId;
+ (nonnull NSPredicate *)fleetIdPredicate:(NSInteger)fleetId;
+ (nonnull NSPredicate *)fleetIdPredicate:(NSInteger)fleetId inventoryType:(nullable NSString *)inventoryType;
+ (nonnull NSPredicate *)deviceTypePredicate:(NSInteger)deviceType;
+ (nonnull NSPredicate *)imeiPredicate:(nullable NSString *)imei;
+ (nonnull NSPredicate *)serialNumberPredicate:(nullable NSString *)serialNumber;
+ (nonnull NSPredicate *)vinPredicate:(nullable NSString *)vin;
- (nonnull NSString *)getVehicleInventoryTitle;
- (BOOL)matchWithSearchText:(nullable NSString *)searchText;

@end

RLM_ARRAY_TYPE(DeviceEntity)

NS_ASSUME_NONNULL_END
