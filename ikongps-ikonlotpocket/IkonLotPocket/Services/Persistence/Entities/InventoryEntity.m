
#import "InventoryEntity.h"

@implementation InventoryEntity

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSPredicate *)inventoryIdPredicate:(NSString *)inventoryId
{
    
    return [NSPredicate predicateWithFormat:@"inventoryId == %@", inventoryId];
}

+ (NSPredicate *)dealerIdPredicate:(NSString *)dealerId
{
    NSLog(@"dealer id : %@",dealerId);
    return [NSPredicate predicateWithFormat:@"dealer.dealerId == %@", dealerId];
}

+ (NSPredicate *)vinPredicate:(NSString *)vin
{
    return [NSPredicate predicateWithFormat:@"vin == %@", vin];
}

+ (NSPredicate *)vinsPredicate:(NSArray *)vins
{
    return [NSPredicate predicateWithFormat:@"vin IN %@", vins];
}

+ (NSPredicate *)headingSearchPredicate:(NSString *)searchString
{
    return [NSPredicate predicateWithFormat:@"heading CONTAINS[cd] %@", searchString];
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithObjectModel:(InventoryModel *)inventoryModel
{
    if ((self = [super initWithObjectModel:inventoryModel])) {
        self.inventoryId = inventoryModel.inventoryId;
        [self updateFromObjectModel:inventoryModel];
    }
    return self;
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (void)updateFromObjectModel:(InventoryModel *)inventoryModel
{
    self.vin = inventoryModel.vin;
    self.heading = inventoryModel.heading;
    self.price = inventoryModel.price;
    self.miles = inventoryModel.miles;
    self.msrp = inventoryModel.msrp;
    self.dataSource = inventoryModel.dataSource;
    self.isCertified = inventoryModel.isCertified;
    self.vdpUrl = inventoryModel.vdpUrl;
    self.carfaxOneOwner = inventoryModel.carfaxOneOwner;
    self.carfaxCleanTitle = inventoryModel.carfaxCleanTitle;
    self.exteriorColor = inventoryModel.exteriorColor;
    self.interiorColor = inventoryModel.interiorColor;
    self.daysOnMarket = inventoryModel.daysOnMarket;
    self.last180DaysOnMarket = inventoryModel.last180DaysOnMarket;
    self.daysOnMarketActive = inventoryModel.daysOnMarketActive;
    self.sellerType = inventoryModel.sellerType;
    self.inventoryType = inventoryModel.inventoryType;
    self.stockNumber = inventoryModel.stockNumber;
    self.lastSeenAt = inventoryModel.lastSeenAt;
    self.lastSeenAtDate = inventoryModel.lastSeenAtDate;
    self.scrapedAt = inventoryModel.scrapedAt;
    self.scrapedAtDate = inventoryModel.scrapedAtDate;
    self.firstSeenAt = inventoryModel.firstSeenAt;
    self.firstSeenAtDate = inventoryModel.firstSeenAtDate;
    self.referencePrice = inventoryModel.referencePrice;
    self.referencePriceDt = inventoryModel.referencePriceDt;
    self.referenceMiles = inventoryModel.referenceMiles;
    self.referenceMilesDt = inventoryModel.referenceMilesDt;
    self.source = inventoryModel.source;
    
    for (FinancingOptionModel *model in inventoryModel.financingOptions) {
        FinancingOptionEntity *entity = [[FinancingOptionEntity alloc] initWithObjectModel:model];
        [self.financingOptions addObject:entity];
    }
    
    if (inventoryModel.media) {
        MediaModel *mediaModel = inventoryModel.media;
        for (NSString *photoLink in mediaModel.photoLinks) {
            [self.photoLinks addObject:[[RealmString alloc] initWithValue:@[photoLink]]];
        }
    }
    
    if (inventoryModel.extra) {
        ExtraModel *extraModel = inventoryModel.extra;
        for (NSString *option in extraModel.options) {
            [self.options addObject:[[RealmString alloc] initWithValue:@[option]]];
        }
        for (NSString *feature in extraModel.features) {
            [self.features addObject:[[RealmString alloc] initWithValue:@[feature]]];
        }
        for (NSString *exteriorFeature in extraModel.exteriorFeatures) {
             [self.exteriorFeatures addObject:[[RealmString alloc] initWithValue:@[exteriorFeature]]];
        }
        for (NSString *standardFeature in extraModel.standardFeatures) {
            [self.standardFeatures addObject:[[RealmString alloc] initWithValue:@[standardFeature]]];
        }
        for (NSString *safetyFeature in extraModel.safetyFeatures) {
            [self.safetyFeatures addObject:[[RealmString alloc] initWithValue:@[safetyFeature]]];
        }
        self.sellerComments = extraModel.sellerComments;
    }
    
    if (inventoryModel.build) {
        BuildModel *buildModel = inventoryModel.build;
        self.year = buildModel.year;
        self.make = buildModel.make;
        self.model = buildModel.model;
        self.trim = buildModel.trim;
        self.bodyType = buildModel.bodyType;
        self.vehicleType = buildModel.vehicleType;
        self.transmission = buildModel.transmission;
        self.driveTrain = buildModel.driveTrain;
        self.fuelType = buildModel.fuelType;
        self.engine = buildModel.engine;
        self.engineSize = buildModel.engineSize;
        self.engineBlock = buildModel.engineBlock;
        self.doors = buildModel.doors;
        self.cylinders = buildModel.cylinders;
        self.madeIn = buildModel.madeIn;
        self.steeringType = buildModel.steeringType;
        self.antiBrakeSystem = buildModel.antiBrakeSystem;
        self.tankSize = buildModel.tankSize;
        self.overallHeight = buildModel.overallHeight;
        self.overallLength = buildModel.overallLength;
        self.overallWidth = buildModel.overallWidth;
        self.standardSeating = buildModel.standardSeating;
        self.highwayMiles = buildModel.highwayMiles;
        self.cityMiles = buildModel.cityMiles;
        self.trimR = buildModel.trimR;
    }
}

- (NSString *)getInventoryTitle
{
    NSString *year = self.year > 0 ? [@(self.year) stringValue] : kEmptyString;
    NSString *make = self.make ?: kEmptyString;
    NSString *model = self.model ?: kEmptyString;
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@", year, make, model];
    return title;
}

- (NSString *)getDetailedInventoryTitle
{
    NSString *year = self.year > 0 ? [@(self.year) stringValue] : kEmptyString;
    NSString *make = self.make ?: kEmptyString;
    NSString *model = self.model ?: kEmptyString;
    NSString *body = self.bodyType ?: kEmptyString;
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@ %@", year, make, model, body];
    return title;
}

- (NSString *)getConditionStatus
{
    if (self.isCertified) {
        return @"certified";
    } else {
        return self.inventoryType;
    }
}

- (NSArray *)getPhotoLinkUrls
{
    NSMutableArray *photoLinks = [NSMutableArray array];
    for (RealmString *photoLink in self.photoLinks) {
        if (photoLink.stringValue) [photoLinks addObject:photoLink.stringValue];
    }
    return photoLinks;
}

@end
