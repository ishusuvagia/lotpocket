
#import <Foundation/Foundation.h>
#import "PersistenceContainer.h"
#import "DataService.h"
#import "ObjectEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersistenceService : NSObject

@property (nonnull, nonatomic, copy, readonly) RLMRealm *localDb;
@property (nullable, nonatomic, strong) ObjectEntity *entity;

+ (void)configLocalDb;
- (void)handleException:(nullable NSException *)exception responder:(nullable id<RestResponseDelegate>)responder;
- (void)handleException:(nullable NSException *)exception callback:(nullable IdResultBlock)callback;
- (void)handleException:(nullable NSException *)exception pagedCallback:(nullable IdPagedResultBlock)callback;
- (nonnull NSMutableArray *)pagedResultsForResults:(nonnull RLMResults *)results start:(NSInteger)start pageSize:(NSInteger)pageSize;
- (nonnull NSMutableArray *)pagedResultsForResults:(nonnull RLMResults *)results page:(NSInteger)page pageSize:(NSInteger)pageSize;

@end

NS_ASSUME_NONNULL_END
