
#import "PersistenceService.h"
#import "AuthenticationManager.h"

static NSString *const __nonnull kDatabaseFolderName = @"LocalDb";

@implementation PersistenceService

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (void)configLocalDb
{
    // Get the user name
    NSString *username = @"ikongps";

#ifdef UNITTESTS
    NSString *fileName = [username stringByAppendingString:@"_tests"];
#else
    NSString *fileName = username;
#endif

    NSURL *path = [[[Utils getLocalDirectory:kDatabaseFolderName] URLByAppendingPathComponent:fileName
                                                                                  isDirectory:NO] URLByAppendingPathExtension:@"realm"];

    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];

    // Configuration URL
    config.fileURL = path;

    // Delete old database if migration needed
    config.deleteRealmIfMigrationNeeded = YES;

    // Set the new schema version. This must be greater than the previously used
    // version (if you've never set a schema version before, the version is 0).
    config.schemaVersion = DATABASE_SCHEMA_VERSION;

    // Set the block which will be called automatically when opening a Realm with a
    // schema version lower than the one set above
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < DATABASE_SCHEMA_VERSION) {
            // Nothing to do!
            // Realm will automatically detect new properties and removed properties
            // And will update the schema on disk automatically
        }
    };

    [RLMRealmConfiguration setDefaultConfiguration:config];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (RLMRealm *)localDb
{
    // Get the user name
    NSString *username = @"ikongps";

#ifdef UNITTESTS
    NSString *fileName = [username stringByAppendingString:@"_tests"];
#else
    NSString *fileName = username;
#endif

    NSURL *path = [[[Utils getLocalDirectory:kDatabaseFolderName] URLByAppendingPathComponent:fileName
                                                                                  isDirectory:NO] URLByAppendingPathExtension:@"realm"];

    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.fileURL = path;
    [RLMRealmConfiguration setDefaultConfiguration:config];

    RLMRealm *realm = [RLMRealm realmWithURL:path] ?: [RLMRealm defaultRealm];
    realm.autorefresh = YES;

    return realm;
}

- (void)handleException:(NSException *)exception responder:(id<RestResponseDelegate>)responder
{
    NSLog(@"%s: Exception occurred: %@", __func__, exception.description);
    [self.localDb cancelWriteTransaction];
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([responder respondsToSelector:@selector(errorHandler:)]) {
            [responder errorHandler:[ErrorsUtil errorWithCode:kErrorPersistenceException message:exception.reason]];
        }
    });
}

- (void)handleException:(NSException *)exception callback:(IdResultBlock)callback
{
    NSLog(@"%s: Exception occurred: %@", __func__, exception.description);
    [self.localDb cancelWriteTransaction];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (callback) callback(nil, [ErrorsUtil errorWithCode:kErrorPersistenceException message:exception.reason]);
    });
}

- (void)handleException:(NSException *)exception pagedCallback:(IdPagedResultBlock)callback
{
    NSLog(@"%s: Exception occurred: %@", __func__, exception.description);
    [self.localDb cancelWriteTransaction];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (callback) callback(nil, 0, [ErrorsUtil errorWithCode:kErrorPersistenceException message:exception.reason]);
    });
}


- (NSMutableArray *)pagedResultsForResults:(RLMResults *)results start:(NSInteger)start pageSize:(NSInteger)pageSize
{
    NSMutableArray *pagedResults = [NSMutableArray array];
    NSInteger count = (start + pageSize) > results.count ? results.count : (start + pageSize);
    for (NSInteger idx = start; idx < count; idx++) {
        RLMObject *object = [results objectAtIndex:idx];
        if (object) [pagedResults addObject:object];
    }
    return pagedResults;
}

- (NSMutableArray *)pagedResultsForResults:(RLMResults *)results page:(NSInteger)page pageSize:(NSInteger)pageSize
{
    NSInteger start = page == 0 ? page : page * pageSize;
    return [self pagedResultsForResults:results start:start pageSize:pageSize];
}

@end
