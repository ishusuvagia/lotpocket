
#import "PersistenceService.h"
#import "ObjectEntity.h"
#import "DealerEntity.h"
#import "InventoryEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryService : PersistenceService

- (void)getDealerById:(nullable NSString *)dealerId isRefreshing:(BOOL)isRefreshing responder:(nullable id<RestResponseDelegate>)responder;
- (void)getDealerById:(nullable NSString *)dealerId isRefreshing:(BOOL)isRefreshing callback:(nullable IdResultBlock)callback;
- (void)searchInventory:(nullable NSString *)searchText responder:(nullable id<RestResponseDelegate>)responder;
- (void)searchInventory:(nullable NSString *)searchText callback:(nullable IdPagedResultBlock)callback;
- (void)getInventoryByDealerId:(nullable NSString *)dealerId
                         start:(NSInteger)start pageSize:(NSInteger)pageSize
                      sortType:(nullable NSString *)sortType filterDict:(nullable NSDictionary *)filterDict
                  isRefreshing:(BOOL)isRefreshing responder:(nullable id<RestResponseDelegate>)responder;
- (void)getInventoryByDealerId:(nullable NSString *)dealerId
                         start:(NSInteger)start pageSize:(NSInteger)pageSize
                      sortType:(nullable NSString *)sortType filterDict:(nullable NSDictionary *)filterDict
                      callback:(nullable IdPagedResultBlock)callback;
- (void)getInventoryListByVins:(nullable NSArray *)vins isRefreshing:(BOOL)isRefreshing responder:(nullable id <RestResponseDelegate>)responder;
- (void)getInventoryListByVins:(nullable NSArray *)vins callback:(nullable IdPagedResultBlock)callback;
- (void)getInventoryById:(nullable NSString *)inventoryId isRefreshing:(BOOL)isRefreshing responder:(nullable id <RestResponseDelegate>)responder;
- (void)getInventoryById:(nullable NSString *)inventoryId callback:(nullable IdResultBlock)callback;
- (void)getInventoryByVin:(nullable NSString *)vin callback:(nullable IdResultBlock)callback;

@end

NS_ASSUME_NONNULL_END
