
#import <Foundation/Foundation.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

#define MODULESMANAGER [ModulesManager sharedManager]

@interface ModulesManager : NSObject

+ (nonnull instancetype)sharedManager;
- (void)initModules:(nullable NSArray *)authRoleAccess;
- (nullable NSArray *)getAllModules;
- (nullable NSArray *)getAllowedModules;
- (void)clear;
- (nullable id <PushDelegate>)getModuleForPush:(nullable NSString *)type;

@end

NS_ASSUME_NONNULL_END
