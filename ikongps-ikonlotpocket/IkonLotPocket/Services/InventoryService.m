
#import "InventoryService.h"

@implementation InventoryService

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (RLMResults *)_getInventoryResultsForDealerId:(NSString *)dealerId sortType:(NSString *)sortType
{
    NSPredicate *predicate = [InventoryEntity dealerIdPredicate:dealerId];
    RLMResults *results = [InventoryEntity objectsInRealm:self.localDb withPredicate:predicate];

    if ([sortType isEqualToString:kExternal_SortBy_Model]) {
        [results sortedResultsUsingKeyPath:@"build.model" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_HighestPrice]) {
        [results sortedResultsUsingKeyPath:@"msrp" ascending:NO];
    } else if ([sortType isEqualToString:kExternal_SortBy_LowestPrice]) {
        [results sortedResultsUsingKeyPath:@"msrp" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_NewestYear]) {
        [results sortedResultsUsingKeyPath:@"build.year" ascending:NO];
    } else if ([sortType isEqualToString:kExternal_SortBy_OldestYear]) {
        [results sortedResultsUsingKeyPath:@"build.year" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_LowestMileage]) {
        [results sortedResultsUsingKeyPath:@"miles" ascending:YES];
    } else if ([sortType isEqualToString:kExternal_SortBy_HighestMileage]) {
        [results sortedResultsUsingKeyPath:@"miles" ascending:NO];
    }

    return results;
}

- (InventoryEntity *)_processInventory:(InventoryModel *)model
{
    NSPredicate *inventoryIdPredicate = [InventoryEntity inventoryIdPredicate:model.inventoryId];
    InventoryEntity *inventoryEntity = (InventoryEntity *) [InventoryEntity objectInRealm:self.localDb withPredicate:inventoryIdPredicate];

    NSPredicate *dealerIdPredicate = [DealerEntity dealerIdPredicate:[@(model.dealer.dealerId) stringValue]];
    DealerEntity *dealerEntity = (DealerEntity *) [DealerEntity objectInRealm:self.localDb withPredicate:dealerIdPredicate];

    if (inventoryEntity) {
        for (FinancingOptionEntity *financingOption in inventoryEntity.financingOptions) {
            [self.localDb deleteObject:financingOption];
        }
        for (RealmString *photoLink in inventoryEntity.photoLinks) {
            [self.localDb deleteObject:photoLink];
        }
        for (RealmString *option in inventoryEntity.options) {
            [self.localDb deleteObject:option];
        }
        for (RealmString *feature in inventoryEntity.features) {
            [self.localDb deleteObject:feature];
        }
        for (RealmString *exteriorFeature in inventoryEntity.exteriorFeatures) {
            [self.localDb deleteObject:exteriorFeature];
        }
        for (RealmString *standardFeature in inventoryEntity.standardFeatures) {
            [self.localDb deleteObject:standardFeature];
        }
        for (RealmString *interiorFeature in inventoryEntity.interiorFeatures) {
            [self.localDb deleteObject:interiorFeature];
        }
        for (RealmString *safetyFeature in inventoryEntity.safetyFeatures) {
            [self.localDb deleteObject:safetyFeature];
        }
        [inventoryEntity updateFromObjectModel:model];
    } else {
        inventoryEntity = [[InventoryEntity alloc] initWithObjectModel:model];
    }

    if (!dealerEntity) {
        dealerEntity = [[DealerEntity alloc] initWithObjectModel:model.dealer];
    }
    inventoryEntity.dealer = dealerEntity;

    return inventoryEntity;
}

///--------------------------------------
#pragma mark - Dealer
///--------------------------------------

- (void)getDealerById:(NSString *)dealerId isRefreshing:(BOOL)isRefreshing responder:(id <RestResponseDelegate>)responder
{
    NSPredicate *predicate = [DealerEntity dealerIdPredicate:dealerId];

    if (!isRefreshing) {
        DealerEntity *entity = (DealerEntity *) [DealerEntity objectInRealm:self.localDb withPredicate:predicate];
        if (entity && [responder respondsToSelector:@selector(responseHandler:)]) {
            [responder responseHandler:entity];
        }
    }

    [DataService getDealerById:dealerId callback:^(DealerModel *model, NSError *__autoreleasing error) {
        if (!error && model.isValid) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self.localDb beginWriteTransaction];
                        DealerEntity *entity = (DealerEntity *) [DealerEntity objectInRealm:self.localDb withPredicate:predicate];
                        if (entity) [entity updateFromObjectModel:model];
                        else entity = [[DealerEntity alloc] initWithObjectModel:model];
                        [self.localDb addOrUpdateObject:entity];
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            DealerEntity *result = (DealerEntity *) [DealerEntity objectInRealm:self.localDb withPredicate:predicate];
                            if ([responder respondsToSelector:@selector(responseHandler:)]) {
                                [responder responseHandler:result];
                            }
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception responder:responder];
                    }
                }
            });
        } else {
            if ([responder respondsToSelector:@selector(errorHandler:)]) {
                [responder errorHandler:error];
            }
        }
    }];
}

- (void)getDealerById:(NSString *)dealerId isRefreshing:(BOOL)isRefreshing callback:(IdResultBlock)callback
{
    NSPredicate *predicate = [DealerEntity dealerIdPredicate:dealerId];

    [DataService getDealerById:dealerId callback:^(DealerModel *model, NSError *__autoreleasing error) {
        if (!error && model.isValid) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    [self.localDb beginWriteTransaction];
                    @try {
                        DealerEntity *entity = (DealerEntity *) [DealerEntity objectInRealm:self.localDb withPredicate:predicate];
                        if (entity) [entity updateFromObjectModel:model];
                        else entity = [[DealerEntity alloc] initWithObjectModel:model];
                        [self.localDb addOrUpdateObject:entity];
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            DealerEntity *result = (DealerEntity *) [DealerEntity objectInRealm:self.localDb withPredicate:predicate];
                            if (callback) callback(result, nil);
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception callback:callback];
                    }
                }
            });
        } else {
            if (callback) callback(nil, error);
        }
    }];
}

///--------------------------------------
#pragma mark - Inventory
///--------------------------------------

- (void)searchInventory:(NSString *)searchText responder:(id <RestResponseDelegate>)responder
{
    RLMResults *results = [Utils stringIsNullOrEmpty:searchText]
            ? [InventoryEntity allObjectsInRealm:self.localDb]
            : [InventoryEntity objectsWithPredicate:[InventoryEntity headingSearchPredicate:searchText]];
    NSMutableArray *pagedResults = [self pagedResultsForResults:results start:0 pageSize:results.count];
    if ([responder respondsToSelector:@selector(responseHandler:total:)]) {
        [responder responseHandler:pagedResults total:results.count];
    }
}

- (void)searchInventory:(NSString *)searchText callback:(IdPagedResultBlock)callback
{
    RLMResults *results = [Utils stringIsNullOrEmpty:searchText]
            ? [InventoryEntity allObjectsInRealm:self.localDb]
            : [InventoryEntity objectsWithPredicate:[InventoryEntity headingSearchPredicate:searchText]];
    NSMutableArray *pagedResults = [self pagedResultsForResults:results start:0 pageSize:results.count];
    if (callback) callback(pagedResults, results.count, nil);
}

- (void)getInventoryByDealerId:(NSString *)dealerId
                         start:(NSInteger)start pageSize:(NSInteger)pageSize
                      sortType:(NSString *)sortType filterDict:(NSDictionary *)filterDict
                  isRefreshing:(BOOL)isRefreshing responder:(id <RestResponseDelegate>)responder
{
    if (!isRefreshing) {
        RLMResults *results = [self _getInventoryResultsForDealerId:dealerId sortType:sortType];
        NSMutableArray *pagedResults = [self pagedResultsForResults:results start:start pageSize:pageSize];
        if (pagedResults.count > 0 && [responder respondsToSelector:@selector(responseHandler:total:)]) {
            [responder responseHandler:pagedResults total:results.count];
        }
    }

    [DataService getInventoryByDealerId:dealerId start:start pageSize:pageSize sortType:sortType filterDict:filterDict callback:^(InventoryListModel *listModel, NSError *error) {
        if (listModel && listModel.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self.localDb beginWriteTransaction];
                        for (InventoryModel *model in listModel.listings) {
                            InventoryEntity *entity = [self _processInventory:model];
                            [self.localDb addOrUpdateObject:entity];
                        }
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            RLMResults *results = [self _getInventoryResultsForDealerId:dealerId sortType:sortType];
                            NSMutableArray *pagedResults = [self pagedResultsForResults:results start:start pageSize:pageSize];
                            if ([responder respondsToSelector:@selector(responseHandler:total:)]) {
                                [responder responseHandler:pagedResults total:results.count];
                            }
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception responder:responder];
                    }
                }
            });
        } else {
            if ([responder respondsToSelector:@selector(errorHandler:)]) {
                [responder errorHandler:error];
            }
        }
    }];
}

- (void)getInventoryByDealerId:(NSString *)dealerId
                         start:(NSInteger)start pageSize:(NSInteger)pageSize
                      sortType:(NSString *)sortType filterDict:(NSDictionary *)filterDict
                      callback:(IdPagedResultBlock)callback
{
    [DataService getInventoryByDealerId:dealerId start:start pageSize:pageSize sortType:sortType filterDict:filterDict callback:^(InventoryListModel *listModel, NSError *error) {
        if (listModel && listModel.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self.localDb beginWriteTransaction];
                        for (InventoryModel *model in listModel.listings) {
                            InventoryEntity *entity = [self _processInventory:model];
                            [self.localDb addOrUpdateObject:entity];
                        }
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            RLMResults *results = [self _getInventoryResultsForDealerId:dealerId sortType:sortType];
                            NSMutableArray *pagedResults = [self pagedResultsForResults:results start:start pageSize:pageSize];
                            if (callback) callback(pagedResults, results.count, nil);
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception pagedCallback:callback];
                    }
                }
            });
        } else {
            if (callback) callback(nil, 0, error);
        }
    }];
}

- (void)getInventoryListByVins:(NSArray *)vins isRefreshing:(BOOL)isRefreshing responder:(id <RestResponseDelegate>)responder
{
    NSPredicate *vinsPredicate = [InventoryEntity vinsPredicate:vins];

    if (!isRefreshing) {
        RLMResults *results = [InventoryEntity objectsInRealm:self.localDb withPredicate:vinsPredicate];
        NSMutableArray *pagedResults = [self pagedResultsForResults:results start:0 pageSize:results.count];
        if (pagedResults.count > 0 && [responder respondsToSelector:@selector(responseHandler:total:)]) {
            [responder responseHandler:pagedResults total:results.count];
        }
    }

    [DataService getInventoryByVins:vins callback:^(InventoryListModel *listModel, NSError *error) {
        if (listModel && listModel.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self.localDb beginWriteTransaction];
                        for (InventoryModel *model in listModel.listings) {
                            InventoryEntity *entity = [self _processInventory:model];
                            [self.localDb addOrUpdateObject:entity];
                        }
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            RLMResults *results = [InventoryEntity objectsInRealm:self.localDb withPredicate:vinsPredicate];
                            NSMutableArray *pagedResults = [self pagedResultsForResults:results start:0 pageSize:results.count];
                            if ([responder respondsToSelector:@selector(responseHandler:total:)]) {
                                [responder responseHandler:pagedResults total:results.count];
                            }
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception responder:responder];
                    }
                }
            });
        } else {
            if ([responder respondsToSelector:@selector(errorHandler:)]) {
                [responder errorHandler:error];
            }
        }
    }];
}

- (void)getInventoryListByVins:(NSArray *)vins callback:(IdPagedResultBlock)callback
{
    NSPredicate *vinsPredicate = [InventoryEntity vinsPredicate:vins];

    [DataService getInventoryByVins:vins callback:^(InventoryListModel *listModel, NSError *error) {
        if (listModel && listModel.isValid && !error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self.localDb beginWriteTransaction];
                        for (InventoryModel *model in listModel.listings) {
                            InventoryEntity *entity = [self _processInventory:model];
                            [self.localDb addOrUpdateObject:entity];
                        }
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            RLMResults *results = [InventoryEntity objectsInRealm:self.localDb withPredicate:vinsPredicate];
                            NSMutableArray *pagedResults = [self pagedResultsForResults:results start:0 pageSize:listModel.numberFound];
                            if (callback) callback(pagedResults, results.count, nil);
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception pagedCallback:callback];
                    }
                }
            });
        } else {
            if (callback) callback(nil, 0, error);
        }
    }];
}

- (void)getInventoryById:(NSString *)inventoryId isRefreshing:(BOOL)isRefreshing responder:(id <RestResponseDelegate>)responder
{
    NSPredicate *predicate = [InventoryEntity inventoryIdPredicate:inventoryId];

    if (!isRefreshing) {
        InventoryEntity *entity = (InventoryEntity *) [InventoryEntity objectInRealm:self.localDb withPredicate:predicate];
        if (entity && [responder respondsToSelector:@selector(responseHandler:)]) {
            [responder responseHandler:entity];
        }
    }

    [DataService getInventoryListingById:inventoryId callback:^(InventoryModel *model, NSError *error) {
        if (!error && model.isValid) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self.localDb beginWriteTransaction];
                        InventoryEntity *entity = [self _processInventory:model];
                        [self.localDb addOrUpdateObject:entity];
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            InventoryEntity *result = (InventoryEntity *) [InventoryEntity objectInRealm:self.localDb withPredicate:predicate];
                            if ([responder respondsToSelector:@selector(responseHandler:)]) {
                                [responder responseHandler:result];
                            }
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception responder:responder];
                    }
                }
            });
        } else {
            if ([responder respondsToSelector:@selector(errorHandler:)]) {
                [responder errorHandler:error];
            }
        }
    }];
}

- (void)getInventoryById:(NSString *)inventoryId callback:(IdResultBlock)callback
{
    NSPredicate *predicate = [InventoryEntity inventoryIdPredicate:inventoryId];

    [DataService getInventoryListingById:inventoryId callback:^(InventoryModel *model, NSError *error) {
        if (!error && model.isValid) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    @try {
                        [self.localDb beginWriteTransaction];
                        InventoryEntity *entity = [self _processInventory:model];
                        [self.localDb addOrUpdateObject:entity];
                        [self.localDb commitWriteTransaction];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.localDb refresh];
                            InventoryEntity *result = (InventoryEntity *) [InventoryEntity objectInRealm:self.localDb withPredicate:predicate];
                            if (callback) callback(result, nil);
                        });
                    } @catch (NSException *exception) {
                        [self handleException:exception callback:callback];
                    }
                }
            });
        } else {
            if (callback) callback(nil, error);
        }
    }];
}

- (void)getInventoryByVin:(NSString *)vin callback:(IdResultBlock)callback
{
    NSPredicate *predicate = [InventoryEntity vinPredicate:vin];
    
    if ([Utils stringIsNullOrEmpty:vin]) {
        NSError *error = [ErrorsUtil errorWithCode:kErrorPersistenceInvalidData message:LOCALIZED(@"error_invaliddata")];
        if (callback) callback(nil, error);
        return;
    }
    
    [DataService getInventoryByVins:@[vin] callback:^(InventoryListModel *listModel, NSError *error) {
        if (listModel && listModel.isValid && listModel.listings.count > 0 && !error) {
            InventoryModel *inventoryModel = listModel.listings[0];
            [DataService getInventoryListingById:inventoryModel.inventoryId callback:^(InventoryModel *model, NSError *error) {
                if (!error && model.isValid) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        @autoreleasepool {
                            @try {
                                [self.localDb beginWriteTransaction];
                                InventoryEntity *entity = [self _processInventory:model];
                                [self.localDb addOrUpdateObject:entity];
                                [self.localDb commitWriteTransaction];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.localDb refresh];
                                    InventoryEntity *result = (InventoryEntity *) [InventoryEntity objectInRealm:self.localDb withPredicate:predicate];
                                    if (callback) callback(result, nil);
                                });
                            } @catch (NSException *exception) {
                                [self handleException:exception callback:callback];
                            }
                        }
                    });
                } else {
                    if (callback) callback(nil, error);
                }
            }];
        } else {
            if (callback) callback(nil, error);
        }
    }];
}

@end
