
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define URLMANAGER [UrlManager sharedManager]

FOUNDATION_EXPORT NSString *const __nonnull kSkyPatrolAvatarImageRepoUrl;

@interface UrlManager : NSObject

+ (nonnull instancetype)sharedManager;
- (nonnull NSString *)webServiceUrlForEndPoint:(nullable NSString *)endPoint;
- (nonnull NSString *)webServiceUrlForEndPoint:(nullable NSString *)endPoint isGateway:(BOOL)isGateway;

@end

NS_ASSUME_NONNULL_END
