
#import "ApiService.h"
#import "AccountModel.h"
#import "BuildModel.h"
#import "CommandStatusModel.h"
#import "DealerModel.h"
#import "DeviceModel.h"
#import "DeviceLocateModel.h"
#import "FinancingOptionModel.h"
#import "FleetModel.h"
#import "InventoryListModel.h"
#import "InventoryModel.h"
#import "MediaModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DataService : ApiService

///--------------------------------------
/// @name Authentication
///--------------------------------------

+ (void)loginWithUsername:(nullable NSString *)username password:(nullable NSString *)password callback:(nullable IdResultBlock)callback;
+ (void)getFleetList:(nullable NSString *)token companyId:(NSInteger)companyId callback:(nullable IdResultBlock)callback;
+ (void)getDeviceListByFleetId:(NSInteger)fleetId page:(NSInteger)page pageSize:(NSInteger)pageSize callback:(nullable IdResultBlock)callback;
+ (void)getDeviceBySerialNumber:(nullable NSString *)serialNumber callback:(nullable IdResultBlock)callback;
+ (void)locateDeviceBySerialNumber:(nullable NSString *)serialNumber imei:(nullable NSString *)imei callback:(nullable IdResultBlock)callback;
+ (void)getCommandStatusByTransactionId:(NSInteger)transactionId callback:(nullable IdResultBlock)callback;

///--------------------------------------
/// @name Gateway
///--------------------------------------

+ (void)getDealerById:(nullable NSString *)dealerId callback:(nullable IdResultBlock)callback;
+ (void)getInventoryByDealerId:(nullable NSString *)dealerId
                         start:(NSInteger)start pageSize:(NSInteger)pageSize
                      sortType:(nullable NSString *)sortType
                    filterDict:(nullable NSDictionary *)filterDict
                      callback:(nullable IdResultBlock)callback;
+ (void)getInventoryByVins:(nullable NSArray *)vins callback:(nullable IdResultBlock)callback;
+ (void)getInventoryListingById:(nullable NSString *)inventoryId callback:(nullable IdResultBlock)callback;

@end

NS_ASSUME_NONNULL_END
