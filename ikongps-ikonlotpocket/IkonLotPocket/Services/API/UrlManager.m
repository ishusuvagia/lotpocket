
#import "UrlManager.h"
#import "CommonAssert.h"

static NSString *devIkonWspUrl = @"http://wsikongps.ecombid.com";
static NSString *prodIkonWspUrl = @"http://wsikongps.ecombid.com";

static NSString *devGatewayUrl = @"http://gatewayapi.ecombid.com";
static NSString *prodGatewayUrl = @"http://gatewayapi.ecombid.com";

NSString *const kSkyPatrolAvatarImageRepoUrl = @"http://images.skypatrolusa.com/img.php?src=mercury/avatars/";

@implementation UrlManager

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)sharedManager
{
    static UrlManager *urlManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        urlManager = [UrlManager new];
    });
    return urlManager;
}

///--------------------------------------
#pragma mark - Life cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
    }
    return self;
}

///--------------------------------------
#pragma mark - Helpers
///--------------------------------------

- (NSString *)_getIkonWspUrl
{
#ifdef DEBUG
    return devIkonWspUrl;
#else
    return prodIkonWspUrl;
#endif
}

- (NSString *)_getGatewayUrl
{
#ifdef DEBUG
    return devGatewayUrl;
#else
    return prodGatewayUrl;
#endif
}

///--------------------------------------
#pragma mark - URL Forming
///--------------------------------------

- (NSString *)webServiceUrlForEndPoint:(NSString *)endPoint
{
    NSString *serviceUrl = [self _getIkonWspUrl];
    NSString *newUrl;
    
    if ([serviceUrl hasSuffix:@"/"]) {
        newUrl = [NSString stringWithFormat:@"%@/%@", serviceUrl, endPoint];
    } else {
        newUrl = [NSString stringWithFormat:@"%@/%@", serviceUrl, endPoint];
    }
    
    return newUrl;
}

- (NSString *)webServiceUrlForEndPoint:(NSString *)endPoint isGateway:(BOOL)isGateway
{
    NSString *serviceUrl = isGateway ? [self _getGatewayUrl] : [self _getIkonWspUrl];
    NSString *newUrl;
    
    if ([serviceUrl hasSuffix:@"/"]) {
        newUrl = [NSString stringWithFormat:@"%@/%@", serviceUrl, endPoint];
    } else {
        newUrl = [NSString stringWithFormat:@"%@/%@", serviceUrl, endPoint];
    }
    
    return newUrl;
}

@end
