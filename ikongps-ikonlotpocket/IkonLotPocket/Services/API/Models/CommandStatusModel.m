
#import "CommandStatusModel.h"

@implementation CommandStatusModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"errorCode": @"ErrorCode",
            @"errorMessage": @"ErrorMessage",
            @"transactionId": @"TransactionId",
            @"commandDescription": @"Description",
            @"commandStatus": @"Status",
            @"latitude": @"Latitude",
            @"longitude": @"Longitude",
            @"location": @"Location"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return YES;
}

@end
