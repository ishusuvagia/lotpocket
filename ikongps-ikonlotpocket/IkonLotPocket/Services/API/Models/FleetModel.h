
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FleetListModel : ObjectModel

@property (nonatomic, assign) NSInteger errorCode;
@property (nullable, nonatomic, copy) NSString *errorMessage;
@property (nonatomic, assign) NSInteger transactionId;
@property (nullable, nonatomic, copy) NSArray *fleetList;
@property (nonatomic, assign) NSInteger totalFleets;

@end

@interface FleetModel : ObjectModel

@property (nonatomic, assign) NSInteger fleetId;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger totalUnits;

@end

NS_ASSUME_NONNULL_END
