
#import "InventoryListModel.h"

@implementation InventoryListModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"numberFound": @"num_found",
            @"listings": @"listings"
    };
}

///--------------------------------------
#pragma mark - Transformer
///--------------------------------------

+ (NSValueTransformer *)listingsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:InventoryModel.class];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return YES;
}

@end
