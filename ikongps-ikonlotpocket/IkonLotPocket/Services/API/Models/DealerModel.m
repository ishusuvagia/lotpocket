
#import "DealerModel.h"

@implementation DealerModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
        @"dealerId": @"id",
        @"sellerName": @"seller_name",
        @"inventoryUrl": @"inventory_url",
        @"dataSource": @"data_source",
        @"status": @"status",
        @"dealerType": @"dealer_type",
        @"street": @"street",
        @"city": @"city",
        @"state": @"state",
        @"zipCode": @"zip",
        @"country": @"country",
        @"latitude": @"latitude",
        @"longitude": @"longitude",
        @"sellerPhone": @"seller_phone",
        @"distance": @"distance"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return ![Utils stringIsNullOrEmpty:_dealerId];
}

@end

@implementation InventoryDealerModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"dealerId": @"id",
            @"website": @"website",
            @"name": @"name",
            @"dealerType": @"dealer_type",
            @"street": @"street",
            @"city": @"city",
            @"state": @"state",
            @"zipCode": @"zip",
            @"country": @"country",
            @"latitude": @"latitude",
            @"longitude": @"longitude"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return self.dealerId > 0;
}

@end
