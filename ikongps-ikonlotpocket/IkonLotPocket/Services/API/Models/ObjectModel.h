
#import <Mantle/Mantle.h>
#import <UIKit/UIKit.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface ObjectModel : MTLModel <MTLJSONSerializing>

+ (nullable NSValueTransformer *)dotnetDateTransformer;
+ (nullable NSValueTransformer *)dateTransformer;
+ (nullable NSValueTransformer *)dateUTCTransformer;
+ (nullable NSValueTransformer *)timeTransformer;
- (BOOL)isValid;

@end

NS_ASSUME_NONNULL_END
