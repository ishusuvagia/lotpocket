
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountModel : ObjectModel

@property (nonatomic, assign) NSInteger userId;
@property (nullable, nonatomic, copy) NSString *token;
@property (nonatomic, assign) BOOL success;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL migrate;
@property (nonatomic, assign) NSInteger companyId;
@property (nonatomic, assign) NSInteger transactionId;
@property (nullable, nonatomic, copy) NSString *errorMessage;
@property (nonatomic, assign) NSInteger errorCode;

@end

NS_ASSUME_NONNULL_END
