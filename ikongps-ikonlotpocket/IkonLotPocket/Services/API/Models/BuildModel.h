
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BuildModel : ObjectModel

@property (nonatomic, assign) NSInteger year;
@property (nullable, nonatomic, copy) NSString *make;
@property (nullable, nonatomic, copy) NSString *model;
@property (nullable, nonatomic, copy) NSString *trim;
@property (nullable, nonatomic, copy) NSString *bodyType;
@property (nullable, nonatomic, copy) NSString *vehicleType;
@property (nullable, nonatomic, copy) NSString *transmission;
@property (nullable, nonatomic, copy) NSString *driveTrain;
@property (nullable, nonatomic, copy) NSString *fuelType;
@property (nullable, nonatomic, copy) NSString *engine;
@property (nonatomic, assign) CGFloat engineSize;
@property (nullable, nonatomic, copy) NSString *engineBlock;
@property (nonatomic, assign) NSInteger doors;
@property (nonatomic, assign) NSInteger cylinders;
@property (nullable, nonatomic, copy) NSString *madeIn;
@property (nullable, nonatomic, copy) NSString *steeringType;
@property (nullable, nonatomic, copy) NSString *antiBrakeSystem;
@property (nullable, nonatomic, copy) NSString *tankSize;
@property (nullable, nonatomic, copy) NSString *overallHeight;
@property (nullable, nonatomic, copy) NSString *overallLength;
@property (nullable, nonatomic, copy) NSString *overallWidth;
@property (nullable, nonatomic, copy) NSString *standardSeating;
@property (nullable, nonatomic, copy) NSString *highwayMiles;
@property (nullable, nonatomic, copy) NSString *cityMiles;
@property (nullable, nonatomic, copy) NSString *trimR;

@end

NS_ASSUME_NONNULL_END
