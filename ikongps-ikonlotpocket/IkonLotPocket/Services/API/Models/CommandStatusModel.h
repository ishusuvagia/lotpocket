
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommandStatusModel : ObjectModel

@property (nonatomic, assign) NSInteger errorCode;
@property (nullable, nonatomic, copy) NSString *errorMessage;
@property (nonatomic, assign) NSInteger transactionId;
@property (nullable, nonatomic, copy) NSString *commandDescription;
@property (nonatomic, assign) CommandStatus commandStatus;
@property (nullable, nonatomic, copy) NSNumber *latitude;
@property (nullable, nonatomic, copy) NSNumber *longitude;
@property (nullable, nonatomic, copy) NSString *location;
@property (nonatomic, assign) NSInteger speed;

@end

NS_ASSUME_NONNULL_END
