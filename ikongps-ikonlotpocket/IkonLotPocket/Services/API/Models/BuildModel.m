
#import "BuildModel.h"

@implementation BuildModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"year": @"year",
            @"make": @"make",
            @"model": @"model",
            @"trim": @"trim",
            @"bodyType": @"body_type",
            @"vehicleType": @"vehicle_type",
            @"transmission": @"transmission",
            @"driveTrain": @"drivetrain",
            @"fuelType": @"fuel_type",
            @"engine": @"engine",
            @"engineSize": @"engine_size",
            @"engineBlock": @"engine_block",
            @"doors": @"doors",
            @"cylinders": @"cylinders",
            @"madeIn": @"made_in",
            @"steeringType": @"steering_type",
            @"antiBrakeSystem": @"antibrake_sys",
            @"tankSize": @"tank_size",
            @"overallHeight": @"overall_height",
            @"overallLength": @"overall_length",
            @"overallWidth": @"overall_width",
            @"standardSeating": @"standard_seating",
            @"highwayMiles": @"highway_miles",
            @"cityMiles": @"city_miles",
            @"trimR": @"trim_r"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return YES;
}

@end