
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExtraModel : ObjectModel

@property (nullable, nonatomic, copy) NSArray *options;
@property (nullable, nonatomic, copy) NSArray *features;
@property (nullable, nonatomic, copy) NSArray *exteriorFeatures;
@property (nullable, nonatomic, copy) NSArray *standardFeatures;
@property (nullable, nonatomic, copy) NSArray *interiorFeatures;
@property (nullable, nonatomic, copy) NSArray *safetyFeatures;
@property (nullable, nonatomic, copy) NSString *sellerComments;

@end

NS_ASSUME_NONNULL_END
