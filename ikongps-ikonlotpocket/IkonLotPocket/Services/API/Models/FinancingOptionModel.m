
#import "FinancingOptionModel.h"


@implementation FinancingOptionModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"loanTerm": @"loan_term",
            @"loanApr": @"loan_apr",
            @"downPaymentPercentage": @"down_payment_percentage"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return YES;
}

@end