
#import "ExtraModel.h"

@implementation ExtraModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"options": @"options",
            @"features": @"features",
            @"exteriorFeatures": @"exterior_f",
            @"standardFeatures": @"standard_f",
            @"interiorFeatures": @"interior_f",
            @"safetyFeatures": @"safety_f",
            @"sellerComments": @"seller_comments"
    };
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return YES;
}

@end