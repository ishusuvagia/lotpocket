

#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@class DeviceLocateModel;

@interface DeviceLocateContainerModel : ObjectModel

@property (nullable, nonatomic, copy) DeviceLocateModel *result;

@end

@interface DeviceLocateModel : ObjectModel

@property (nonatomic, assign) NSInteger errorCode;
@property (nullable, nonatomic, copy) NSString *errorMessage;
@property (nonatomic, assign) NSInteger transactionId;

@end

NS_ASSUME_NONNULL_END
