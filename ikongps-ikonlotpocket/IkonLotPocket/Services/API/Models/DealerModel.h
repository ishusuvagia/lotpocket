
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DealerModel : ObjectModel

@property (nullable, nonatomic, copy) NSString *dealerId;
@property (nullable, nonatomic, copy) NSString *sellerName;
@property (nullable, nonatomic, copy) NSString *inventoryUrl;
@property (nullable, nonatomic, copy) NSString *dataSource;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSString *dealerType;
@property (nullable, nonatomic, copy) NSString *street;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, copy) NSString *state;
@property (nullable, nonatomic, copy) NSString *zipCode;
@property (nullable, nonatomic, copy) NSString *country;
@property (nullable, nonatomic, copy) NSString *latitude;
@property (nullable, nonatomic, copy) NSString *longitude;
@property (nullable, nonatomic, copy) NSString *sellerPhone;
@property (nonatomic, assign) CGFloat distance;

@end

@interface InventoryDealerModel : ObjectModel

@property (nonatomic, assign) NSInteger dealerId;
@property (nullable, nonatomic, copy) NSString *website;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *dealerType;
@property (nullable, nonatomic, copy) NSString *street;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, copy) NSString *state;
@property (nullable, nonatomic, copy) NSString *zipCode;
@property (nullable, nonatomic, copy) NSString *country;
@property (nullable, nonatomic, copy) NSString *latitude;
@property (nullable, nonatomic, copy) NSString *longitude;

@end

NS_ASSUME_NONNULL_END
