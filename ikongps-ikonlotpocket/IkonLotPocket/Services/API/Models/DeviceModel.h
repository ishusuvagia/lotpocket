
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@class DeviceListModel;
@class DeviceModel;

@interface DeviceDetailModel : ObjectModel

@property (nullable, nonatomic, strong) DeviceListModel *deviceDetail;

@end

@interface DeviceListModel : ObjectModel

@property (nonatomic, assign) NSInteger errorCode;
@property (nullable, nonatomic, copy) NSString *errorMessage;
@property (nonatomic, assign) NSInteger transactionId;
@property (nullable, nonatomic, copy) NSArray *deviceList;
@property (nonatomic, assign) NSInteger totalDevices;

@end

@interface DeviceModel : ObjectModel

@property (nonatomic, assign) NSInteger deviceId;
@property (nonatomic, assign) NSInteger fleetId;
@property (nonatomic, assign) NSInteger deviceType;
@property (nullable, nonatomic, copy) NSString *avatar;
@property (nullable, nonatomic, copy) NSString *batteryLevel;
@property (nullable, nonatomic, copy) NSDate *firstReport;
@property (nullable, nonatomic, copy) NSDate *lastReport;
@property (nullable, nonatomic, copy) NSDate *lastReportUtc;
@property (nullable, nonatomic, copy) NSString *heading;
@property (nullable, nonatomic, copy) NSString *imei;
@property (nullable, nonatomic, copy) NSString *inCommunication;
@property (nonatomic, assign) BOOL isInstalled;
@property (nullable, nonatomic, copy) NSNumber *latitude;
@property (nullable, nonatomic, copy) NSNumber *longitude;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSDate *paidUntil;
@property (nonatomic, assign) NSInteger rowNumber;
@property (nullable, nonatomic, copy) NSString *simStatus;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nonatomic, assign) NSInteger speed;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSString *stockNumber;
@property (nullable, nonatomic, copy) NSString *supportedFeature;
@property (nullable, nonatomic, copy) NSString *vin;
@property (nullable, nonatomic, copy) NSString *exteriorColor;
@property (nullable, nonatomic, copy) NSString *model;
@property (nullable, nonatomic, copy) NSString *make;
@property (nullable, nonatomic, copy) NSString *year;

@end

NS_ASSUME_NONNULL_END
