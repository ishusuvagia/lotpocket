
#import "InventoryModel.h"

@implementation InventoryModel

///--------------------------------------
#pragma mark - Mapping
///--------------------------------------

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"inventoryId": @"id",
            @"vin": @"vin",
            @"heading": @"heading",
            @"price": @"price",
            @"miles": @"miles",
            @"msrp": @"msrp",
            @"dataSource": @"data_source",
            @"isCertified": @"is_certified",
            @"vdpUrl": @"vdp_url",
            @"carfaxOneOwner": @"carfax_1_owner",
            @"carfaxCleanTitle": @"carfax_clean_title",
            @"exteriorColor": @"exterior_color",
            @"interiorColor": @"interior_color",
            @"daysOnMarket": @"dom",
            @"last180DaysOnMarket": @"dom_180",
            @"daysOnMarketActive": @"dom_active",
            @"sellerType": @"seller_type",
            @"inventoryType": @"inventory_type",
            @"stockNumber": @"stock_no",
            @"lastSeenAt": @"last_seen_at",
            @"lastSeenAtDate": @"last_seen_at_date",
            @"scrapedAt": @"scraped_at",
            @"scrapedAtDate": @"scraped_at_date",
            @"firstSeenAt": @"first_seen_at",
            @"firstSeenAtDate": @"first_seen_at_date",
            @"referencePrice": @"ref_price",
            @"referencePriceDt": @"ref_price_dt",
            @"referenceMiles": @"ref_miles",
            @"referenceMilesDt": @"ref_miles_dt",
            @"source": @"source",
            @"financingOptions": @"financing_options",
            @"media": @"media",
            @"extra": @"extra",
            @"dealer": @"dealer",
            @"build": @"build"
    };
}

///--------------------------------------
#pragma mark - Transformer
///--------------------------------------

+ (NSValueTransformer *)lastSeenAtDateJSONTransformer
{
    return [self dateUTCTransformer];
}

+ (NSValueTransformer *)scrapedAtDateJSONTransformer
{
    return [self dateUTCTransformer];
}

+ (NSValueTransformer *)firstSeenAtDateJSONTransformer
{
    return [self dateUTCTransformer];
}

+ (NSValueTransformer *)financingOptionsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:FinancingOptionModel.class];
}

+ (NSValueTransformer *)mediaJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:MediaModel.class];
}

+ (NSValueTransformer *)dealerJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:InventoryDealerModel.class];
}

+ (NSValueTransformer *)buildJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:BuildModel.class];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (BOOL)isValid
{
    return ![Utils stringIsNullOrEmpty:self.inventoryId];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (NSString *)getInventoryTitle
{
    if (self.build) {
        NSString *year = self.build.year > 0 ? [@(self.build.year) stringValue] : kEmptyString;
        NSString *make = self.build.make ?: kEmptyString;
        NSString *model = self.build.model ?: kEmptyString;
        NSString *title = [NSString stringWithFormat:@"%@ %@ %@", year, make, model];
        return title;
    } else {
        return self.heading;
    }
}

- (NSString *)getDetailedInventoryTitle
{
    if (self.build) {
        NSString *year = self.build.year > 0 ? [@(self.build.year) stringValue] : kEmptyString;
        NSString *make = self.build.make ?: kEmptyString;
        NSString *model = self.build.model ?: kEmptyString;
        NSString *body = self.build.bodyType ?: kEmptyString;
        NSString *title = [NSString stringWithFormat:@"%@ %@ %@ %@", year, make, model, body];
        return title;
    } else {
        return self.heading;
    }
}

- (NSString *)getConditionStatus
{
    if (self.isCertified) {
        return @"certified";
    } else {
        return self.inventoryType;
    }
}

@end
