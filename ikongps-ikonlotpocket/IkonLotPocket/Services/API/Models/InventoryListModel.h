
#import "ObjectModel.h"
#import "InventoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryListModel : ObjectModel

@property (nonatomic, assign) NSInteger numberFound;
@property (nullable, nonatomic, copy) NSArray *listings;

@end

NS_ASSUME_NONNULL_END
