
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MediaModel : ObjectModel

@property (nullable, nonatomic, copy) NSArray *photoLinks;

@end

NS_ASSUME_NONNULL_END
