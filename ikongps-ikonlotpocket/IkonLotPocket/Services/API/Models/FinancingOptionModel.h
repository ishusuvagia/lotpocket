
#import "ObjectModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FinancingOptionModel : ObjectModel

@property (nonatomic, assign) NSInteger loanTerm;
@property (nonatomic, assign) CGFloat loanApr;
@property (nonatomic, assign) CGFloat downPaymentPercentage;

@end

NS_ASSUME_NONNULL_END