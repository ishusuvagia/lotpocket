
#import "ApiService.h"
#import "ObjectModel.h"

NSUInteger const kDefaultPageSize = 50;

@implementation ApiService

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSDictionary *)additionalHeaders
{
    return @{};
}

+ (void)serializeObject:(id)value model:(Class)model callback:(IdResultBlock)callback
{
    @try {
        NSError *error = nil;

        if ([value isKindOfClass:[NSDictionary class]]) {
            id parsedObject = [MTLJSONAdapter modelOfClass:model fromJSONDictionary:value error:&error];
            if (!error) {
                if (![parsedObject isValid]) {
                    error = [ErrorsUtil errorWithCode:kErrorAPICompatible domain:WebRequestErrorDomain
                            message:[ErrorsUtil getErrorMessage:kErrorAPICompatible]];
                }
            } else {
                parsedObject = @{};
                TRACE("Parsing Error: %@", error);
            }
            if (callback) callback(parsedObject, error);
        } else if ([value isKindOfClass:[NSArray class]]) {
            NSMutableArray *parsedObjects = [NSMutableArray array];
            for (NSDictionary *item in value) {
                id parsedObject = [MTLJSONAdapter modelOfClass:model fromJSONDictionary:item error:&error];
                if (!error) {
                    if ([parsedObject isValid]) {
                        [parsedObjects addObject:parsedObject];
                    }
                } else {
                    TRACE("Parsing Error: %@", error);
                }
            }
            if (callback) callback(parsedObjects, error);
        } else if ([value isKindOfClass:[NSNull class]]) {
            TRACE("%@", @"Object is Null");
            error = [ErrorsUtil errorWithCode:kErrorAPINullData domain:WebRequestErrorDomain
                    message:[ErrorsUtil getErrorMessage:kErrorAPINullData]];
            if (callback) callback(nil, error);
        } else {
            error = [ErrorsUtil errorWithCode:kErrorAPIInconsistentData domain:WebRequestErrorDomain
                                      message:[ErrorsUtil getErrorMessage:kErrorAPIInconsistentData]];
            if (callback) callback(nil, error);
        }
    }
    @catch (NSException *exception) {
        TRACE("Exception: %@", exception.description);
        NSError *error = [ErrorsUtil errorWithCode:kErrorParsingException domain:WebRequestErrorDomain
                                           message:[ErrorsUtil getErrorMessage:kErrorParsingException]];
        if (callback) callback(nil, error);
    }
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
        _restClient = [RestClient new];
    }
    return self;
}

///--------------------------------------
#pragma mark - Protected
///--------------------------------------

- (void)performRequest:(RestRequest *)request model:(Class)model objectCallback:(IdResultBlock)callback
{
    fTRACE("Requestasd: %@", request);

    [self.restClient performRequest:request callback:[RestCallback callbackWithResult:^(RestResponse *response) {
        [self _processResponse:response model:model callback:^(ObjectModel *objectModel, NSError *__autoreleasing error) {
            if (!error) {
                if (callback) callback(objectModel, nil);
            } else {
                if (callback) callback(nil, error);
            }
        }];
    }]];
}

- (void)performRequest:(RestRequest *)request booleanCallback:(BooleanResultBlock)callback
{
    fTRACE("Request: %@", request);

    [self.restClient performRequest:request callback:[RestCallback callbackWithResult:^(RestResponse *response) {
        [self _processResponse:response booleanCallback:callback];
    }]];
}

///--------------------------------------
#pragma mark - Response Helpers
///--------------------------------------

- (void)_processResponse:(RestResponse *)response model:(Class)model callback:(IdResultBlock)callback
{
    NSError *error = nil;

    if (response.isSuccess) {
        NSDictionary *result = (NSDictionary *) response.result;
        NSLog(@"Login Result : %@",result);
        [ApiService serializeObject:result model:model callback:callback];
    } else {
        error = [ErrorsUtil errorWithCode:(ErrorCode)response.headerStatusCode domain:WebRequestErrorDomain
                                  message:response.statusMessage];
        if (callback) callback(nil, error);
    }
}

- (void)_processResponse:(RestResponse *)response booleanCallback:(BooleanResultBlock)callback
{
    NSError *error = nil;

    if (response.isSuccess) {
        if (callback) callback(YES, nil);
    } else {
        error = [ErrorsUtil errorWithCode:(ErrorCode)response.headerStatusCode domain:WebRequestErrorDomain
                                  message:response.statusMessage];
        if (callback) callback(NO, error);
    }
}

@end
