
#import "DataService.h"
#import "OrderedDictionary.h"


static NSString *const kAuthenticationToken = @"krtG-448y-yTR4-d5tg-8I0r-yT55";

static NSString *const kLogin_Endpoint = @"login";
static NSString *const kFleetList_Endpoint = @"v1/get_subfleets";
static NSString *const kUnitList_Endpoint = @"units";
static NSString *const kUnitDetail_Endpoint = @"get_units_details";
static NSString *const kLocate_Endpoint = @"locate";
static NSString *const kCommandStatus_Endpoint = @"command_status";

static NSString *const kGateway_MarketCheck_Search_Endpoint = @"external/search?";
static NSString *const kGateway_MarketCheck_Dealer_Endpoint = @"external/dealer/";
static NSString *const kGateway_MarketCheck_Listing_Endpoint = @"external/listing/";

@implementation DataService

///--------------------------------------
#pragma mark - Private
///--------------------------------------

+ (NSString *)_buildUrlQueryForUrl:(NSString *)url paramDict:(OrderedDictionary *)paramDict
{
    if ([Utils stringIsNullOrEmpty:url] || paramDict == nil) {
        return url;
    }

    for (NSUInteger idx = 0; idx < paramDict.count; idx++) {
        NSString *key = [paramDict keyAtIndex:idx];
        NSString *value = [paramDict objectAtIndex:idx];
        NSString *urlParam = kEmptyString;
        if (idx == 0) {
            urlParam = [NSString stringWithFormat:@"%@=%@", [key lowercaseString], [value lowercaseString]];
        } else {
            urlParam = [NSString stringWithFormat:@"&%@=%@", [key lowercaseString], [value lowercaseString]];
        }
        url = [url stringByAppendingString:urlParam];
    }

    return url;
}

+ (NSDictionary *)_buildParametersBySortType:(NSString *)sortType
{
    if ([sortType isEqualToString:kExternal_SortBy_Model]) {
        return @{@"sort_by": @"model", @"sort_order": @"asc"};
    } else if ([sortType isEqualToString:kExternal_SortBy_HighestPrice]) {
        return @{@"sort_by": @"msrp", @"sort_order": @"desc"};
    } else if ([sortType isEqualToString:kExternal_SortBy_LowestPrice]) {
        return @{@"sort_by": @"msrp", @"sort_order": @"asc"};
    } else if ([sortType isEqualToString:kExternal_SortBy_NewestYear]) {
        return @{@"sort_by": @"year", @"sort_order": @"desc"};
    } else if ([sortType isEqualToString:kExternal_SortBy_OldestYear]) {
        return @{@"sort_by": @"year", @"sort_order": @"asc"};
    } else if ([sortType isEqualToString:kExternal_SortBy_LowestMileage]) {
        return @{@"sort_by": @"miles", @"sort_order": @"asc"};
    } else if ([sortType isEqualToString:kExternal_SortBy_HighestMileage]) {
        return @{@"sort_by": @"miles", @"sort_order": @"desc"};
    }
    return @{};
}

+ (NSDictionary *)_buildParametersByFilterDict:(NSDictionary *)filterDict
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    for (NSString *key in filterDict) {
        NSString *value = filterDict[key];
        if ([key isEqualToString:kExternal_FilterBy_Year]) {
            dict[@"year"] = value;
        } else if ([key isEqualToString:kExternal_FilterBy_PriceRange]) {
            dict[@"price_range"] = value;
        } else if ([key isEqualToString:kExternal_FilterBy_MilesRange]) {
            dict[@"miles_range"] = value;
        } else if ([key isEqualToString:kExternal_FilterBy_BodyStyle]) {
            dict[@"body_type"] = value;
        } else if ([key isEqualToString:kExternal_FilterBy_Condition]) {
            if (![value isEqualToString:@"all"]) {
                dict[@"car_type"] = value;
            }
        } else if ([key isEqualToString:kExternal_FilterBy_Transmission]) {
            NSString *newValue = [value stringByReplacingOccurrencesOfString:@"automatic" withString:@"a"];
            newValue = [newValue stringByReplacingOccurrencesOfString:@"manual" withString:@"m"];
            dict[@"transmission"] = value;
        } else if ([key isEqualToString:kExternal_FilterBy_FuelType]) {
            NSString *newValue = [value stringByReplacingOccurrencesOfString:@"regular_unleaded" withString:@"regular unleaded"];
            newValue = [newValue stringByReplacingOccurrencesOfString:@"premium_unleaded" withString:@"premium unleaded"];
            newValue = [newValue stringByReplacingOccurrencesOfString:@"flex_fuel" withString:@"flex-fuel"];
            dict[@"fuel_type"] = value;
        }
    }
    return dict;
}

///--------------------------------------
#pragma mark - Autentication
///--------------------------------------

+ (void)loginWithUsername:(NSString *)username password:(NSString *)password callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kLogin_Endpoint isGateway:NO];
    if (![Utils stringIsNullOrEmpty:username]) path = [path stringByAppendingString:[NSString stringWithFormat:@"/%@", username]];
    if (![Utils stringIsNullOrEmpty:password]) path = [path stringByAppendingString:[NSString stringWithFormat:@"/%@", password]];

    NSDictionary *additionalHeaders = [self additionalHeaders];
    
      NSLog(@"loginWithUsername paths  :: %@",path);

    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:AccountModel.class objectCallback:callback];
}

+ (void)getFleetList:(NSString *)token companyId:(NSInteger)companyId callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kFleetList_Endpoint isGateway:NO];
    if (![Utils stringIsNullOrEmpty:token]) path = [path stringByAppendingString:[NSString stringWithFormat:@"/%@", token]];
    path = [path stringByAppendingString:[NSString stringWithFormat:@"/%ld", (long)companyId]];
    path = [path stringByAppendingString:@"/0/1"];

    NSDictionary *additionalHeaders = [self additionalHeaders];
    
    NSLog(@"getFleetList paths  :: %@",path);

    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:FleetListModel.class objectCallback:callback];
}

+ (void)getDeviceListByFleetId:(NSInteger)fleetId page:(NSInteger)page pageSize:(NSInteger)pageSize callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kUnitList_Endpoint isGateway:NO];
    if (![Utils stringIsNullOrEmpty:AUTHMANAGER.token]) path = [path stringByAppendingString:[NSString stringWithFormat:@"/%@", AUTHMANAGER.token]];
    path = [path stringByAppendingString:[NSString stringWithFormat:@"/%ld", (long)fleetId]];
    path = [path stringByAppendingString:[NSString stringWithFormat:@"/%ld/%ld", (long)page, (long)pageSize]];

    NSDictionary *additionalHeaders = [self additionalHeaders];

    
    NSLog(@"getDeviceListByFleetId paths  :: %@",path);
    
    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:DeviceListModel.class objectCallback:callback];
}

+ (void)getDeviceBySerialNumber:(NSString *)serialNumber callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kUnitDetail_Endpoint isGateway:NO];

    MutableOrderedDictionary *parameters = [MutableOrderedDictionary dictionary];
    parameters[@"token"] = AUTHMANAGER.token ?: kEmptyString;
    parameters[@"serials"] = serialNumber ?: kEmptyString;

    NSDictionary *additionalHeaders = [self additionalHeaders];
    
    NSLog(@"getDeviceBySerialNumber paths :: %@",path);

    [[DataService new] performRequest:POSTREQUEST(path, parameters, additionalHeaders) model:DeviceDetailModel.class objectCallback:callback];
}

+ (void)locateDeviceBySerialNumber:(NSString *)serialNumber imei:(NSString *)imei callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kLocate_Endpoint isGateway:NO];

    MutableOrderedDictionary *parameters = [MutableOrderedDictionary dictionary];
    parameters[@"token"] = AUTHMANAGER.token ?: kEmptyString;
    parameters[@"serial"] = serialNumber ?: kEmptyString;
    parameters[@"imei"] = imei ?: kEmptyString;
    parameters[@"authtoken"] = kAuthenticationToken;

    NSDictionary *additionalHeaders = [self additionalHeaders];
    
    
       NSLog(@"locateDeviceBySerialNumber paths :: %@",path);

    [[DataService new] performRequest:POSTREQUEST(path, parameters, additionalHeaders) model:DeviceLocateContainerModel.class objectCallback:callback];
}

+ (void)getCommandStatusByTransactionId:(NSInteger)transactionId callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kCommandStatus_Endpoint isGateway:NO];
    if (![Utils stringIsNullOrEmpty:AUTHMANAGER.token]) path = [path stringByAppendingString:[NSString stringWithFormat:@"/%@", AUTHMANAGER.token]];
    path = [path stringByAppendingString:[NSString stringWithFormat:@"/%ld", (long)transactionId]];

    NSDictionary *additionalHeaders = [self additionalHeaders];
    
    
     NSLog(@"getCommandStatusByTransactionId paths  :: %@",path);

    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:CommandStatusModel.class objectCallback:callback];
}

///--------------------------------------
#pragma mark - Gateway
///--------------------------------------

+ (void)getDealerById:(NSString *)dealerId callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kGateway_MarketCheck_Dealer_Endpoint isGateway:YES];
    if (![Utils stringIsNullOrEmpty:dealerId]) path = [path stringByAppendingString:dealerId];
    
    NSDictionary *additionalHeaders = [self additionalHeaders];
    
    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:DealerModel.class objectCallback:callback];
}

+ (void)getInventoryByDealerId:(NSString *)dealerId
                         start:(NSInteger)start pageSize:(NSInteger)pageSize
                      sortType:(NSString *)sortType
                    filterDict:(NSDictionary *)filterDict
                      callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kGateway_MarketCheck_Search_Endpoint isGateway:YES];

    MutableOrderedDictionary *parameters = [MutableOrderedDictionary dictionary];
    parameters[@"dealer_id"] = dealerId ?: kEmptyString;
    parameters[@"start"] = [@(start) stringValue];
    parameters[@"rows"] = [@(pageSize) stringValue];
    [parameters addEntriesFromDictionary:[self _buildParametersBySortType:sortType]];
    [parameters addEntriesFromDictionary:[self _buildParametersByFilterDict:filterDict]];

    path = [self _buildUrlQueryForUrl:path paramDict:parameters];

    NSDictionary *additionalHeaders = [self additionalHeaders];

    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:InventoryListModel.class objectCallback:callback];
}

+ (void)getInventoryByVins:(NSArray *)vins callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kGateway_MarketCheck_Search_Endpoint isGateway:YES];
    NSString *vinStrings = [Utils formatCommaSpaceValue:vins];

    MutableOrderedDictionary *parameters = [MutableOrderedDictionary dictionary];
    parameters[@"start"] = [@(0) stringValue];
    parameters[@"rows"] = [@(vins.count) stringValue];
    parameters[@"vin"] = vinStrings ?: kEmptyString;

    path = [self _buildUrlQueryForUrl:path paramDict:parameters];

    NSDictionary *additionalHeaders = [self additionalHeaders];

    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:InventoryListModel.class objectCallback:callback];
}

+ (void)getInventoryListingById:(NSString *)inventoryId callback:(IdResultBlock)callback
{
    NSString *path = [URLMANAGER webServiceUrlForEndPoint:kGateway_MarketCheck_Listing_Endpoint isGateway:YES];
    if (![Utils stringIsNullOrEmpty:inventoryId]) path = [path stringByAppendingString:inventoryId];
    
    NSDictionary *additionalHeaders = [self additionalHeaders];
    
    [[DataService new] performRequest:GETREQUEST(path, additionalHeaders) model:InventoryModel.class objectCallback:callback];
}

@end
