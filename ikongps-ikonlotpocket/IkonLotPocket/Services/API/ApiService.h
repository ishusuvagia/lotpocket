
#import <Foundation/Foundation.h>
#import "ErrorsUtil.h"
#import "UrlManager.h"
#import "RestClient.h"

NS_ASSUME_NONNULL_BEGIN

///--------------------------------------
/// @name Api Constants
///--------------------------------------

FOUNDATION_EXPORT NSUInteger const kDefaultPageSize;

///--------------------------------------
/// @name Api Macros
///--------------------------------------

#define GETREQUEST(path, additionalHeaders) [RestRequest requestObjectWithHTTPPath:path httpMethod:HTTPRequestMethodGET bodyData:nil additionalHeaders:additionalHeaders]
#define POSTREQUEST(path, data, additionalHeaders) [RestRequest requestObjectWithHTTPPath:path httpMethod:HTTPRequestMethodPOST bodyData:data additionalHeaders:additionalHeaders]
#define PUTREQUEST(path, data, additionalHeaders) [RestRequest requestObjectWithHTTPPath:path httpMethod:HTTPRequestMethodPUT bodyData:data additionalHeaders:additionalHeaders]

///--------------------------------------
/// @name ApiService
///--------------------------------------

@interface ApiService : NSObject

@property (nonnull, nonatomic, readonly) RestClient *restClient;

+ (nullable NSDictionary *)additionalHeaders;
+ (void)serializeObject:(id)value model:(Class)model callback:(nullable IdResultBlock)callback;
- (void)performRequest:(nonnull RestRequest *)request model:(nonnull Class)model objectCallback:(nullable IdResultBlock)callback;
- (void)performRequest:(nonnull RestRequest *)request booleanCallback:(BooleanResultBlock)callback;

@end

NS_ASSUME_NONNULL_END
