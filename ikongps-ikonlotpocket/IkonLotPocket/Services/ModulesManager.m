
#import "ModulesManager.h"
#import "InventoryModule.h"
#import "EngageModule.h"
#import "SalesToolModule.h"
#import "VehicleLotModule.h"
#import "MapModule.h"
#import "GuardModule.h"
#import "MaintenanceModule.h"
#import "AlertsModule.h"


#define REGISTER_MODULE(name) [self _registerModule:[name class]];

@interface ModulesManager ()

@property (nonatomic, strong) NSMutableArray *_allModules;
@property (nonatomic, strong) NSMutableDictionary *_pushModules;

@end

@implementation ModulesManager

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)sharedManager;
{
    static ModulesManager *modulesManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        modulesManager = [ModulesManager new];
    });
    return modulesManager;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_registerModule:(Class)moduleClass
{
    if (![moduleClass conformsToProtocol:@protocol(ModuleDelegate)]) {
        [NSException raise:@"Module cannot be registered"
                    format:@"Module class must conform JKModuleProtocol"];
    }

    id module = [[moduleClass alloc] init];
    [self._allModules addObject:module];

    if ([moduleClass conformsToProtocol:@protocol(PushDelegate)]) {
        NSArray *types = [module getRequiredPushTypes];
        for (id type in types) {
            if (![type isKindOfClass:[NSString class]]) {
                [NSException raise:@"Invalid value type"
                            format:@"Type must be typeof NSString"];
            }
            if (self._pushModules[type]) {
                [NSException raise:@"PushType is invalid"
                            format:@"%@ push type is already registered", type];
            }
            self._pushModules[type] = module;
        }
    }
}

- (BOOL)_isAllowedModule:(id<ModuleDelegate>)module
{
    return YES;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)initModules:(NSArray *)authRoleAccess
{
    __allModules = [NSMutableArray new];
    __pushModules = [NSMutableDictionary new];
    
    REGISTER_MODULE(InventoryModule)
    REGISTER_MODULE(EngageModule)
    REGISTER_MODULE(SalesToolModule)
    REGISTER_MODULE(VehicleLotModule)
    REGISTER_MODULE(MapModule)
    REGISTER_MODULE(GuardModule)
    REGISTER_MODULE(MaintenanceModule)
    REGISTER_MODULE(AlertsModule)
    
}

- (NSArray *)getAllModules
{
    return self._allModules;
}

- (NSArray *)getAllowedModules
{
    NSMutableArray *result = [NSMutableArray new];
    for (id<ModuleDelegate> module in self.getAllModules) {
        if ([self _isAllowedModule:module]) {
            [result addObject:module];
        }
    }
    return result;
}

- (id <PushDelegate>)getModuleForPush:(NSString *)type
{
    return self._pushModules[type];
}

- (void)clear
{
    [self._allModules removeAllObjects];
    [self._pushModules removeAllObjects];
}

@end
