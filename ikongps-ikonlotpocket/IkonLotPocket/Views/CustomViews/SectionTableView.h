
#import <TPKeyboardAvoiding/TPKeyboardAvoidingTableView.h>

NS_ASSUME_NONNULL_BEGIN

@interface SectionTableView : TPKeyboardAvoidingTableView

@property (nonatomic, weak) id<UITableViewDelegate>tableViewProxyDelegate;

- (void)initWithCellPrototypes:(nullable NSArray<NSDictionary *> *)cellPrototypes
              hasHeaderSection:(BOOL)hasHeaderSection
              hasFooterSection:(BOOL)hasFooterSection
                 enableRefresh:(BOOL)enableRefresh
                   enableFetch:(BOOL)enableFetch
                 emptyDataText:(nullable NSString *)emptyDataText
                      delegate:(nullable id <SectionTableViewDelegate>)delegate;
- (nullable UIView *)buildHeaderFooterView:(nullable NSString *)text
                                 alignment:(NSTextAlignment)textAligment;
- (BOOL)isEmpty;
- (void)emptyData;
- (void)loadData:(nullable NSArray *)cellSectionData;
- (void)reload;
- (BOOL)isRefreshing;
- (BOOL)isFetching;
- (void)endRefreshing;
- (void)updateEmptyDataText:(nullable NSString *)emptyDataText;
- (void)hideEmptyView;

@end

NS_ASSUME_NONNULL_END
