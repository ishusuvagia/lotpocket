
#import <UIKit/UIKit.h>

@interface EmptyDataView : UIView

+ (CGFloat)height;
+ (CGFloat)width;
- (void)show;
- (void)hide;
- (void)updateViewWithText:(nullable NSString *)text;

@end
