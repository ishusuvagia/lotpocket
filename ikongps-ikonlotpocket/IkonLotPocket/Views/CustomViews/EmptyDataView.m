
#import "EmptyDataView.h"
#import "Utils.h"
#import "Constants.h"

@interface EmptyDataView ()

@property (strong, nonatomic) IBOutlet UIView *_view;
@property (weak, nonatomic) IBOutlet UILabel *_label;

@end

@implementation EmptyDataView

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 44.0f;
}

+ (CGFloat)width
{
    return 200.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self _init];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self _init];
    }
    return self;
}

- (void)_init
{
    [[NSBundle mainBundle] loadNibNamed:@"EmptyDataView" owner:self options:nil];
    [self addSubview:self._view];
    self._view.hidden = YES;
    self._label.text = LOCALIZED(@"no_availabledata");
    self._label.lineBreakMode = NSLineBreakByWordWrapping;
    self._label.numberOfLines = 0;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)show
{
    self._view.hidden = NO;
    [self._view setNeedsDisplay];
}

- (void)hide
{
    self._view.hidden = YES;
    [self._view setNeedsDisplay];
}

- (void)updateViewWithText:(NSString *)text
{
    self._label.text = ![Utils stringIsNullOrEmpty:text] ? text : kEmptyString;
}

@end
