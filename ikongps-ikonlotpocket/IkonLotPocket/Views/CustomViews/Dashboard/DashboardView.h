
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardView : UICollectionView

- (void)initButtons:(nullable UIViewController *)parentViewController;

@end

NS_ASSUME_NONNULL_END
