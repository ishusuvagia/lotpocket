
#import "DashboardCell.h"
#import "Logger.h"
#import "AppFont.h"
#import "AppColorScheme.h"

static const CGFloat kRelativePaddingSpacing = 0.02f;
static const CGFloat kTextLabelFontSize = 10.0f;
static const NSInteger kTextLabelMaxNumberOfLines = 2;
static const CGFloat kImageScaleFactor = 2.0f;
static const CGFloat kTextLabelMaxHeight = kTextLabelFontSize * kTextLabelMaxNumberOfLines * 1.5f;

/// <editor-fold desc="JKDashboardCellView">
///----------------------------------------------------------------------------
#pragma mark - JKDashboardCellView
///----------------------------------------------------------------------------

@interface DashboardCellView ()

@property (nonatomic, strong) UIImageView *_imageView;
@property (nonatomic, strong) UILabel *_textLabel;
@property (nonatomic, strong) UIView *_badgeView;

@end

@implementation DashboardCellView

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
    }
    return self;
}

- (instancetype)init
{
    if ((self = [super initWithFrame:CGRectZero])) {
        __imageView = [UIImageView new];
        __textLabel = [UILabel new];
        [self addSubview:__imageView];
        [self addSubview:__textLabel];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self._textLabel.textAlignment = NSTextAlignmentCenter;
        self._textLabel.numberOfLines = kTextLabelMaxNumberOfLines;
        self._textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self._textLabel.font = [AppFont dashboardTileFont];
        self._textLabel.textColor = [UIColor colorWithRed:24/255.f green:72/255.f blue:132/255.f alpha:1];
        self._imageView.contentMode = UIViewContentModeScaleAspectFit;
        self._imageView.clipsToBounds = YES;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self _adjustImageView];
    [self _adjustTextLabel];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (CGRect)_centerFrameWithInnerFrame:(CGRect)innerFrame outerFrame:(CGRect)outerFrame y:(CGFloat)y
{
    return CGRectMake((outerFrame.size.width - innerFrame.size.width) / 2, y, innerFrame.size.width, innerFrame.size.height);
}

- (CGSize)_imageViewSize
{
    CGFloat height =self.frame.size.height - (3 * kRelativePaddingSpacing * self.frame.size.height + kTextLabelMaxHeight);
    CGFloat width = self.frame.size.width - (2 * kRelativePaddingSpacing * self.frame.size.width);
    CGFloat size = MIN(150, 150);
    return CGSizeMake(size / kImageScaleFactor, size / kImageScaleFactor);
}

- (CGFloat)_remainingHeight
{
    return self.frame.size.height - (self._imageView.frame.size.height + kTextLabelMaxHeight);
}

- (void)_adjustImageView
{
    CGSize imageSize = [self _imageViewSize];
    // DEBUG_SIZE("imageSize=", imageSize);
    self._imageView.frame = CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height);
    self._imageView.frame = [self _centerFrameWithInnerFrame:self._imageView.frame outerFrame:self.frame
                                                           y:(CGFloat) (self._imageView.frame.origin.y + 0.5f * [self _remainingHeight])];
}

- (void)_adjustTextLabel
{
    self._textLabel.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width * (1 - 2 * kRelativePaddingSpacing), kTextLabelMaxHeight);
    [self._textLabel sizeToFit];
    self._textLabel.frame = [self _centerFrameWithInnerFrame:self._textLabel.frame outerFrame:self.frame y:CGRectGetMaxY(self._imageView.frame)+10.0f];
}

///--------------------------------------
#pragma mark - Setters
///--------------------------------------

- (void)setImage:(UIImage *)image
{
    _image = image;
    self._imageView.image = image;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self._textLabel.text = [text uppercaseString];
}

@end

/// </editor-fold>

/// <editor-fold desc="JKDashboardCell">
///----------------------------------------------------------------------------
#pragma mark - JKDashboardCell
///----------------------------------------------------------------------------

@implementation DashboardCell

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self _customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self _customInit];
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_customInit
{
    self.layer.borderWidth = 1.0f / [UIScreen mainScreen].scale;
    self.layer.borderColor = [AppColorScheme clear].CGColor;
    self.layer.backgroundColor = [AppColorScheme clear].CGColor;
    
}

///--------------------------------------
#pragma mark - Setter
///--------------------------------------

- (void)setContent:(DashboardCellView *)content
{
    if (_content != nil && _content.superview == self) {
        [_content removeFromSuperview];
    }
    _content = content;
    if (_content) {
        _content.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height);
        [self addSubview:_content];
    }
    
}

@end

/// </editor-fold>
