
#import <Mapbox/Mapbox.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapboxPointAnnotation : MGLPointAnnotation

@property (nonatomic, assign) BOOL willUseImage;

@end

NS_ASSUME_NONNULL_END
