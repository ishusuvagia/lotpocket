
#import <UIKit/UIKit.h>
#import <Mapbox/Mapbox.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapboxCalloutView : UIView <MGLCalloutView>

+ (CGFloat)height;
+ (CGFloat)width;
- (void)setupViewWithTitle:(nullable NSString *)title inventoryCode:(nullable NSString *)inventoryCode
                       vin:(nullable NSString *)vin batteryLevel:(nullable NSString *)batteryLevel imageUrl:(nullable NSString *)imageUrl;

@end

NS_ASSUME_NONNULL_END
