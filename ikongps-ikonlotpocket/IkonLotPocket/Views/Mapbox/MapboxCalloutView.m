
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "MapboxCalloutView.h"

static CGFloat const kTipHeight = 10.0;
static CGFloat const kTipWidth = 20.0;

@interface MapboxCalloutView ()
{
    id <MGLAnnotation> _representedObject;
    __weak id <MGLCalloutViewDelegate> _delegate;
    __unused UIView *_leftAccessoryView;    /* unused */
    __unused UIView *_rightAccessoryView;   /* unused */
    BOOL _dismissesAutomatically;
    BOOL _anchoredToAnnotation;
    CGRect originalFrame;
}

@property (strong, nonatomic) IBOutlet UIView *_view;
@property (weak, nonatomic) IBOutlet UIImageView *_imageView;
@property (weak, nonatomic) IBOutlet UILabel *_titlelbl;
@property (weak, nonatomic) IBOutlet UILabel *_vinLbl;
@property (weak, nonatomic) IBOutlet UILabel *_inventoryCodeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *_batteryIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *_batteryLevelLbl;
@property (weak, nonatomic) IBOutlet UIButton *_mainBtn;

@end

@implementation MapboxCalloutView

@synthesize representedObject = _representedObject;
@synthesize leftAccessoryView = _leftAccessoryView;     /* unused */
@synthesize rightAccessoryView = _rightAccessoryView;   /* unused */
@synthesize delegate = _delegate;
@synthesize anchoredToAnnotation = _anchoredToAnnotation;
@synthesize dismissesAutomatically = _dismissesAutomatically;

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 87.0f;
}

+ (CGFloat)width
{
    return 320.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self _init];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self _init];
    }
    return self;
}

- (void)_init
{
    [[NSBundle mainBundle] loadNibNamed:@"MapboxCalloutView" owner:self options:nil];
    self._view.backgroundColor = [UIColor clearColor];
    [self addSubview:self._view];
    self._mainBtn.backgroundColor = [UIColor whiteColor];
    self._mainBtn.contentEdgeInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    self._mainBtn.layer.cornerRadius = 5.0f;
    self.backgroundColor = [UIColor clearColor];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (BOOL)_isCalloutTappable
{
    if ([self.delegate respondsToSelector:@selector(calloutViewShouldHighlight:)]) {
        return (BOOL)[self.delegate performSelector:@selector(calloutViewShouldHighlight:) withObject:self];
    }
    
    return NO;
}

- (void)_calloutTapped
{
    if (self._isCalloutTappable && [self.delegate respondsToSelector:@selector(calloutViewTapped:)]) {
        [self.delegate performSelector:@selector(calloutViewTapped:) withObject:self];
    }
}

///--------------------------------------
#pragma mark - <MGLCalloutView>
///--------------------------------------

- (void)presentCalloutFromRect:(CGRect)rect inView:(UIView *)view constrainedToRect:(CGRect)constrainedRect animated:(BOOL)animated
{
    if (![self.representedObject respondsToSelector:@selector(title)]) {
        return;
    }
    
    [view addSubview:self];
    
    if (self._isCalloutTappable) {
        [self._mainBtn addTarget:self action:@selector(_calloutTapped) forControlEvents:UIControlEventTouchUpInside];
    } else {
        self._mainBtn.userInteractionEnabled = NO;
    }
    
    originalFrame = self.frame;
    CGFloat frameWidth = self._mainBtn.bounds.size.width;
    CGFloat frameHeight = self._mainBtn.bounds.size.height + kTipHeight;
    CGFloat frameOriginX = rect.origin.x + (rect.size.width/2.0) - (frameWidth/2.0);
    CGFloat frameOriginY = rect.origin.y - frameHeight;
    self.frame = CGRectMake(frameOriginX, frameOriginY, frameWidth, frameHeight);
    
    if (animated) {
        self.alpha = 0.0;
        [UIView animateWithDuration:0.2 animations:^{
            self.alpha = 1.0;
        }];
    }
}

- (void)dismissCalloutAnimated:(BOOL)animated
{
    if (self.superview) {
        if (animated) {
            [UIView animateWithDuration:0.2 animations:^{
                self.alpha = 0.0;
            } completion:^(BOOL finished) {
                [self removeFromSuperview];
                self.frame = originalFrame;
            }];
        } else {
            [self removeFromSuperview];
            self.frame = originalFrame;
        }
    }
}

- (BOOL)dismissesAutomatically
{
    return NO;
}

- (BOOL)isAnchoredToAnnotation
{
    return YES;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupViewWithTitle:(NSString *)title inventoryCode:(NSString *)inventoryCode vin:(NSString *)vin
              batteryLevel:(NSString *)batteryLevel imageUrl:(NSString *)imageUrl
{
    // Image
    [self._imageView sd_addActivityIndicator];
    [self._imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                       placeholderImage:[UIImage imageNamed:@"img-inventory-placeholder"]
                                options:SDWebImageRetryFailed|SDWebImageRefreshCached|SDWebImageContinueInBackground
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
    
    // Title
    self._titlelbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    
    // Inventory Code
    self._inventoryCodeLbl.text = ![Utils stringIsNullOrEmpty:inventoryCode] ? [inventoryCode uppercaseString] : @"-";
    
    // Vin Number
    self._vinLbl.text = ![Utils stringIsNullOrEmpty:vin] ? [vin uppercaseString] : @"-";
    
    // Battery Level
    CGFloat level = [BusinessUtils getBatteryLevel:batteryLevel];
    self._batteryIconImgView.image = [BusinessUtils getBatteryIconImage:level];
    self._batteryIconImgView.tintColor = [BusinessUtils getBatteryLevelColor:level];
    self._batteryLevelLbl.text = [[BusinessUtils getBatteryLevelString:level] uppercaseString];
    self._batteryLevelLbl.textColor = [BusinessUtils getBatteryLevelColor:level];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (void)setCenter:(CGPoint)center
{
    center.y = center.y - CGRectGetMidY(self.bounds);
    [super setCenter:center];
}

- (void)drawRect:(CGRect)rect
{
    UIColor *fillColor = [UIColor whiteColor];

    CGFloat tipLeft = rect.origin.x + (rect.size.width / 2.0) - (kTipWidth / 2.0);
    CGPoint tipBottom = CGPointMake(rect.origin.x + (rect.size.width / 2.0), rect.origin.y + rect.size.height);
    CGFloat heightWithoutTip = rect.size.height - kTipHeight - 1;

    CGContextRef currentContext = UIGraphicsGetCurrentContext();

    CGMutablePathRef tipPath = CGPathCreateMutable();
    CGPathMoveToPoint(tipPath, NULL, tipLeft, heightWithoutTip);
    CGPathAddLineToPoint(tipPath, NULL, tipBottom.x, tipBottom.y);
    CGPathAddLineToPoint(tipPath, NULL, tipLeft + kTipWidth, heightWithoutTip);
    CGPathCloseSubpath(tipPath);

    [fillColor setFill];
    CGContextAddPath(currentContext, tipPath);
    CGContextFillPath(currentContext);
    CGPathRelease(tipPath);
}

@end
