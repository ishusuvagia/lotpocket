
#import "InfoTableCell.h"

@interface InfoTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *_informationLbl;

@end

@implementation InfoTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 64.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title information:(NSString *)information
               customColor:(UIColor *)customColor showDisclosure:(BOOL)showDisclosure
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    self._informationLbl.text = ![Utils stringIsNullOrEmpty:information] ? information : @"-";
    if (customColor) {
        self._informationLbl.textColor = customColor;
    } else {
        self._informationLbl.textColor = [AppColorScheme darkGray];
    }
    
    if (showDisclosure) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
