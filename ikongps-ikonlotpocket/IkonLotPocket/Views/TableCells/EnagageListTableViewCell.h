//
//  EnagageListTableViewCell.h
//  IkonLotPocket
//
//  Created by Samir imac4 on 18/04/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EnagageListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property(weak,nonatomic) IBOutlet UIImageView *imgIndicator;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

+ (CGFloat)height;
- (void)setupCellWithTitle:(NSString *)title description:(NSString *)description imageUrl:(NSString *)imageUrl;

@end

NS_ASSUME_NONNULL_END
