
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithLatitude:(CGFloat)latitude longitude:(CGFloat)longitude annotationTitle:(nullable NSString *)annotationTitle;

@end

NS_ASSUME_NONNULL_END
