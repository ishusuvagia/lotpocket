//
//  SideMenuTableViewCell.m
//  IkonLotPocket
//
//  Created by BAPS on 21/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "SideMenuTableViewCell.h"

@interface SideMenuTableViewCell ()



@end

@implementation SideMenuTableViewCell


///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 45.0f;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}
+ (NSString *)reuseIdentifier {
    return @"CustomCellIdentifier";
}



@end
