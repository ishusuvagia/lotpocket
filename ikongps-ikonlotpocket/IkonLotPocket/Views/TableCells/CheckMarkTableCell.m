
#import "CheckMarkTableCell.h"

@interface CheckMarkTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;

@end

@implementation CheckMarkTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 56.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title colorCode:(UIColor *)colorCode isSelected:(BOOL)isSelected;
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? title : kEmptyString;
    self._titleLbl.textColor = colorCode ?: [AppColorScheme brandBlack];
    
    if (isSelected) {
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
