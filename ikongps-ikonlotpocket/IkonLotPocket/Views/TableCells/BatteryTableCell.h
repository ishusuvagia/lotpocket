
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BatteryTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithBatteryLevel:(nullable NSString *)batteryLevel;

@end

NS_ASSUME_NONNULL_END
