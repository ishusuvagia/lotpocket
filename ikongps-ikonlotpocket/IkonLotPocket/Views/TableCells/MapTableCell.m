
#import <Mapbox/Mapbox.h>
#import "MapBoxPointAnnotation.h"
#import "MapTableCell.h"

static NSString *const kAnnotationIdentifier = @"annotation_dotview";

@interface MapTableCell () <MGLMapViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *_latitudeTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *_latitudeLbl;
@property (weak, nonatomic) IBOutlet UILabel *_longitudeTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *_longitudeLbl;
@property (weak, nonatomic) IBOutlet MGLMapView *_mapView;

@end

@implementation MapTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 240.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self._latitudeTitleLbl.text = [NSString stringWithFormat:@"%@:", LOCALIZED(@"latitude")];
    self._longitudeTitleLbl.text = [NSString stringWithFormat:@"%@:", LOCALIZED(@"longitude")];
    self._mapView.userInteractionEnabled = NO;
    self._mapView.styleURL = [MGLStyle satelliteStyleURL];
    self._mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self._mapView.logoView.hidden = YES;
    self._mapView.attributionButton.hidden = YES;
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img-logo-white"]];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoImageView.clipsToBounds = YES;
    logoImageView.frame = CGRectMake(0.0f, 0.0f, self._mapView.logoView.frame.size.width, self._mapView.frame.size.height);
    logoImageView.center = CGPointMake(58.5f, self._mapView.frame.size.height - 24.0f);
    [self._mapView addSubview:logoImageView];
    
    self._mapView.delegate = self;
}

///--------------------------------------
#pragma mark - <MGLMapViewDelegate>
///--------------------------------------

- (MGLAnnotationImage *)mapView:(MGLMapView *)mapView imageForAnnotation:(id <MGLAnnotation>)annotation {
    
    if ([annotation isKindOfClass:MapboxPointAnnotation.class]) {
        MapboxPointAnnotation *mapboxAnnotation = (MapboxPointAnnotation *)annotation;
        if (!mapboxAnnotation.willUseImage) {
            return nil;
        }
    }
    
    MGLAnnotationImage *annotationImage = [mapView dequeueReusableAnnotationImageWithIdentifier:kAnnotationIdentifier];
    if (!annotationImage) {
        UIImage *image = [UIImage imageNamed:@"img-inventory-annotation"];
        image = [image imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, image.size.height/2, 0)];
        annotationImage = [MGLAnnotationImage annotationImageWithImage:image reuseIdentifier:kAnnotationIdentifier];
    }
    
    return annotationImage;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithLatitude:(CGFloat)latitude longitude:(CGFloat)longitude annotationTitle:(NSString *)annotationTitle
{
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);

    self._latitudeLbl.text = [NSString stringWithFormat:@"%.2f°", latitude];
    self._longitudeLbl.text = [NSString stringWithFormat:@"%.2f°", longitude];
    
    [self._mapView removeAnnotations:self._mapView.annotations];
    MapboxPointAnnotation *annotation = [MapboxPointAnnotation new];
    annotation.title = annotationTitle;
    annotation.coordinate = coordinate;
    annotation.willUseImage = YES;
    [self._mapView addAnnotation:annotation];
    [self._mapView setCenterCoordinate:coordinate zoomLevel:kDefaultMapZoom animated:YES];
}

@end
