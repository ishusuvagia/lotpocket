
#import "BatteryTableCell.h"

@interface BatteryTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *_batteryIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *_batteryLevelLbl;

@end

@implementation BatteryTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 75.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    self._titleLbl.text = [NSString stringWithFormat:@"%@:", LOCALIZED(@"batterylevel")];
}

- (void)setupCellWithBatteryLevel:(NSString *)batteryLevel
{
    CGFloat level = [BusinessUtils getBatteryLevel:batteryLevel];
    self._batteryIconImgView.image = [BusinessUtils getBatteryIconImage:level];
    self._batteryIconImgView.tintColor = [BusinessUtils getBatteryLevelColor:level];
    self._batteryLevelLbl.text = [[BusinessUtils getBatteryLevelString:level] uppercaseString];
    self._batteryLevelLbl.textColor = [BusinessUtils getBatteryLevelColor:level];
}

@end
