
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InventoryTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithTitle:(nullable NSString *)title condition:(nullable NSString *)condition
             inventoryCode:(nullable NSString *)inventoryCode vin:(nullable NSString *)vin
                 msrpPrice:(NSInteger)msrpPrice mileage:(NSInteger)mileage
             exteriorColor:(nullable NSString *)exteriorColor interiorColor:(nullable NSString *)interiorColor
           deviceInstalled:(BOOL)deviceInstalled
              batteryLevel:(nullable NSString *)batteryLevel
                  imageUrl:(nullable NSString *)imageUrl
                  condition:(nullable NSString *)inventoryCondition;

@end

NS_ASSUME_NONNULL_END
