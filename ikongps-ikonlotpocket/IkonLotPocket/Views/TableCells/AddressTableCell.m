
#import "AddressTableCell.h"
#import "UILabel+VerticalAlignment.h"

@interface AddressTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *_addressLbl;

@end

@implementation AddressTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 86.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    self._addressLbl.textVerticalAlignment = TextVerticalAlignmentTop;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title address:(NSString *)address
               customColor:(UIColor *)customColor showDisclosure:(BOOL)showDisclosure
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    self._addressLbl.text = ![Utils stringIsNullOrEmpty:address] ? address : @"-";
    if (customColor) {
        self._addressLbl.textColor = customColor;
    } else {
        self._addressLbl.textColor = [AppColorScheme darkGray];
    }
    
    if (showDisclosure) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
