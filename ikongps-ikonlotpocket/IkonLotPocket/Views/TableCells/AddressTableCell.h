
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddressTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithTitle:(nullable NSString *)title address:(nullable NSString *)address
               customColor:(nullable UIColor *)customColor showDisclosure:(BOOL)showDisclosure;

@end

NS_ASSUME_NONNULL_END
