
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RoundTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithTitle:(nullable NSString *)title colorCode:(nullable UIColor *)colorCode;

@end

NS_ASSUME_NONNULL_END
