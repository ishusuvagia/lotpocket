//
//  SideMenuTableViewCell.h
//  IkonLotPocket
//
//  Created by BAPS on 21/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InventoryDetailsNewViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface SideMenuTableViewCell : UITableViewCell
+ (CGFloat)height;
+ (NSString *)reuseIdentifier;
- (void)setupCellWithTitle:(NSString *)title;
@property (strong, nonatomic) IBOutlet UILabel *_menutitleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *_menuImg;
@end

NS_ASSUME_NONNULL_END
