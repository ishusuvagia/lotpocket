
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CheckMarkTableCell : UITableViewCell

+ (CGFloat)height;
- (void)setupCellWithTitle:(nullable NSString *)title colorCode:(nullable UIColor *)colorCode isSelected:(BOOL)isSelected;

@end

NS_ASSUME_NONNULL_END
