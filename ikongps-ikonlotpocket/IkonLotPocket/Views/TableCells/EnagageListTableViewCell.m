//
//  EnagageListTableViewCell.m
//  IkonLotPocket
//
//  Created by Samir imac4 on 18/04/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "EnagageListTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <QuartzCore/QuartzCore.h>

@implementation EnagageListTableViewCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 120.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.imgProfile sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.lblName.text = @"bhoomi";
    self.lblDescription.text = @"Referral through current customer";
    [UITools dropShadow:self shadowOpacity:1.0f shadowRadius:0.5f offset:CGSizeZero];
    
  
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title description:(NSString *)description imageUrl:(NSString *)imageUrl
{
    // Image
    [self.imgProfile sd_addActivityIndicator];
    [self.imgProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                       placeholderImage:[UIImage imageNamed:@"img-inventory-placeholder"]
                                options:SDWebImageRetryFailed|SDWebImageRefreshCached|SDWebImageContinueInBackground
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
    
    // Title
    self.lblName.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
    
    // Name
    self.lblDescription.text = ![Utils stringIsNullOrEmpty:description] ? [Utils capitalizedOnlyFirstLetter:description]  : @"-";
    
   
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
