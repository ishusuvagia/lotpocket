
#import "RoundTableCell.h"
#import "AppColorScheme.h"
#import "CheckMark.h"
#import "UITools.h"
#import "Utils.h"

@interface RoundTableCell ()

@property (weak, nonatomic) IBOutlet UIView *_containerView;
@property (weak, nonatomic) IBOutlet UILabel *_titleLbl;
@property (weak, nonatomic) IBOutlet UIView *_colorCodeIndicatorView;

@end

@implementation RoundTableCell

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (CGFloat)height
{
    return 100.0f;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentView.backgroundColor = [AppColorScheme clear];
    self.backgroundColor = [AppColorScheme clear];
    self._containerView.backgroundColor = [AppColorScheme clear];
    [UITools roundView:self._containerView cornerRadius:3.0f];
    self._titleLbl.layer.cornerRadius=self._titleLbl.frame.size.height/2;
    self._titleLbl.clipsToBounds=YES;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setupCellWithTitle:(NSString *)title colorCode:(UIColor *)colorCode
{
    self._titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? [title uppercaseString] : @"-";
    self._titleLbl.textColor = [AppColorScheme blue];
    self._colorCodeIndicatorView.backgroundColor = colorCode ?: [AppColorScheme blue];
}

@end
