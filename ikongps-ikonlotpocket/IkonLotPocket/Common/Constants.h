
#import <UIKit/UIKit.h>

///--------------------------------------
/// @name Commons
///--------------------------------------

#define LOCALIZEDFORMAT(fmt, ...) [NSString stringWithFormat:NSLocalizedString(fmt, nil), __VA_ARGS__]
#define LOCALIZED(str) NSLocalizedString(str, @"")
#define HOMEDIRECTORY [NSProcessInfo processInfo].environment[@"HOME"]
#define DATABASE_SCHEMA_VERSION 1
#define GET_SERVICE(clazz) (clazz *)[PersistenceContainer getService:[clazz class]];
#define INIT_SERVICE(clazz) (clazz *)[PersistenceContainer getService:[clazz class]];
#define INIT_CONTROLLER_XIB(clazz) [[clazz alloc] initWithNibName:NSStringFromClass([clazz class]) bundle:nil]

FOUNDATION_EXPORT CGFloat const kDefaultMapZoomNear;
FOUNDATION_EXPORT CGFloat const kDefaultMapZoom;
FOUNDATION_EXPORT CGFloat const kDefaultMapZoomFar;
FOUNDATION_EXPORT CGFloat const kBarButtonDefaultWidth;
FOUNDATION_EXPORT CGFloat const kBarButtonDefaultHeight;
FOUNDATION_EXPORT CGFloat const kNavigationBarDefaultHeight;
FOUNDATION_EXPORT NSString *const __nonnull kEmptyString;
FOUNDATION_EXPORT CGFloat const kGoldenRatio;

///--------------------------------------
/// @name Date Format
///--------------------------------------

FOUNDATION_EXPORT NSString *const __nonnull kFormatDate;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateTime;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateTimeShort;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateTimeLong;
FOUNDATION_EXPORT NSString *const __nonnull kFormatUSDateTime24;
FOUNDATION_EXPORT NSString *const __nonnull kFormatUSDateTimeAMPM;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateUTC;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateISO8601Short;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateISO8601;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateISO8601UTC;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateGMT;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDayOfWeekWithDate;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDayOfWeekWithDateTime;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDayMonthShort;
FOUNDATION_EXPORT NSString *const __nonnull kFormatMonthYearShort;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateOnly;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDayOfWeekShort;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDayOfWeekLong;
FOUNDATION_EXPORT NSString *const __nonnull kFormatTimeHourMinute;
FOUNDATION_EXPORT NSString *const __nonnull k24FormatTimeHourMinute;
FOUNDATION_EXPORT NSString *const __nonnull k12FormatTimeHourMinute;
FOUNDATION_EXPORT NSString *const __nonnull kFormatDateUS;

///--------------------------------------
/// @name Segues & Storyboards
///--------------------------------------

FOUNDATION_EXPORT NSString *const __nonnull kStoryboardMain;

FOUNDATION_EXPORT NSString *const __nonnull kSegueLogin;
FOUNDATION_EXPORT NSString *const __nonnull kSegueLoginForm;
FOUNDATION_EXPORT NSString *const __nonnull kSegueLanding;

FOUNDATION_EXPORT NSString *const __nonnull kSceneLogin;
FOUNDATION_EXPORT NSString *const __nonnull kSceneLanding;

///--------------------------------------
/// @name Preferences Keys
///--------------------------------------

FOUNDATION_EXPORT NSString *const __nonnull kPrefUsernameKey;
FOUNDATION_EXPORT NSString *const __nonnull kPrefUserIdKey;
FOUNDATION_EXPORT NSString *const __nonnull kPrefPasswordKey;
FOUNDATION_EXPORT NSString *const __nonnull kPrefPictureUrlKey;
FOUNDATION_EXPORT NSString *const __nonnull kPrefIsAccountActivatedKey;
FOUNDATION_EXPORT NSString *const __nonnull kPrefSessionTokenKey;
FOUNDATION_EXPORT NSString *const __nonnull kPrefAccessTokenExpiredKey;
FOUNDATION_EXPORT NSString *const __nonnull kPrefAccessTokenCreatedKey;

///--------------------------------------
/// @name UITableView
///--------------------------------------

#define REGISTER_CELL(tableView, nibName, identifier) \
    [tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:identifier]

#define REUSECELL_INIT(tableView, cellClass, identifier) \
    [tableView dequeueReusableCellWithIdentifier:identifier] \
        ?: [[cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier]

FOUNDATION_EXPORT NSString *const __nonnull kCellTitleKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellInformationKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellPlaceholderKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellShowDisclosureIndicatorKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellColorCodeKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellIconImageKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellIdentifierKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellIsSelectedKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellNibNameKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellClassKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellHeightKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellObjectDataKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellTagKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellEnableInputKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellKeyboardTypeKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellTimePickerMode;
FOUNDATION_EXPORT NSString *const __nonnull kCellItemsKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellLabelsKey;
FOUNDATION_EXPORT NSString *const __nonnull kCellValuesKey;

FOUNDATION_EXPORT NSString *const __nonnull kSectionTitleKey;
FOUNDATION_EXPORT NSString *const __nonnull kSectionDataKey;
FOUNDATION_EXPORT CGFloat const kHeaderFooterPadding;
FOUNDATION_EXPORT CGFloat const kHeaderFooterHeight;

///--------------------------------------
/// @name Business
///--------------------------------------

FOUNDATION_EXPORT NSString *const __nonnull kExternal_SortBy_Model;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_SortBy_HighestPrice;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_SortBy_LowestPrice;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_SortBy_NewestYear;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_SortBy_OldestYear;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_SortBy_HighestMileage;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_SortBy_LowestMileage;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_FilterBy_Year;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_FilterBy_PriceRange;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_FilterBy_MilesRange;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_FilterBy_BodyStyle;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_FilterBy_Condition;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_FilterBy_Transmission;
FOUNDATION_EXPORT NSString *const __nonnull kExternal_FilterBy_FuelType;

///--------------------------------------
/// @name Notifications
///--------------------------------------

///--------------------------------------
/// @name Enums
///--------------------------------------

typedef NS_ENUM(NSInteger, ScrollDirection)
{
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
};

typedef NS_ENUM(NSInteger, NotificationType)
{
    Normal,
    Primary,
    Success,
    Warning,
    Error
};

typedef NS_ENUM(NSInteger, ModuleType)
{
    ModuleTypeInventory = 1,
    ModuleTypeEngage = 2,
    ModuleTypeSalesTool = 3,
    ModuleTypeVehicleLot = 4,
    ModuleTypeMap = 5,
    ModuleTypeReports = 6,
    ModuleTypeMaintenance = 7,
    ModuleTypeAlerts = 8,
    ModuleTypeDevices = 9,
    ModuleTypeGuard = 10
};

typedef NS_ENUM(NSInteger, CommandStatus)
{
    CommandStatus_Pending = 1,
    CommandStatus_Failed = 2,
    CommandStatus_Success = 3,
    CommandStatus_Expired = 4
};

///--------------------------------------
/// @name Custom Delegates
///--------------------------------------

@protocol ModuleDelegate <NSObject>
@required
- (nonnull NSString *)getTitle;
- (nonnull UIImage *)getDashboardIcon;
- (nonnull UIImage *)getTabbarIcon;
- (nonnull UIImage *)getTabbarSelectedIcon;
- (nonnull UIViewController *)getMainViewController;
- (nullable NSString *)getNavbarButtonTitle;
- (ModuleType)getModuleType;
- (BOOL)alwaysVisible;
@end

@protocol PushDelegate <NSObject, ModuleDelegate>
@required
- (nullable NSArray *)getRequiredPushTypes;
@required
- (nullable UIViewController *)getViewControllerForPush:(nullable NSString *)pushType withRecordId:(nullable NSNumber *)recordId;
@end

@protocol RestResponseDelegate <NSObject>
@optional
- (void)responseHandler:(nullable id)response;
- (void)responseHandler:(nullable id)response total:(NSInteger)total;
@optional
- (void)errorHandler:(nullable NSError *)error;
@end

@protocol SectionTableViewDelegate <NSObject>
@required
- (void)setupCell:(nonnull UITableViewCell *)cell withDictionary:(nullable NSDictionary *)cellDict
      atIndexPath:(nonnull NSIndexPath *)indexPath;
@required
- (void)didSelectCell:(nonnull UITableViewCell *)cell sectionTitle:(nullable NSString *)sectionTitle
       withDictionary:(nullable NSDictionary *)cellDict atIndexPath:(nonnull NSIndexPath *)indexPath;
@optional
- (void)refreshData;
- (void)loadMoreData;
@end

@protocol FormDelegate <NSObject>
@optional
- (void)didFocus;
- (void)isReadyToExecute;
- (void)isNotReadyToExecute;
- (void)updateDataForTag:(nonnull NSString *)tag withValue:(nullable NSString *)value;
@end

@protocol ScrollableTabbarDelegate <NSObject>
@optional
- (void)handleLeftNavigationBarButtonEvent;
- (void)handleRightNavigationBarButtonEvent;
@end

@protocol SelectionDelegate <NSObject>
@optional
- (void)updateSelectedValue:(nullable NSString *)value forKey:(nullable NSString *)key;
- (void)updateSelectedValues:(nullable NSArray *)values forKey:(nullable NSString *)key;
@end

///--------------------------------------
/// @name Blocks
///--------------------------------------

typedef void (^IdResultBlock)(__nullable id object, NSError *__nullable error);
typedef void (^IdPagedResultBlock)(__nullable id object, NSInteger total, NSError *__nullable error);
typedef void (^ErrorResultBlock)(NSInteger statusCode, NSString *__nullable statusMessage);
typedef void (^BooleanResultBlock)(BOOL success, NSError *__nullable error);
typedef void (^StringResultBlock)(NSString *__nullable string, NSError *__nullable error);
typedef void (^ProgressBlock)(NSProgress *__nullable progress);
