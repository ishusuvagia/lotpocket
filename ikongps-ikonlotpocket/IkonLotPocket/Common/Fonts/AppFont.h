
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define SYSTEMTYPEFACE(fontSize, fontWeight) { return [UIFont systemFontOfSize:fontSize weight:fontWeight]; }

@interface AppFont : NSObject

+ (nonnull UIFont *)dashboardTileFont;
+ (nonnull UIFont *)statusBarTitleFont;
+ (nonnull UIFont *)navigationBarTitleFont;
+ (nonnull UIFont *)navigationBarSubtitleFont;
+ (nonnull UIFont *)largeNavigationBarTitleFont;
+ (nonnull UIFont *)tabbarTitleFont;
+ (nonnull UIFont *)tabbarSelectedTitleFont;
+ (nonnull UIFont *)toolBarTitleFont;
+ (nonnull UIFont *)segmentTitleFont;
+ (nonnull UIFont *)tableHeaderFooterFont;
+ (nonnull UIFont *)buttonTitleFont;
+ (nonnull UIFont *)smallButtonTitleFont;
+ (nonnull UIFont *)textFieldFont;
+ (nonnull UIFont *)textViewFont;
+ (nonnull UIFont *)alertTitleFont;
+ (nonnull UIFont *)alertMessageFont;
+ (nonnull UIFont *)titleLabelFont;
+ (nonnull UIFont *)labelFont;
+ (nonnull UIFont *)statusFont;
+ (nonnull UIFont *)calendarTitleFont;
+ (nonnull UIFont *)calendarSubtitleFont;
+ (nonnull UIFont *)calendarWeekdayFont;
+ (nonnull UIFont *)calendarHeaderFont;
+ (nonnull UIFont *)boldFontWithSize:(CGFloat)size;

@end

NS_ASSUME_NONNULL_END
