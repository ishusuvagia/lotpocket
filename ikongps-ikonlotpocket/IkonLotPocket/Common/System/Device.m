
#import <SAMKeychain/SAMKeychain.h>
#import "Device.h"
#include <sys/sysctl.h>

static NSString *DeviceSysctlByName(NSString *name)
{
    const char *charName = [name UTF8String];
    size_t size;

    sysctlbyname(charName, NULL, &size, NULL, 0);
    char *answer = (char *) malloc(size);
    if (answer == NULL) {
        return nil;
    }

    sysctlbyname(charName, answer, &size, NULL, 0);
    NSString *string = [NSString stringWithUTF8String:answer];
    free(answer);

    return string;
}

@implementation Device

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

+ (instancetype)currentDevice
{
    static Device *device;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{device = [Device new];});
    return device;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Device: <Model: %@,  OS Version: %@,   Build: %@,   UUID: %@>",
                                      self.detailedModel, self.osFullVersion, self.osBuild, self.deviceUUID];
}

///--------------------------------------
#pragma mark - Accessors
///--------------------------------------

- (NSString *)detailedModel
{
    NSString *name = DeviceSysctlByName(@"hw.machine");
    if (!name) {
        name = [UIDevice currentDevice].model;
    }
    return name;
}

- (NSString *)osFullVersion
{
    return [UIDevice currentDevice].systemVersion;
}

- (NSString *)osBuild
{
    return DeviceSysctlByName(@"kern.osversion");
}

- (NSString *)deviceUUID
{
    NSString *appName = [[NSBundle mainBundle] infoDictionary][(NSString *) kCFBundleNameKey];
    NSString *uuid = [SAMKeychain passwordForService:appName account:@"incoding"];
    if (!uuid) {
        uuid = [UIDevice currentDevice].identifierForVendor.UUIDString;
        [SAMKeychain setPassword:uuid forService:appName account:@"incoding"];
    }

    return uuid;
}

@end
