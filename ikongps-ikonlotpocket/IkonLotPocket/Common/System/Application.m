
#import "Application.h"

@implementation Application

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

+ (instancetype)currentApp
{
    static Application *application;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ application = [Application new]; });
    return application;
}

- (NSString *)description
{
    return
            [NSString stringWithFormat:
                    @"Application: <BundleId: %@,  Version: %@,  Build Mode: %@,  AppStoreEnv: %@,  Badges: %ld>",
                    self.bundleId, self.version, self.buildMode, self.isAppStoreEnvironment ? @"YES" : @"NO", (long)self.iconBadgeNumber];
}

///--------------------------------------
#pragma mark - Accessors
///--------------------------------------

- (UIApplication *)systemApplication { return [UIApplication performSelector:@selector(sharedApplication)]; }

- (NSString *)bundleId { return [[NSBundle mainBundle] bundleIdentifier]; }

- (NSString *)version { return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]; }

- (NSString *)buildMode
{
#ifdef DEBUG
    return @"DEBUG";
#else
    return @"RELEASE";
#endif
}

- (BOOL)isAppStoreEnvironment
{
#if TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR
    return ([[NSBundle mainBundle] pathForResource:@"embedded" ofType:@"mobileprovision"] == nil);
#endif
    return NO;
}

- (BOOL)isProductionMode
{
#ifdef DEBUG
    return NO;
#else
    return YES;
#endif
}

- (NSInteger)iconBadgeNumber { return self.systemApplication.applicationIconBadgeNumber; }

- (void)setIconBadgeNumber:(NSInteger)iconBadgeNumber
{
    if (self.iconBadgeNumber != iconBadgeNumber)
    {
        self.systemApplication.applicationIconBadgeNumber = iconBadgeNumber;
    }
}

@end
