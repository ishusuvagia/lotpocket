
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define APPLICATION [Application currentApp]

@interface Application : NSObject

@property (nonnull, nonatomic, strong, readonly) UIApplication *systemApplication;
@property (nonnull, nonatomic, copy, readonly) NSString *bundleId;
@property (nonnull, nonatomic, copy, readonly) NSString *version;
@property (nonnull, nonatomic, copy, readonly) NSString *buildMode;
@property (nonatomic, assign, readonly, getter=isAppStoreEnvironment) BOOL appStoreEnvironment;
@property (nonatomic, assign, readonly, getter=isProductionMode) BOOL productionMode;
@property (nonatomic, assign) NSInteger iconBadgeNumber;

+ (instancetype)currentApp;

@end

NS_ASSUME_NONNULL_END
