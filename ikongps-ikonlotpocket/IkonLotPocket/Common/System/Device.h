
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define DEVICE [Device currentDevice]

@interface Device : NSObject

@property (nonnull, nonatomic, readonly) NSString *detailedModel;
@property (nonnull, nonatomic, readonly) NSString *osFullVersion;
@property (nonnull, nonatomic, readonly) NSString *osVersion;
@property (nonnull, nonatomic, readonly) NSString *osBuild;
@property (nonnull, nonatomic, readonly) NSString *deviceUUID;
@property (nullable, nonatomic, strong) NSString *deviceToken;

+ (instancetype)currentDevice;

@end

NS_ASSUME_NONNULL_END
