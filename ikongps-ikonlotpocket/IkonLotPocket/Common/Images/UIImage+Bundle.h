
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Bundle)

+ (nonnull UIImage *)imageInBundleNamed:(nullable NSString *)name;

@end

NS_ASSUME_NONNULL_END
