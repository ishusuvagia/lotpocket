#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Crop)

+ (nonnull UIImage *)screenshotFromView:(nullable UIView *)view;
+ (nonnull UIImage *)createRoundedRectImage:(nullable UIImage *)image size:(CGSize)size roundRadius:(CGFloat)radius;
+ (nonnull UIImage *)returnImage:(nullable UIImage *)img withSize:(CGSize)finalSize;
- (nonnull UIImage *)croppedImage:(CGRect)cropRect;
- (nonnull UIImage *)rotateUIImage;

@end

NS_ASSUME_NONNULL_END
