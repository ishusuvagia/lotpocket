
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Tint)

- (nonnull UIImage *)tintImageWithColor:(nullable UIColor *)tintColor;

@end

NS_ASSUME_NONNULL_END
