
#import "AppColorScheme.h"

@implementation AppColorScheme

#define RGB(r, g, b) \
{ \
    static UIColor *rgb; \
    if(!rgb) \
    rgb = [UIColor colorWithRed: r / 255.0 green: g / 255.0 blue: b / 255.0 alpha: 1.0]; \
    return rgb; \
}

#define RGBA(r, g, b, a) \
{ \
    static UIColor *rgb; \
    if(!rgb) \
    rgb = [UIColor colorWithRed: r / 255.0 green: g / 255.0 blue: b / 255.0 alpha: a]; \
    return rgb; \
}

///--------------------------------------
#pragma mark - Primary Color Palette
///--------------------------------------

+ (UIColor *)normal RGB(235, 237, 235)

+ (UIColor *)primary RGB(49, 130, 217)

+ (UIColor *)success RGB(46, 177, 135)

+ (UIColor *)warning RGB(217, 108, 0)

+ (UIColor *)error RGB(201, 63, 69)

///--------------------------------------
#pragma mark - Light and Dark Color Palette
///--------------------------------------

+ (UIColor *)white RGB(255, 255, 255)

+ (UIColor *)lightWhite RGB(232, 236, 238)

+ (UIColor *)silver RGB(235, 237, 235)

+ (UIColor *)darkWhite RGB(176, 182, 187)

+ (UIColor *)powerBlueLight RGB(216, 223, 252)

+ (UIColor *)powerBlueDark RGB(181, 192, 232)

+ (UIColor *)black RGB(0, 0, 0)

+ (UIColor *)blackDarkGray RGB(32, 32, 32)

+ (UIColor *)pewter RGB(83, 83, 83)

+ (UIColor *)lightDarkGray RGB(106, 106, 106)

+ (UIColor *)darkGray RGB(109, 121, 122)

+ (UIColor *)lightGray RGB(132, 148, 149)

+ (UIColor *)blue RGB(33, 83, 144)

+ (UIColor *)lightBlue RGB(9, 169, 236)

+ (UIColor *)darkBlue RGB(44, 56, 114)

+ (UIColor *)navyBlue RGB(40, 56, 77)

+ (UIColor *)red RGB(234, 4, 4)

+ (UIColor *)lightRed RGB(227, 90, 102)

+ (UIColor *)orange RGB(255, 38, 0)

+ (UIColor *)lightOrange RGB(252, 143, 35)

+ (UIColor *)transparentBlack RGBA(0, 0, 0, 0.5)

+ (UIColor *)green RGB(10, 146, 0)

+ (UIColor *)lightGreen RGB(59, 198, 81)

+ (UIColor *)purple RGB(144, 19, 254)

+ (UIColor *)pink RGB(233, 96, 187)

+ (UIColor *)yellow RGB(251, 199, 0)

+ (UIColor *)clear RGBA(0, 0, 0, 0)

///--------------------------------------
#pragma mark - Brand Colors
///--------------------------------------

+ (UIColor *)brandBlack RGB(37, 37, 37)

+ (UIColor *)brandDarkBlue RGB(57, 63, 75)

+ (UIColor *)brandBlue RGB(49, 130, 217)

+ (UIColor *)brandLightBlue RGB(0, 174, 239)

+ (UIColor *)brandGreen RGB(48, 173, 99)

+ (UIColor *)brandGray RGB(109, 121, 122)

+ (UIColor *)brandOrange RGB(217, 108, 0)

+ (UIColor *)brandRed RGB(189, 10, 27)

+ (UIColor *)brandPlaceholder RGB(199, 199, 205)

+ (UIColor *)brandWhite RGB(255, 255, 255)

@end
