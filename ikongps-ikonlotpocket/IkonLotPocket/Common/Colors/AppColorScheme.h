
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppColorScheme : NSObject

///--------------------------------------
/// @name Primary Color Palette
///--------------------------------------

+ (nonnull UIColor *)normal;
+ (nonnull UIColor *)primary;
+ (nonnull UIColor *)success;
+ (nonnull UIColor *)warning;
+ (nonnull UIColor *)error;

///--------------------------------------
/// @name Light and Dark Color Palette
///--------------------------------------

+ (nonnull UIColor *)white;
+ (nonnull UIColor *)lightWhite;
+ (nonnull UIColor *)silver;
+ (nonnull UIColor *)darkWhite;
+ (nonnull UIColor *)powerBlueLight;
+ (nonnull UIColor *)powerBlueDark;
+ (nonnull UIColor *)black;
+ (nonnull UIColor *)blackDarkGray;
+ (nonnull UIColor *)pewter;
+ (nonnull UIColor *)lightDarkGray;
+ (nonnull UIColor *)darkGray;
+ (nonnull UIColor *)lightGray;
+ (nonnull UIColor *)blue;
+ (nonnull UIColor *)lightBlue;
+ (nonnull UIColor *)darkBlue;
+ (nonnull UIColor *)navyBlue;
+ (nonnull UIColor *)red;
+ (nonnull UIColor *)lightRed;
+ (nonnull UIColor *)orange;
+ (nonnull UIColor *)lightOrange;
+ (nonnull UIColor *)transparentBlack;
+ (nonnull UIColor *)green;
+ (nonnull UIColor *)lightGreen;
+ (nonnull UIColor *)purple;
+ (nonnull UIColor *)pink;
+ (nonnull UIColor *)yellow;
+ (nonnull UIColor *)clear;

///--------------------------------------
/// @name - Brand Colors
///--------------------------------------

+ (nonnull UIColor *)brandBlack;
+ (nonnull UIColor *)brandDarkBlue;
+ (nonnull UIColor *)brandBlue;
+ (nonnull UIColor *)brandLightBlue;
+ (nonnull UIColor *)brandGreen;
+ (nonnull UIColor *)brandGray;
+ (nonnull UIColor *)brandOrange;
+ (nonnull UIColor *)brandRed;
+ (nonnull UIColor *)brandPlaceholder;
+ (nonnull UIColor *)brandWhite;

@end

NS_ASSUME_NONNULL_END
