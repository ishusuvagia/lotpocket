
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThemeManager : NSObject

+ (void)setDefaultTheme;
+ (void)setNavigationBarDefaultTheme;
+ (void)setNavigationBarTransparentTheme;

@end

NS_ASSUME_NONNULL_END
