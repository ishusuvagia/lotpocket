
#import "RestRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface RestFileUploadRequest : RestRequest

@property (nonnull, nonatomic, copy, readonly) NSString *fileKeyName;
@property (nonnull, nonatomic, copy, readonly) NSString *fileName;
@property (nonnull, nonatomic, copy, readonly) NSString *mimeType;
@property (nonnull, nonatomic, copy, readonly) NSDictionary *parameters;

+ (nonnull instancetype)fileUploadRequestWithHttpPath:(nonnull NSString *)path
                                          fileKeyName:(nullable NSString *)fileKeyName
                                             fileName:(nullable NSString *)fileName
                                             mimeType:(nullable NSString *)mimeType
                                           parameters:(nullable NSDictionary *)parameters
                                             bodyData:(nonnull id)bodyData;
+ (nonnull instancetype)fileUploadRequestWithHttpPath:(nonnull NSString *)path
                                          fileKeyName:(nullable NSString *)fileKeyName
                                             fileName:(nullable NSString *)fileName
                                             mimeType:(nullable NSString *)mimeType
                                    additionalHeaders:(nullable NSDictionary *)additionalHeaders
                                           parameters:(nullable NSDictionary *)parameters
                                             bodyData:(nonnull id)bodyData;

@end

NS_ASSUME_NONNULL_END
