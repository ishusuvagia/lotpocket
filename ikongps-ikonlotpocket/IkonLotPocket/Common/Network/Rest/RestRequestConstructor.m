
#import <AFNetworking/AFNetworking.h>
#import "RestRequestConstructor.h"
#import "HttpCommon.h"
#import "CommonAssert.h"
#import "Logger.h"

@implementation RestRequestConstructor

+ (NSMutableURLRequest *)urlRequestWithURL:(NSString *)url
                                httpMethod:(NSString *)httpMethod
                               httpHeaders:(NSDictionary *)httpHeaders
                                  httpBody:(id)bodyData
{
    NSParameterAssert(url != nil);
    NSParameterAssert(httpMethod != nil);
    
    // Initialize request with provided URL
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // HTTP Request Method
    request.HTTPMethod = httpMethod;
    
    // HTTP Request Headers
    NSMutableDictionary *allHeaderFields = [NSMutableDictionary dictionary];
    allHeaderFields[HTTPRequestHeaderNameContentEncoding] = HTTPURLRequestContentEncodingGZIP;
    allHeaderFields[HTTPRequestHeaderNameAccept] = HTTPURLRequestContentTypeJSON;
    allHeaderFields[HTTPRequestHeaderApplicationType] = HTTPURLRequestApplicationTypeREST;
    
    // Additional HTTP Headers (if any)
    if (httpHeaders)
        [allHeaderFields addEntriesFromDictionary:httpHeaders];
    
    // HTTP Body
    if ([bodyData isKindOfClass:[NSDictionary class]]) {
        ConsistencyAssert(
                          [httpMethod isEqualToString:HTTPRequestMethodPOST] || [httpMethod isEqualToString:HTTPRequestMethodPUT],
                          @"Can't create %@ request with json body.", httpMethod);
        allHeaderFields[HTTPRequestHeaderNameContentType] = HTTPURLRequestContentTypeJSON;
        
        NSError *error = nil;
        [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:bodyData options:(NSJSONWritingOptions) 0
                                                               error:&error]];
        
        ConsistencyAssert(error == nil, @"Failed to serialize JSON with error = %@", error);
        
        TRACE("%@ JSON Request: %@\nHeader Params:\n%@\nBody Params:\n%@", request.HTTPMethod,
              request.URL.absoluteString,
              request.allHTTPHeaderFields, bodyData);
    }
    
    request.allHTTPHeaderFields = allHeaderFields;
    
    return request;
}

+ (NSMutableURLRequest *)fileUploadUrlRequestwithURL:(NSString *)url
                                          httpMethod:(NSString *)httpMethod
                                         httpHeaders:(NSDictionary *)httpHeaders
                                         fileKeyName:(NSString *)fileKeyName
                                            fileName:(NSString *)fileName
                                            mimeType:(NSString *)mimeType
                                          parameters:(NSDictionary *)parameters
                                            httpBody:(id)bodyData
{
    NSParameterAssert(url != nil);
    NSParameterAssert(httpMethod != nil);
    
    NSMutableURLRequest *request =
    [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:httpMethod
                                                               URLString:url
                                                              parameters:parameters
                                               constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
                                                   [formData appendPartWithFileData:bodyData
                                                                               name:fileKeyName
                                                                           fileName:fileName
                                                                           mimeType:mimeType];
                                               } error:nil];
    
    // HTTP Request Headers
    NSMutableDictionary *allHeaderFields = [NSMutableDictionary dictionary];
    allHeaderFields[HTTPRequestHeaderNameContentEncoding] = HTTPURLRequestContentEncodingGZIP;
    allHeaderFields[HTTPRequestHeaderNameAccept] = HTTPURLRequestContentTypeJSON;
    
    // Additional HTTP Headers (if any)
    if (httpHeaders)
        [allHeaderFields addEntriesFromDictionary:httpHeaders];
    
    NSMutableDictionary *dict = [request.allHTTPHeaderFields mutableCopy];
    [dict addEntriesFromDictionary:allHeaderFields];
    request.allHTTPHeaderFields = dict;
    
    return request;
}

@end
