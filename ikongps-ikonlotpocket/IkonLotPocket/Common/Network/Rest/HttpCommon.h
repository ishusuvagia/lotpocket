
#import <Foundation/Foundation.h>

#ifndef HttpCommon_h
#define HttpCommon_h

///--------------------------------------
/// @name HTTP Methods
///--------------------------------------

static NSString *const HTTPRequestMethodGET = @"GET";
static NSString *const HTTPRequestMethodHEAD = @"HEAD";
static NSString *const HTTPREquestMethodDELETE = @"DELETE";
static NSString *const HTTPRequestMethodPOST = @"POST";
static NSString *const HTTPRequestMethodPUT = @"PUT";
static NSString *const HTTPRequestMethodPATCH = @"PATCH";

///--------------------------------------
/// @name HTTP Headers
///--------------------------------------

static NSString *const HTTPRequestHeaderNameContentType = @"Content-Type";
static NSString *const HTTPRequestHeaderNameContentLength = @"Content-Length";
static NSString *const HTTPRequestHeaderNameContentEncoding = @"Content-Encoding";
static NSString *const HTTPRequestHeaderNameAccept = @"Accept";
static NSString *const HTTPRequestHeaderApplicationType = @"application-type";
static NSString *const HTTPRequestHeaderAuthorization = @"Authorization";

static NSString *const HTTPURLRequestContentEncodingGZIP = @"application/gzip";
static NSString *const HTTPURLRequestContentTypeJSON = @"application/json; charset=utf-8";
static NSString *const HTTPURLRequestContentTypeXML = @"application/xml; charset=utf-8";
static NSString *const HTTPURLRequestContentTypeIMAGE = @"image/*";
static NSString *const HTTPURLRequestApplicationTypeREST = @"REST";
static NSString *const HTTPURLRequestContentTypeXWFORMURLENCODED = @"application/x-www-form-urlencoded";
static NSString *const HTTPURLRequestContentTypeBINARYOCTETSTREAM = @"binary/octet-stream";
static NSString *const HTTPURLRequestContentTypeAPPLICATIONOCTEMSTREAM = @"application/octet-stream";
static NSString *const HTTPURLRequestContentTypeMULTIPARTFORMDATA = @"multipart/form-data";

///--------------------------------------
/// @name HTTP Status Codes
///--------------------------------------

static NSInteger const HTTPStatusCodeOK = 200;
static NSInteger const HTTPStatusCodeCREATED = 201;
static NSInteger const HTTPStatusCodeACCEPTED = 202;
static NSInteger const HTTPStatusCodeNOCONTENT = 204;
static NSInteger const HTTPStatusCodeMULTIPLECHOICES = 300;
static NSInteger const HTTPStatusCodeFORBIDDEN = 403;
static NSInteger const HTTPStatusCodeNotFound = 404;
static NSInteger const HTTPStatusCodeMETHODNOTALLOWED = 405;
static NSInteger const HTTPSTatusCodeCONFLICT = 409;
static NSInteger const HTTPStatusCodeINTERNALERROR = 500;

///--------------------------------------
/// @name Mime Types
///--------------------------------------

static NSString *const HTTPMimeTypeTEXTPLAIN = @"text/plain";
static NSString *const HTTPMimeTypeTEXTHTML = @"text/html";
static NSString *const HTTPMimeTypeIMAGEJPEG = @"image/jpeg";
static NSString *const HTTPMimeTypeIMAGEPNG = @"image/png";
static NSString *const HTTPMimeTypeAUDIOMPEG = @"audio/mpeg";
static NSString *const HTTPMimeTypeAUDIOOGG = @"audio/ogg";

#endif
