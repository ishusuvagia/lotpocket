
#import "RestResponse.h"

@interface RestResponse ()

@property (nonatomic, strong, readwrite) id result;
@property (nonatomic, assign, readwrite) BOOL isSuccess;
@property (nonatomic, assign, readwrite) NSInteger headerStatusCode;
@property (nonatomic, copy, readwrite) NSDictionary *allHeaderFields;
@property (nonatomic, copy, readwrite) NSString *statusMessage;

@end

@implementation RestResponse

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (nonnull instancetype)reponseObjectWithResult:(id)result
                                      isSuccess:(BOOL)isSuccess
                                     statusCode:(NSInteger)statusCode
                                allHeaderFields:(NSDictionary *)allHeaderFields
                                  statusMessage:(NSString *)statusMessage
{
    RestResponse *restResponse = [RestResponse new];
    restResponse.result = result;
    restResponse.isSuccess = isSuccess;
    restResponse.headerStatusCode = statusCode;
    restResponse.allHeaderFields = allHeaderFields;
    restResponse.statusMessage = statusMessage;
    return restResponse;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
    }
    return self;
}

- (void)dealloc
{
    self.result = nil;
    self.allHeaderFields = nil;
    self.statusMessage = nil;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<Result=%@, Headers=%@, Status=%@",
                                      self.result, self.allHeaderFields, self.statusMessage];
}

@end
