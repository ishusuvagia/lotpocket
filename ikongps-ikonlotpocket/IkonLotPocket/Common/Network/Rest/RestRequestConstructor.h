
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RestRequestConstructor : NSObject

+ (nonnull NSMutableURLRequest *)urlRequestWithURL:(nullable NSString *)url
                                        httpMethod:(nonnull NSString *)httpMethod
                                       httpHeaders:(nullable NSDictionary *)httpHeaders
                                          httpBody:(nullable id)bodyData;

+ (nonnull NSMutableURLRequest *)fileUploadUrlRequestwithURL:(nullable NSString *)url
                                                  httpMethod:(nonnull NSString *)httpMethod
                                                 httpHeaders:(nullable NSDictionary *)httpHeaders
                                                 fileKeyName:(nullable NSString *)fileKeyName
                                                    fileName:(nullable NSString *)fileName
                                                    mimeType:(nullable NSString *)mimeType
                                                  parameters:(nullable NSDictionary *)parameters
                                                    httpBody:(nullable id)bodyData;


@end

NS_ASSUME_NONNULL_END
