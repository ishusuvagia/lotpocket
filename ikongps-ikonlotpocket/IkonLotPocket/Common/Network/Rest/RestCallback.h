
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class RestResponse;
typedef void (^RestResponseBlock)(RestResponse *);

@interface RestCallback : NSObject

@property (nonatomic, readonly) RestResponseBlock result;

+ (instancetype)callbackWithResult:(RestResponseBlock)result;

@end

NS_ASSUME_NONNULL_END
