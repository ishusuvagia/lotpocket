
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "HttpCommon.h"
#import "RestRequest.h"
#import "RestFileUploadRequest.h"
#import "RestResponse.h"
#import "RestCallback.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, RequestOptions)
{
    RequestOptionNoRetry = 0,
    RequestOptionRetryIfFailed = 1 << 0
};

@interface RestClient : NSObject

@property (nonatomic, assign) BOOL allowInvalidCertificates;
@property (nonatomic, assign) NSURLRequestCachePolicy requestCachePolicy;
@property (nonatomic, assign) NSTimeInterval requestTimeOut;
@property (nonatomic, assign) NSTimeInterval requestResourceTimeOut;
@property (nonatomic, assign) NSUInteger requestMaxAttemptsCount;

- (void)performRequest:(nonnull RestRequest *)restRequest downloadProgress:(nullable ProgressBlock)progressBlock callback:(nullable RestCallback *)callback;
- (void)performRequest:(nonnull RestRequest *)restRequest callback:(nullable RestCallback *)callback;
- (void)performRequest:(nonnull RestRequest *)restRequest options:(RequestOptions)options callback:(nullable RestCallback *)callback;
- (void)performUploadRequest:(nonnull RestFileUploadRequest *)fileUploadRequest progress:(ProgressBlock)progressBlock callback:(nonnull RestCallback *)callback;
- (void)performUploadRequest:(nonnull RestRequest *)requestObject filePath:(nonnull NSURL *)filePath callback:(nonnull RestCallback *)callback;

@end

NS_ASSUME_NONNULL_END
