
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RestRequest : NSObject

@property (nonnull, nonatomic, copy, readonly) NSString *httpPath;
@property (nonnull, nonatomic, copy, readonly) NSString *httpMethod;
@property (nullable, nonatomic, copy, readonly) id httpBody;
@property (nullable, nonatomic, copy) NSMutableDictionary *additionalHeaders;

+ (nonnull instancetype)requestObjectWithHTTPPath:(nonnull NSString *)path
                                       httpMethod:(nonnull NSString *)method
                                         bodyData:(nullable id)bodyData;
+ (nonnull instancetype)requestObjectWithHTTPPath:(nonnull NSString *)path
                                       httpMethod:(nonnull NSString *)method
                                         bodyData:(nullable id)bodyData
                                additionalHeaders:(nullable NSDictionary *)additionalHeaders;

@end

NS_ASSUME_NONNULL_END
