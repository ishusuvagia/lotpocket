
#import "RestFileUploadRequest.h"
#import "HttpCommon.h"

@interface RestFileUploadRequest ()

@property (nonatomic, copy, readwrite) NSString *httpPath;
@property (nonatomic, copy, readwrite) NSString *httpMethod;
@property (nonatomic, copy, readwrite) id httpBody;
@property (nonatomic, copy, readwrite) NSString *fileKeyName;
@property (nonatomic, copy, readwrite) NSString *fileName;
@property (nonatomic, copy, readwrite) NSString *mimeType;
@property (nonatomic, copy, readwrite) NSDictionary *parameters;

@end

@implementation RestFileUploadRequest

@synthesize httpPath;
@synthesize httpMethod;
@synthesize httpBody;

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (nonnull instancetype)fileUploadRequestWithHttpPath:(NSString *)path
                                          fileKeyName:(NSString *)fileKeyName
                                             fileName:(NSString *)fileName
                                             mimeType:(NSString *)mimeType
                                           parameters:(NSDictionary *)parameters
                                             bodyData:(id)bodyData
{
    RestFileUploadRequest *restFileUploadRequest = [RestFileUploadRequest new];
    restFileUploadRequest.httpPath = path;
    restFileUploadRequest.httpMethod = HTTPRequestMethodPUT;
    restFileUploadRequest.fileKeyName = fileKeyName;
    restFileUploadRequest.fileName = fileName;
    restFileUploadRequest.mimeType = mimeType;
    restFileUploadRequest.parameters = parameters;
    restFileUploadRequest.httpBody = bodyData;
    return restFileUploadRequest;
}

+ (nonnull instancetype)fileUploadRequestWithHttpPath:(NSString *)path
                                          fileKeyName:(NSString *)fileKeyName
                                             fileName:(NSString *)fileName
                                             mimeType:(NSString *)mimeType
                                    additionalHeaders:(NSDictionary *)additionalHeaders
                                           parameters:(NSDictionary *)parameters
                                             bodyData:(id)bodyData
{
    RestFileUploadRequest *restFileUploadRequest = [RestFileUploadRequest fileUploadRequestWithHttpPath:path
                                                                                            fileKeyName:fileKeyName
                                                                                               fileName:fileName
                                                                                               mimeType:mimeType
                                                                                             parameters:parameters
                                                                                               bodyData:bodyData];
    restFileUploadRequest.additionalHeaders = [additionalHeaders mutableCopy];
    return restFileUploadRequest;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {}
    return self;
}

- (void)dealloc
{
    self.fileKeyName = nil;
    self.fileName = nil;
    self.mimeType = nil;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"\nPath=%@\nMethod=%@\nFileKeyName=%@\nFileName=%@\nMimeType=%@\n",
                                      self.httpPath, self.httpMethod, self.fileKeyName, self.fileName, self.mimeType];
}

@end
