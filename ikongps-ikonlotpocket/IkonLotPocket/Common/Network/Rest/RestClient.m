
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import "RestRequestConstructor.h"
#import "Logger.h"
#import "Application.h"
#import "CommonAssert.h"
#import "RestClient.h"

// static NSTimeInterval const DefaultRetryDelay = 1.0;
static NSUInteger const DefaultRequestMaxAttemptsCount = 5;
static NSTimeInterval const DefaultRequestTimeOut = 90.0f;
static NSTimeInterval const DefaultRequestResourceTimeOut = 300.0f;

@interface RestClient ()

@property (nonatomic, readonly) NSURLSessionConfiguration *_sessionConfiguration;

@end

@implementation RestClient

@synthesize _sessionConfiguration = __sessionConfiguration;

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
        _allowInvalidCertificates = !APPLICATION.isProductionMode;
        _requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
        _requestTimeOut = DefaultRequestTimeOut;
        _requestResourceTimeOut = DefaultRequestResourceTimeOut;
        _requestMaxAttemptsCount = DefaultRequestMaxAttemptsCount;
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    }
    return self;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSURLSessionConfiguration *)_sessionConfiguration
{
    if (!__sessionConfiguration) {
        __sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        __sessionConfiguration.requestCachePolicy = self.requestCachePolicy;
        __sessionConfiguration.timeoutIntervalForRequest = self.requestTimeOut;
        __sessionConfiguration.timeoutIntervalForResource = self.requestResourceTimeOut;
    }
    return __sessionConfiguration;
}

- (NSMutableURLRequest *)_urlRequestWithUrl:(NSString *)url httpMethod:(NSString *)method
                          additionalHeaders:(NSDictionary *)additionalHeaders httpBody:(id)bodyData
{
    // Initialize request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    request.HTTPMethod = method;

    // Build the request headers
    NSMutableDictionary *allHeaderFields = [NSMutableDictionary dictionary];
    allHeaderFields[HTTPRequestHeaderNameContentEncoding] = HTTPURLRequestContentEncodingGZIP;
    allHeaderFields[HTTPRequestHeaderNameAccept] = HTTPURLRequestContentTypeJSON;
    allHeaderFields[HTTPRequestHeaderApplicationType] = HTTPURLRequestApplicationTypeREST;
    if (additionalHeaders && additionalHeaders.count > 0) {
        [allHeaderFields addEntriesFromDictionary:additionalHeaders];
    }

    // Http Body
    if ([bodyData isKindOfClass:[NSDictionary class]]) {
        ConsistencyAssert([method isEqualToString:HTTPRequestMethodPOST] || [method isEqualToString:HTTPRequestMethodPUT],
                @"Can't create %@ request with json body.", method);
        allHeaderFields[HTTPRequestHeaderNameContentType] = HTTPURLRequestContentTypeJSON;
        NSError *error = nil;
        request.HTTPBody = [NSJSONSerialization dataWithJSONObject:bodyData options:(NSJSONWritingOptions) 0
                                                             error:&error];
        ConsistencyAssert(error == nil, @"Failed to serialize JSON with error = %@", error);

        TRACE("%@ JSON Request: %@\nHeader Params:\n%@\nBody Params:\n%@", request.HTTPMethod,
                request.URL.absoluteString,
                request.allHTTPHeaderFields, bodyData);
    }

    // Http Headers
    request.allHTTPHeaderFields = allHeaderFields;

    return request;
}

- (RestResponse *)_processResponse:(NSURLResponse *)response responseObject:(id)responseObject error:(NSError *)error
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;

//    fTRACE("Response Status: %ld\nResponse Header:\n%@\nResponse Body:\n%@\nError:\n%@", (long) httpResponse.statusCode, httpResponse.allHeaderFields, responseObject, error.localizedDescription);

    BOOL isSuccess = YES;
    NSInteger httpStatusCode = httpResponse.statusCode;
    NSString *statusMessage = [NSHTTPURLResponse localizedStringForStatusCode:httpStatusCode];
    NSDictionary *allHeaderFields = httpResponse.allHeaderFields;

    if (!responseObject || error) {
        if (error) {
            statusMessage = error.localizedDescription;
        } else {
            statusMessage = @"The content is empty";
            httpStatusCode = HTTPStatusCodeNOCONTENT;
        }
        isSuccess = NO;
        responseObject = @{};
    }
    if (!(httpResponse.statusCode >= HTTPStatusCodeOK && httpStatusCode < HTTPStatusCodeMULTIPLECHOICES)) {
        if (error) {
            statusMessage = error.localizedDescription;
        }
        isSuccess = NO;
    }

    // Construct the response body
    RestResponse *webResponse = [RestResponse reponseObjectWithResult:responseObject
                                                            isSuccess:isSuccess
                                                           statusCode:httpStatusCode
                                                      allHeaderFields:allHeaderFields
                                                        statusMessage:statusMessage];

    return webResponse;
}

- (AFSecurityPolicy *)_securityPolicy
{
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setValidatesDomainName:NO];
    [securityPolicy setAllowInvalidCertificates:self.allowInvalidCertificates];
    return securityPolicy;
}

///--------------------------------------
#pragma mark - Public
- (NSURLSessionDataTask *)extracted:(RestCallback * _Nullable)callback progressBlock:(ProgressBlock _Nullable)progressBlock request:(NSMutableURLRequest *)request sessionManager:(AFHTTPSessionManager *)sessionManager {
    return [sessionManager dataTaskWithRequest:request uploadProgress:nil downloadProgress:^(NSProgress *downloadProgress) {
        // fTRACE("Upload Progress: %.2f", downloadProgress.fractionCompleted);
        if (progressBlock) progressBlock(downloadProgress);
    }                  completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (callback) callback.result([self _processResponse:response responseObject:responseObject error:error]);
    }];
}

///--------------------------------------

- (void)performRequest:(RestRequest *)restRequest downloadProgress:(ProgressBlock)progressBlock callback:(RestCallback *)callback
{
    // Asserts
    ParameterAssert(restRequest, @"RestRequest can not be null");
    ParameterAssert(restRequest.httpPath, @"RestRequest > HTTP Path can not be null");
    ParameterAssert(restRequest.httpMethod, @"RestRequest > HTTP Method can not be null");

    // Session manager
    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:self._sessionConfiguration];
    sessionManager.securityPolicy = self._securityPolicy;

    // Request
    NSMutableURLRequest *request = [self _urlRequestWithUrl:restRequest.httpPath
                                                 httpMethod:restRequest.httpMethod
                                          additionalHeaders:restRequest.additionalHeaders
                                                   httpBody:restRequest.httpBody];

    // Perform request
    [[self extracted:callback progressBlock:progressBlock request:request sessionManager:sessionManager] resume];
}

- (void)performRequest:(RestRequest *)restRequest callback:(RestCallback *)callback
{
    [self performRequest:restRequest downloadProgress:nil callback:callback];
}

- (void)performRequest:(RestRequest *)restRequest options:(RequestOptions)options callback:(RestCallback *)callback
{
    [self performRequest:restRequest callback:callback];
    // TODO: Implement later
}

- (void)performUploadRequest:(RestFileUploadRequest *)fileUploadRequest progress:(ProgressBlock)progressBlock callback:(RestCallback *)callback
{
    ParameterAssert(fileUploadRequest, @"FileUploadRequest can not be null");
    ParameterAssert(fileUploadRequest.httpPath, @"HTTP Path can not be null");
    ParameterAssert(fileUploadRequest.httpMethod, @"HTTP Method can not be null");

    // AFNetworking
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.securityPolicy = self._securityPolicy;

    NSMutableURLRequest *request = [RestRequestConstructor fileUploadUrlRequestwithURL:fileUploadRequest.httpPath
                                                                            httpMethod:fileUploadRequest.httpMethod
                                                                           httpHeaders:fileUploadRequest.additionalHeaders
                                                                           fileKeyName:fileUploadRequest.fileKeyName
                                                                              fileName:fileUploadRequest.fileName
                                                                              mimeType:fileUploadRequest.mimeType
                                                                            parameters:fileUploadRequest.parameters
                                                                              httpBody:fileUploadRequest.httpBody];
    request.timeoutInterval = DefaultRequestResourceTimeOut;
    [[manager uploadTaskWithStreamedRequest:request
                                   progress:^(NSProgress *uploadProgress) {
                                       fTRACE(@"Upload Progress: %.2f", uploadProgress.fractionCompleted);
                                       if (progressBlock) progressBlock(uploadProgress);
                                   } completionHandler:^(NSURLResponse *response, id object, NSError *error) {
                if (callback) callback.result([self _processResponse:response responseObject:object error:error]);
            }] resume];
}

- (void)performUploadRequest:(RestRequest *)requestObject filePath:(NSURL *)filePath callback:(RestCallback *)callback
{
    // Asserts
    ParameterAssert(requestObject, @"RequestObject can not be null");
    ParameterAssert(requestObject.httpPath, @"HTTP Path can not be null");
    ParameterAssert(requestObject.httpMethod, @"HTTP Method can not be null");

    // AFNetworking
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.securityPolicy.allowInvalidCertificates = self.allowInvalidCertificates;

    NSURLRequest *request = [RestRequestConstructor urlRequestWithURL:requestObject.httpPath
                                                           httpMethod:requestObject.httpMethod
                                                          httpHeaders:requestObject.additionalHeaders
                                                             httpBody:requestObject.httpBody];

    [[manager uploadTaskWithRequest:request
                           fromFile:filePath
                           progress:nil
                  completionHandler:^(NSURLResponse *response, id object, NSError *error) {
                      if (callback) callback.result([self _processResponse:response responseObject:object error:error]);
                  }] resume];
}

@end
