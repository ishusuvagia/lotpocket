
#import "RestCallback.h"

@interface RestCallback ()

@property (nonatomic, readwrite) RestResponseBlock result;

@end

@implementation RestCallback

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)callbackWithResult:(RestResponseBlock)result
{
    RestCallback *callback = [RestCallback new];
    callback.result = result;
    return callback;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
    }
    return self;
}

@end
