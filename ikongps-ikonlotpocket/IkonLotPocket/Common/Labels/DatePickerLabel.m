
#import "DatePickerLabel.h"
#import "Utils.h"
#import "AppFont.h"
#import "AppColorScheme.h"

@interface DatePickerLabel ()

@property (nonatomic, strong) UIDatePicker *_datePicker;
@property (nonatomic, strong) UILabel *_toolbarTitleLbl;

@end

@implementation DatePickerLabel

@synthesize inputView = _inputView;
@synthesize inputAccessoryView = _inputAccessoryView;

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.isActionOverridenByContainer) {
        [super touchesEnded:touches withEvent:event];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardWillShowNotification object:nil];
        [super touchesEnded:touches withEvent:event];
        [self becomeFirstResponder];
    }
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (UIToolbar *)_createToolbarWithText:(NSString *)labelText
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, 50.0f)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    // Close Button
    UIBarButtonItem *closeBarButton = [[UIBarButtonItem alloc] initWithTitle:LOCALIZED(@"btn_close")
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(_respondToClose:)];
    // Done Button
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithTitle:LOCALIZED(@"btn_done")
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(_respondToDone:)];

    __toolbarTitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 230.f, 21.0f)];
    self._toolbarTitleLbl.text = labelText;
    self._toolbarTitleLbl.font = [AppFont textFieldFont];
    self._toolbarTitleLbl.textColor = [AppColorScheme white];
    self._toolbarTitleLbl.textAlignment = NSTextAlignmentCenter;
    self._toolbarTitleLbl.userInteractionEnabled = NO;
    UIBarButtonItem *labelBtn = [[UIBarButtonItem alloc] initWithCustomView:self._toolbarTitleLbl];

    // Add items to toolbar
    toolbar.items = @[closeBarButton, space, labelBtn, space, doneBarButton];
    toolbar.barStyle = UIBarStyleBlack;
    [toolbar sizeToFit];

    return toolbar;
}

- (void)_respondToClose:(id)sender
{
    [self endEditing:YES];
}

- (void)_respondToDone:(id)sender
{
    NSString *label = [self _labelStringForDatePicker];
    self._toolbarTitleLbl.text = [NSString stringWithFormat:@"%@ %@", self.inputAccessoryPrefix ?: kEmptyString, label];
    if (self.datePickerMode == UIDatePickerModeDate) {
        if ([self.protocol respondsToSelector:@selector(updatewithNewDate:ofLabel:)]) {
            [self.protocol updatewithNewDate:self._datePicker.date ofLabel:self];
        }
    }
    if (self.datePickerMode == UIDatePickerModeDateAndTime) {
        if ([self.protocol respondsToSelector:@selector(updatewithNewDate:ofLabel:)]) {
            [self.protocol updatewithNewDate:self._datePicker.date ofLabel:self];
        }
    } else if (self.datePickerMode == UIDatePickerModeCountDownTimer) {
        if ([self.protocol respondsToSelector:@selector(updateWithNewCountDownDuration:ofLabel:)]) {
            [self.protocol updateWithNewCountDownDuration:self._datePicker.countDownDuration ofLabel:self];
        }
    }

    [self _respondToClose:sender];
}

- (void)_updateToolbarTitle:(id)sender
{
    NSString *label = [self _labelStringForDatePicker];
    self._toolbarTitleLbl.text = [NSString stringWithFormat:@"%@ %@", self.inputAccessoryPrefix ?: kEmptyString, label];
}

- (NSString *)_labelStringForDatePicker
{
    NSString *label = kEmptyString;
    
    if (self.datePickerMode == UIDatePickerModeTime) {
        label = [Utils convertNSDateToString:self._datePicker.date withFormat:kFormatUSDateTimeAMPM];
    } else if (self.datePickerMode == UIDatePickerModeDate) {
        label = [Utils convertNSDateToString:self._datePicker.date withFormat:kFormatDateUS];
    } else if (self.datePickerMode == UIDatePickerModeDateAndTime) {
        label = [Utils convertNSDateToString:self._datePicker.date withFormat:kFormatUSDateTimeAMPM];
    } else {
        NSInteger hours = lround(floor(self._datePicker.countDownDuration / 3600.0f));
        NSInteger minutes = lround(floor((self._datePicker.countDownDuration - hours * 3600.0f) / 60));
        NSString *hoursLbl = hours == 1 ? LOCALIZED(@"hour") : LOCALIZED(@"hours");
        label = [NSString stringWithFormat:@"%ld %@ %ld %@", (long)hours, hoursLbl, (long)minutes, LOCALIZED(@"min")];
        self._toolbarTitleLbl.text = [NSString stringWithFormat:@"%@ %@", self.inputAccessoryPrefix ?: kEmptyString, label];
    }
    
    return label;
}

- (void)_setDatePickerValue
{
    if (self.datePickerMode == UIDatePickerModeTime) {
        self._datePicker.date = ![Utils stringIsNullOrEmpty:self.text] ? [Utils convertStringToNSDate:self.text withFormat:kFormatUSDateTimeAMPM] : [NSDate date];
    } else if (self.datePickerMode == UIDatePickerModeDate) {
        self._datePicker.date = ![Utils stringIsNullOrEmpty:self.text] ? [Utils convertStringToNSDate:self.text withFormat:kFormatDateUS] : [NSDate date];
    } else if (self.datePickerMode == UIDatePickerModeDateAndTime) {
        self._datePicker.date = ![Utils stringIsNullOrEmpty:self.text] ? [Utils convertStringToNSDate:self.text withFormat:kFormatUSDateTimeAMPM] : [NSDate date];
    } else {
        NSArray *stringArray = [self.text componentsSeparatedByString: @" "];
        if (stringArray.count > 0) {
            CGFloat countDownDuration = lround([stringArray[0] floatValue] * 3600.0f);
            self._datePicker.countDownDuration = countDownDuration;
            dispatch_async(dispatch_get_main_queue(), ^{
                // Account for bug in iOS 7 and 8 when a UIDatePicker is used in countdown mode
                // http://stackoverflow.com/a/20204225/89342
                //
                // This does not fix the bug when the datepicker is shown, and a rotation occurs with the
                // date picker still open.
                self._datePicker.countDownDuration = countDownDuration;
            });
            
        }
    }
}

///--------------------------------------
#pragma mark - Setters
///--------------------------------------

- (UIView *)inputView
{
    if (!_inputView) {
        __datePicker = [UIDatePicker new];
        self._datePicker.datePickerMode = self.datePickerMode;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        [self._datePicker setTimeZone:[calendar timeZone]];
        [self._datePicker setLocale:[NSLocale currentLocale]];
        [self._datePicker setCalendar:calendar];
        [self._datePicker addTarget:self action:@selector(_updateToolbarTitle:) forControlEvents:UIControlEventValueChanged];
        [self _setDatePickerValue];
        _inputView = self._datePicker;
    }
    return _inputView;
}

- (UIView *)inputAccessoryView
{
    if (!_inputAccessoryView) {
        NSString *label = [self _labelStringForDatePicker];
        UIToolbar *toolbar = [self _createToolbarWithText:[NSString stringWithFormat:@"%@ %@", self.inputAccessoryPrefix ?: kEmptyString, label]];
        _inputAccessoryView = toolbar;
    }
    return _inputAccessoryView;
}

@end
