
#import <QuartzCore/QuartzCore.h>
#import "RoundLabel.h"
#import "AppColorScheme.h"

static CGFloat const kLabelTextLeftRightMargin = 15.0f;
static CGFloat const kLabelTopBottomMargin = 5.0f;
static CGFloat const kLabelCornerRadius = 10.0f;

@implementation RoundLabel

@synthesize backgroundColor = _backgroundColor;
@synthesize pinPoint = _pinPoint;
@synthesize pinPosition = _pinPosition;

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (id)init
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.textColor = [AppColorScheme white];
        self.textAlignment = NSTextAlignmentCenter;
        self.layer.cornerRadius = kLabelCornerRadius;
        self.layer.masksToBounds = YES;
        self.layer.shouldRasterize = YES;
        super.backgroundColor = [AppColorScheme blue];
        self.pinPosition = RoundLabelPinAtTopLeft;
    }
    return self;
}

- (id)initWithFont:(UIFont *)font
{
    self = [self init];
    if (self) {
        self.font = font;
    }
    return self;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    _backgroundColor = backgroundColor;
    super.backgroundColor = backgroundColor;
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self sizeToFit];
    CGRect frame = self.frame;

    frame.size.width += kLabelTextLeftRightMargin;
    frame.size.height += kLabelTopBottomMargin;
    frame.origin = self.pinPoint;

    switch (self.pinPosition) {
        case RoundLabelPinAtBottomLeft:frame.origin.y -= frame.size.height;
            break;
        case RoundLabelPinAtBottomRight:frame.origin.y -= frame.size.height;
        case RoundLabelPinAtTopRight:frame.origin.x = self.superview.frame.size.width - frame.size.width;
            break;
        default:break;
    }

    self.frame = frame;
}

@end
