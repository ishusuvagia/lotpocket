
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, TextVerticalAlignment) { TextVerticalAlignmentTop, TextVerticalAlignmentMiddle, TextVerticalAlignmentBottom };

@interface UILabel (VerticalAlignment)

- (TextVerticalAlignment)textVerticalAlignment;
- (void)setTextVerticalAlignment:(TextVerticalAlignment)textVerticalAlignment;

@end

NS_ASSUME_NONNULL_END
