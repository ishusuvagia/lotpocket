
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, RoundLabelPinPosition) {
    RoundLabelPinAtTopLeft,
    RoundLabelPinAtTopRight,
    RoundLabelPinAtBottomLeft,
    RoundLabelPinAtBottomRight
};

@interface RoundLabel : UILabel

- (nonnull id)init;
- (id)initWithFont:(nullable UIFont *)font;

@property (nonnull, nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, assign) CGPoint pinPoint;
@property (nonatomic, assign) RoundLabelPinPosition pinPosition;

@end

NS_ASSUME_NONNULL_END
