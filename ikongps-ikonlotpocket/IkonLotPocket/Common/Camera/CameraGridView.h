
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CameraGridView : UIView

@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) NSUInteger numberOfColumns;
@property (nonatomic, assign) NSUInteger numberOfRows;

@end

NS_ASSUME_NONNULL_END
