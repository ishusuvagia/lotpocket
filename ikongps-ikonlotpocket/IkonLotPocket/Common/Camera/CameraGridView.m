
#import "CameraGridView.h"

@implementation CameraGridView

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self _setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self _setup];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, self.lineWidth);
    CGContextSetStrokeColorWithColor(context, [UIColor lightGrayColor].CGColor);

    ///--------------------------------------
    /// Drawing column lines
    ///--------------------------------------

    // calculate column width
    CGFloat columnWidth = (CGFloat) (self.frame.size.width / (self.numberOfColumns + 1.0));

    for (NSUInteger i = 1; i <= self.numberOfColumns; i++) {
        CGPoint startPoint;
        CGPoint endPoint;

        startPoint.x = columnWidth * i;
        startPoint.y = 0.0f;

        endPoint.x = startPoint.x;
        endPoint.y = self.frame.size.height;

        CGContextMoveToPoint(context, startPoint.x, startPoint.y);
        CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
        CGContextStrokePath(context);
    }

    ///--------------------------------------
    /// Drawing row lines
    ///--------------------------------------

    // calculate row height
    CGFloat rowHeight = (CGFloat) (self.frame.size.height / (self.numberOfRows + 1.0));

    for (NSUInteger j = 1; j <= self.numberOfRows; j++) {
        CGPoint startPoint;
        CGPoint endPoint;

        startPoint.x = 0.0f;
        startPoint.y = rowHeight * j;

        endPoint.x = self.frame.size.width;
        endPoint.y = startPoint.y;

        CGContextMoveToPoint(context, startPoint.x, startPoint.y);
        CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
        CGContextStrokePath(context);
    }
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_setup
{
    self.backgroundColor = [UIColor clearColor];
    self.lineWidth = 1.0;
}

@end
