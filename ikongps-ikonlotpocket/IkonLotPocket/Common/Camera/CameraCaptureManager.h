
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CameraCaptureManagerDelegate <NSObject>

- (void)captureImageDidStart;
- (void)captureImageDidFinish:(nullable UIImage *)image withMetadata:(nullable NSDictionary *)metadata;
- (void)captureImageFailedWithError:(nullable NSError *)error;
- (void)someOtherError:(nullable NSError *)error;
- (void)acquiringDeviceLockFailedWithError:(nullable NSError *)error;
- (void)captureSessionDidStartRunning;

@end

@interface CameraCaptureManager : NSObject

@property (nullable, nonatomic, weak) id <CameraCaptureManagerDelegate> delegate;
@property (nullable, nonatomic, readonly) AVCaptureSession *captureSession;
@property (nullable, nonatomic, readonly) AVCaptureDeviceInput *videoInput;
@property (nonatomic, assign) AVCaptureFlashMode flashMode;
@property (nonatomic, assign) AVCaptureTorchMode torchMode;
@property (nonatomic, assign) AVCaptureFocusMode focusMode;
@property (nonatomic, assign) AVCaptureExposureMode exposureMode;
@property (nonatomic, assign) AVCaptureWhiteBalanceMode whiteBalanceMode;
@property (nonatomic, assign, readonly) NSUInteger cameraCount;
@property (nonatomic, assign) BOOL allowToSaveToPhotoAlbum;

- (void)setCameraMaxScale:(CGFloat)maxScale;
- (CGFloat)cameraMaxScale;
- (BOOL)cameraToggle;
- (BOOL)hasMultipleCameras;
- (BOOL)hasFlash;
- (BOOL)hasTorch;
- (BOOL)hasFocus;
- (BOOL)hasExposure;
- (BOOL)hasWhiteBalance;
- (BOOL)setupSessionWithPreset:(nullable NSString *)sessionPreset error:(NSError **)error;
- (void)startRunning;
- (void)stopRunning;
- (void)captureImageForDeviceOrientation:(UIDeviceOrientation)deviceOrientation;
- (void)focusAtPoint:(CGPoint)point;
- (void)exposureAtPoint:(CGPoint)point;
- (CGPoint)convertToPointOfInterestFrom:(CGRect)frame coordinates:(CGPoint)viewCoordinates layer:(nullable AVCaptureVideoPreviewLayer *)layer;

@end

NS_ASSUME_NONNULL_END
