
#import "CameraView.h"
#import "Logger.h"
#import "CustomActivityButton.h"

#define previewFrame \
    (CGRect) { 0, 65, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 138 }
#define MAX_PINCH_SCALE_NUM 3.0f
#define MIN_PINCH_SCALE_NUM 1.0f

@implementation CameraView
{
    CGFloat preScaleNum;
    CGFloat scaleNum;
}

@synthesize tintColor = _tintColor;
@synthesize selectedTintColor = _selectedTintColor;

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (instancetype)initWithFrame:(CGRect)frame
{
    return [[self alloc] initWithFrame:frame captureSession:nil];
}

+ (CameraView *)initWithCaptureSession:(AVCaptureSession *)captureSession
{
    return [[self alloc] initWithFrame:[[UIScreen mainScreen] bounds] captureSession:captureSession];
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithFrame:(CGRect)frame captureSession:(AVCaptureSession *)captureSession
{
    if ((self = [super initWithFrame:frame])) {
        [self setBackgroundColor:[UIColor blackColor]];
        _previewLayer = [AVCaptureVideoPreviewLayer new];
        if (captureSession) {
            [self.previewLayer setSession:captureSession];
            [_previewLayer setFrame:previewFrame];
        } else
            [_previewLayer setFrame:self.bounds];

        if ([_previewLayer respondsToSelector:@selector(connection)]) {
            if ([_previewLayer.connection isVideoOrientationSupported])
                [_previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
        }

        [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];

        [self.layer addSublayer:_previewLayer];

        self.tintColor = [UIColor whiteColor];
        self.selectedTintColor = [UIColor redColor];
    }

    return self;
}

- (void)dealloc
{
    // TRACE_HERE;
    self.delegate = nil;
    self.focusBox = nil;
    self.exposeBox = nil;
    self.photoLibraryButton = nil;
    self.triggerButton = nil;
    self.gridButton = nil;
    self.cameraButton = nil;
    self.flashButton = nil;
    _previewLayer = nil;
    _singleTap = nil;
    _doubleTap = nil;
    _panGestureRecognizer = nil;
    _pinch = nil;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_createGesture
{
    // Single tap is designated to focus
    _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_tapToFocus:)];
    [_singleTap setDelaysTouchesEnded:NO];
    [_singleTap setNumberOfTapsRequired:1];
    [_singleTap setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:_singleTap];

    // Double tap is designated to expose
    _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_tapToExpose:)];
    [_doubleTap setDelaysTouchesEnded:NO];
    [_doubleTap setNumberOfTapsRequired:2];
    [_doubleTap setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:_doubleTap];
    [_singleTap requireGestureRecognizerToFail:_doubleTap];

    // Pan gesture
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(_handlePanGestureRecognizer:)];
    [_panGestureRecognizer setDelaysTouchesEnded:NO];
    [_panGestureRecognizer setMinimumNumberOfTouches:1];
    [_panGestureRecognizer setMaximumNumberOfTouches:1];
    [_panGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:_panGestureRecognizer];

    // Pinch is designated to zoom
    _pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(_handlePinch:)];
    [_pinch setDelaysTouchesEnded:NO];
    [self addGestureRecognizer:_pinch];
}

- (void)_tapToFocus:(UIGestureRecognizer *)recognizer
{
    TRACE_HERE;

    CGPoint tempPoint = (CGPoint) [recognizer locationInView:self];
    if ([self.delegate respondsToSelector:@selector(cameraView:focusAtPoint:)] && CGRectContainsPoint(self.previewLayer.frame, tempPoint)) {
        [self.delegate cameraView:self focusAtPoint:(CGPoint) {tempPoint.x, tempPoint.y - CGRectGetMinY(self.previewLayer.frame)}];
        [self drawFocusBoxAtPointOfInterest:tempPoint andRemove:YES];
    }
}

- (void)_tapToExpose:(UIGestureRecognizer *)recognizer
{
    TRACE_HERE;

    CGPoint tempPoint = (CGPoint) [recognizer locationInView:self];
    if ([self.delegate respondsToSelector:@selector(cameraView:exposeAtPoint:)] && CGRectContainsPoint(self.previewLayer.frame, tempPoint)) {
        [self.delegate cameraView:self exposeAtPoint:(CGPoint) {tempPoint.x, tempPoint.y - CGRectGetMinY(self.previewLayer.frame)}];
        [self drawExposeBoxAtPointOfInterest:tempPoint andRemove:YES];
    }
}

- (void)_handlePanGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer
{
    TRACE_HERE;

    BOOL hasFocus = YES;
    if ([self.delegate respondsToSelector:@selector(cameraViewHasFocus)])
        hasFocus = [self.delegate cameraViewHasFocus];

    if (!hasFocus)
        return;

    UIGestureRecognizerState state = panGestureRecognizer.state;
    CGPoint touchPoint = [panGestureRecognizer locationInView:self];
    [self draw:self.focusBox
            atPointOfInterest:(CGPoint) {touchPoint.x, touchPoint.y - CGRectGetMinY(self.previewLayer.frame)}
            andRemove:YES];

    switch (state) {
        case UIGestureRecognizerStateBegan:

            break;
        case UIGestureRecognizerStateChanged: {
            break;
        }
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateEnded: {
            [self _tapToFocus:panGestureRecognizer];
            break;
        }
        default:break;
    }
}

- (void)_handlePinch:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    TRACE_HERE;

    BOOL allTouchesAreOnThePreviewLayer = YES;
    NSUInteger numTouches = [pinchGestureRecognizer numberOfTouches], i;
    for (i = 0; i < numTouches; ++i) {
        CGPoint location = [pinchGestureRecognizer locationOfTouch:i inView:self];
        CGPoint convertedLocation = [_previewLayer convertPoint:location fromLayer:self.previewLayer.superlayer];
        if (![self.previewLayer containsPoint:convertedLocation]) {
            allTouchesAreOnThePreviewLayer = NO;
            break;
        }
    }

    if (allTouchesAreOnThePreviewLayer) {
        scaleNum = preScaleNum * pinchGestureRecognizer.scale;

        if (scaleNum < MIN_PINCH_SCALE_NUM)
            scaleNum = MIN_PINCH_SCALE_NUM;
        else if (scaleNum > MAX_PINCH_SCALE_NUM)
            scaleNum = MAX_PINCH_SCALE_NUM;

        if ([self.delegate respondsToSelector:@selector(cameraCaptureScale:)])
            [self.delegate cameraCaptureScale:scaleNum];

        [self _doPinch];
    }

    if ([pinchGestureRecognizer state] == UIGestureRecognizerStateEnded ||
            [pinchGestureRecognizer state] == UIGestureRecognizerStateCancelled ||
            [pinchGestureRecognizer state] == UIGestureRecognizerStateFailed) {
        preScaleNum = scaleNum;
    }
}

- (void)_doPinch
{
    if ([self.delegate respondsToSelector:@selector(cameraMaxScale)]) {
        CGFloat maxScale = [self.delegate cameraMaxScale];
        if (scaleNum > maxScale)
            scaleNum = maxScale;

        [CATransaction begin];
        [CATransaction setAnimationDuration:.025];
        [self.previewLayer setAffineTransform:CGAffineTransformMakeScale(scaleNum, scaleNum)];
        [CATransaction commit];
    }
}

- (void)_libraryAction:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(openLibrary)])
        [self.delegate openLibrary];
}

- (void)_triggerAction:(UIButton *)button
{
    if ([button isKindOfClass:[CustomActivityButton class]]) {
        [(CustomActivityButton *) button showActivity];
    } else {
        button.enabled = NO;
    }

    if ([self.delegate respondsToSelector:@selector(cameraViewStartRecording)])
        [self.delegate cameraViewStartRecording];
}

- (void)_close
{
    if ([self.delegate respondsToSelector:@selector(closeCamera)])
        [self.delegate closeCamera];
}

- (void)_changeCamera:(UIButton *)button
{
    [button setSelected:!button.isSelected];
    if (button.isSelected && self.flashButton.isSelected)
        [self _flashTriggerAction:self.flashButton];
    [self.flashButton setEnabled:!button.isSelected];
    if ([self.delegate respondsToSelector:@selector(switchCamera)]) {
        [self.delegate switchCamera];
    }
}

- (void)_flashTriggerAction:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(triggerFlashForMode:)]) {
        if (button.tag == AVCaptureFlashModeOff) {
            button.tag = AVCaptureFlashModeOn;
            [button setImage:[UIImage imageNamed:@"img-flashbtn-on"] forState:UIControlStateNormal];
            [self.delegate triggerFlashForMode:AVCaptureFlashModeOn];
        } else {
            button.tag = AVCaptureFlashModeOff;
            [button setImage:[UIImage imageNamed:@"img-flashbtn-off"] forState:UIControlStateNormal];
            [self.delegate triggerFlashForMode:AVCaptureFlashModeOff];
        }
    }
}

- (void)_addGridToCameraAction:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(cameraView:showGridView:)]) {
        button.selected = !button.selected;
        [self.delegate cameraView:self showGridView:button.selected];
    }
}

///--------------------------------------
#pragma mark - Getter & Setter
///--------------------------------------

- (CALayer *)focusBox
{
    if (!_focusBox) {
        _focusBox = [CALayer new];
        [_focusBox setCornerRadius:45.0f];
        [_focusBox setBounds:CGRectMake(0.0f, 0.0f, 90, 90)];
        [_focusBox setBorderWidth:5.f];
        [_focusBox setBorderColor:[UIColor whiteColor].CGColor];
        [_focusBox setOpacity:0];
    }

    return _focusBox;
}

- (CALayer *)exposeBox
{
    if (!_exposeBox) {
        _exposeBox = [CALayer new];
        [_exposeBox setCornerRadius:55.0f];
        [_exposeBox setBounds:CGRectMake(0.0f, 0.0f, 110, 110)];
        [_exposeBox setBorderWidth:5.f];
        [_exposeBox setBorderColor:[self.selectedTintColor CGColor]];
        [_exposeBox setOpacity:0];
    }

    return _exposeBox;
}

- (void)setTriggerButton:(UIButton *)triggerButton
{
    _triggerButton = triggerButton;
    [_triggerButton addTarget:self action:@selector(_triggerAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setFlashButton:(UIButton *)flashButton
{
    _flashButton = flashButton;
    [_flashButton addTarget:self action:@selector(_flashTriggerAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setGridButton:(UIButton *)gridButton
{
    _gridButton = gridButton;
    [_gridButton addTarget:self action:@selector(_addGridToCameraAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setPhotoLibraryButton:(UIButton *)photoLibraryButton
{
    _photoLibraryButton = photoLibraryButton;
    [_photoLibraryButton addTarget:self action:@selector(_libraryAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (UIButton *)cameraButton
{
    if (!_cameraButton) {
        _cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cameraButton setImage:[UIImage imageNamed:@"img-swapcamerabtn"] forState:UIControlStateNormal];
        [_cameraButton setBackgroundColor:[UIColor clearColor]];
        [_cameraButton setFrame:(CGRect) {CGRectGetWidth(self.bounds) - 50, CGRectGetHeight(self.bounds) - 50, 40, 40}];
        [_cameraButton addTarget:self action:@selector(_changeCamera:) forControlEvents:UIControlEventTouchUpInside];
    }

    return _cameraButton;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)defaultInterface
{
    [self addSubview:self.flashButton];
    [self addSubview:self.gridButton];
    [self addSubview:self.cameraButton];

    [self _createGesture];
}

- (void)draw:(nullable CALayer *)layer atPointOfInterest:(CGPoint)point andRemove:(BOOL)remove
{
    if (remove)
        [layer removeAllAnimations];

    if (![layer animationForKey:@"transform.scale"] && ![layer animationForKey:@"opacity"]) {
        [CATransaction begin];
        [CATransaction setValue:(id) kCFBooleanTrue forKey:kCATransactionDisableActions];
        [layer setPosition:point];
        [CATransaction commit];

        CABasicAnimation *scale = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [scale setFromValue:@(1)];
        [scale setToValue:@(0.7)];
        [scale setDuration:0.8];
        [scale setRemovedOnCompletion:YES];

        CABasicAnimation *opacity = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [opacity setFromValue:@(1)];
        [opacity setToValue:@(0)];
        [opacity setDuration:0.8];
        [opacity setRemovedOnCompletion:YES];

        [layer addAnimation:scale forKey:@"transform.scale"];
        [layer addAnimation:opacity forKey:@"opacity"];
    }
}

- (void)drawFocusBoxAtPointOfInterest:(CGPoint)point andRemove:(BOOL)remove
{
    [self draw:self.focusBox atPointOfInterest:point andRemove:remove];
}

- (void)drawExposeBoxAtPointOfInterest:(CGPoint)point andRemove:(BOOL)remove
{
    [self draw:self.exposeBox atPointOfInterest:point andRemove:remove];
}

- (void)pinchCameraViewWithScaleNum:(CGFloat)scale
{
    scaleNum = scale;
    if (scaleNum < MIN_PINCH_SCALE_NUM)
        scaleNum = MIN_PINCH_SCALE_NUM;
    else if (scaleNum > MAX_PINCH_SCALE_NUM)
        scaleNum = MAX_PINCH_SCALE_NUM;

    [self _doPinch];
    preScaleNum = scale;
}

- (void)enableActionButtons
{
    if ([self.triggerButton isKindOfClass:[CustomActivityButton class]]) {
        [(CustomActivityButton *) self.triggerButton hideActivity];
    } else {
        self.triggerButton.enabled = YES;
    }
}

@end
