
#import "CameraCaptureManager.h"
#import <PhotosUI/PhotosUI.h>

@interface CameraCaptureManager (AVCaptureFileOutputRecordingDelegate) <AVCaptureFileOutputRecordingDelegate>
@end

@interface CameraCaptureManager ()
{
    AVCaptureStillImageOutput *_stillImageOutput;
    CGFloat _maxScale;
}

@end

@implementation CameraCaptureManager

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (AVCaptureConnection *)connectionWithMediaType:(NSString *)mediaType fromConnections:(NSArray *)connections
{
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([port.mediaType isEqual:mediaType]) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) {
            break;
        }
    }

    return videoConnection;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)init
{
    if ((self = [super init])) {
        _maxScale = 1.0;

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(_captureSessionDidStartRunning:)
                                                     name:AVCaptureSessionDidStartRunningNotification
                                                   object:_captureSession];
    }

    return self;
}

- (void)dealloc
{
    // TRACE_HERE;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_captureSession stopRunning];
    _captureSession = nil;
    _videoInput = nil;
    _stillImageOutput = nil;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_captureSessionDidStartRunning:(NSNotification *)notification
{
    if ([self.delegate respondsToSelector:@selector(captureSessionDidStartRunning)])
        [self.delegate captureSessionDidStartRunning];
}

- (AVCaptureDevice *)_cameraWithPosition:(AVCaptureDevicePosition)position
{
    __block AVCaptureDevice *deviceBlock = nil;

    [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]
            enumerateObjectsUsingBlock:^(AVCaptureDevice *device, NSUInteger idx, BOOL *stop) {
                if ([device position] == position) {
                    deviceBlock = device;
                    *stop = YES;
                }
            }];

    return deviceBlock;
}

- (AVCaptureDevice *)_frontCamera
{
    return [self _cameraWithPosition:AVCaptureDevicePositionFront];
}

- (AVCaptureDevice *)_backCamera
{
    return [self _cameraWithPosition:AVCaptureDevicePositionBack];
}

///--------------------------------------
#pragma mark - Getter & Setter
///--------------------------------------

- (AVCaptureFlashMode)flashMode
{
    return _videoInput.device.flashMode;
}

- (void)setFlashMode:(AVCaptureFlashMode)flashMode
{
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isFlashModeSupported:flashMode] && device.flashMode != flashMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.flashMode = flashMode;
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)])
                [self.delegate acquiringDeviceLockFailedWithError:error];
        }
    }
}

- (AVCaptureTorchMode)torchMode
{
    return self.videoInput.device.torchMode;
}

- (void)setTorchMode:(AVCaptureTorchMode)torchMode
{
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isTorchModeSupported:torchMode] && device.torchMode != torchMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.torchMode = torchMode;
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)])
                [self.delegate acquiringDeviceLockFailedWithError:error];
        }
    }
}

- (AVCaptureFocusMode)focusMode
{
    return self.videoInput.device.focusMode;
}

- (void)setFocusMode:(AVCaptureFocusMode)focusMode
{
    AVCaptureDevice *device = self.videoInput.device;
    if ([device isFocusModeSupported:focusMode] && device.focusMode != focusMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.focusMode = focusMode;
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)])
                [self.delegate acquiringDeviceLockFailedWithError:error];
        }
    }
}

- (AVCaptureExposureMode)exposureMode
{
    return self.videoInput.device.exposureMode;
}

- (void)setExposureMode:(AVCaptureExposureMode)exposureMode
{
    if (exposureMode == AVCaptureExposureModeAutoExpose)
        exposureMode = AVCaptureExposureModeContinuousAutoExposure;

    AVCaptureDevice *device = self.videoInput.device;
    if ([device isExposureModeSupported:exposureMode] && device.exposureMode != exposureMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.exposureMode = exposureMode;
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)])
                [self.delegate acquiringDeviceLockFailedWithError:error];
        }
    }
}

- (AVCaptureWhiteBalanceMode)whiteBalanceMode
{
    return self.videoInput.device.whiteBalanceMode;
}

- (void)setWhiteBalanceMode:(AVCaptureWhiteBalanceMode)whiteBalanceMode
{
    if (whiteBalanceMode == AVCaptureWhiteBalanceModeAutoWhiteBalance)
        whiteBalanceMode = AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;

    AVCaptureDevice *device = self.videoInput.device;
    if ([device isWhiteBalanceModeSupported:whiteBalanceMode] && device.whiteBalanceMode != whiteBalanceMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.whiteBalanceMode = whiteBalanceMode;
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)])
                [self.delegate acquiringDeviceLockFailedWithError:error];
        }
    }
}

- (NSUInteger)cameraCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)setCameraMaxScale:(CGFloat)maxScale
{
    _maxScale = maxScale;
}

- (CGFloat)cameraMaxScale
{
    AVCaptureConnection *videoConnection =
            [CameraCaptureManager connectionWithMediaType:AVMediaTypeVideo
                                          fromConnections:_stillImageOutput.connections];
    return videoConnection.videoMaxScaleAndCropFactor;
}

- (BOOL)cameraToggle
{
    BOOL success = NO;

    if ([self hasMultipleCameras]) {
        NSError *error;
        AVCaptureDeviceInput *newVideoInput;
        AVCaptureDevicePosition position = self.videoInput.device.position;

        if (position == AVCaptureDevicePositionBack)
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self _frontCamera] error:&error];
        else if (position == AVCaptureDevicePositionFront)
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self _backCamera] error:&error];
        else
            goto bail;

        if (newVideoInput != nil) {
            [self.captureSession beginConfiguration];
            [self.captureSession removeInput:_videoInput];

            if ([self.captureSession canAddInput:newVideoInput]) {
                [self.captureSession addInput:newVideoInput];
                _videoInput = newVideoInput;
            } else
                [self.captureSession addInput:_videoInput];

            [self.captureSession commitConfiguration];

            success = YES;
        } else if (error) {
            if ([self.delegate respondsToSelector:@selector(someOtherError:)])
                [self.delegate someOtherError:error];
        }
    }

    bail:
    return success;
}

- (BOOL)hasMultipleCameras
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count] > 1 ? YES : NO;
}

- (BOOL)hasFlash
{
    return self.videoInput.device.hasFlash;
}

- (BOOL)hasTorch
{
    return self.videoInput.device.hasTorch;
}

- (BOOL)hasFocus
{
    AVCaptureDevice *device = self.videoInput.device;

    return [device isFocusModeSupported:AVCaptureFocusModeLocked] || [device isFocusModeSupported:AVCaptureFocusModeAutoFocus] ||
            [device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus];
}

- (BOOL)hasExposure
{
    AVCaptureDevice *device = self.videoInput.device;

    return [device isExposureModeSupported:AVCaptureExposureModeLocked] ||
            [device isExposureModeSupported:AVCaptureExposureModeAutoExpose] ||
            [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure];
}

- (BOOL)hasWhiteBalance
{
    AVCaptureDevice *device = self.videoInput.device;

    return [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeLocked] ||
            [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance];
}

- (BOOL)setupSessionWithPreset:(NSString *)sessionPreset error:(NSError **)error
{
    _videoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self _backCamera] error:error];

    _stillImageOutput = [AVCaptureStillImageOutput new];
    [_stillImageOutput setOutputSettings:@{AVVideoCodecKey: AVVideoCodecJPEG}];

    _captureSession = [AVCaptureSession new];
    if ([self.captureSession canAddInput:_videoInput])
        [self.captureSession addInput:_videoInput];

    if ([self.captureSession canAddOutput:_stillImageOutput])
        [self.captureSession addOutput:_stillImageOutput];

    [self.captureSession setSessionPreset:sessionPreset];

    [self setFlashMode:AVCaptureFlashModeOff];

    return YES;
}

- (void)startRunning
{
    [self.captureSession startRunning];
}

- (void)stopRunning
{
    [self.captureSession stopRunning];
}

- (void)captureImageForDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
    if ([self.delegate respondsToSelector:@selector(captureImageDidStart)]) {
        [self.delegate captureImageDidStart];
    }

    AVCaptureConnection *videoConnection =
            [CameraCaptureManager connectionWithMediaType:AVMediaTypeVideo
                                          fromConnections:_stillImageOutput.connections];

    if (!videoConnection) {
        NSError *error = [NSError errorWithDomain:@"CameraCaptureManager"
                                             code:-1
                                         userInfo:@{
                                                 NSLocalizedFailureReasonErrorKey: @"cameraimage.noconnection"
                                         }];
        if ([self.delegate respondsToSelector:@selector(captureImageFailedWithError:)]) {
            [self.delegate captureImageFailedWithError:error];
        }
        return;
    }

    if ([videoConnection isVideoOrientationSupported]) {
        switch (deviceOrientation) {
            case UIDeviceOrientationPortraitUpsideDown:[videoConnection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
                break;
            case UIDeviceOrientationLandscapeLeft:[videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
                break;
            case UIDeviceOrientationLandscapeRight:[videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                break;
            default:[videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                break;
        }
    }

    [videoConnection setVideoScaleAndCropFactor:_maxScale];

    __weak AVCaptureSession *captureSessionBlock = _captureSession;
    __weak id <CameraCaptureManagerDelegate> delegateBlock = self.delegate;

    [_stillImageOutput
            captureStillImageAsynchronouslyFromConnection:videoConnection
                                        completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {

                                            [captureSessionBlock stopRunning];

                                            if (imageDataSampleBuffer != NULL) {
                                                NSData *imageData =
                                                        [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                                                UIImage *image = [[UIImage alloc] initWithData:imageData];

                                                CFDictionaryRef metadata =
                                                        CMCopyDictionaryOfAttachments(NULL, imageDataSampleBuffer,
                                                                kCMAttachmentMode_ShouldPropagate);
                                                NSDictionary *meta =
                                                        [[NSDictionary alloc] initWithDictionary:(__bridge NSDictionary *) (metadata)];
                                                CFRelease(metadata);

                                                // Notify the delegate that capturing event is finished
                                                if ([delegateBlock respondsToSelector:@selector(captureImageDidFinish:withMetadata:)])
                                                    [delegateBlock captureImageDidFinish:image withMetadata:meta];

                                                // If the user allows every captured to be saved into the Photo Album
                                                // save the image to the Photo album
                                                if (self.allowToSaveToPhotoAlbum) {
                                                    [[PHPhotoLibrary sharedPhotoLibrary]
                                                            performChanges:^{
                                                                [PHAssetChangeRequest creationRequestForAssetFromImage:image];
                                                            }
                                                         completionHandler:^(BOOL success, NSError *error) {
                                                             if (error) {
                                                                 if ([self.delegate respondsToSelector:@selector(someOtherError:)]) {
                                                                     [self.delegate someOtherError:error];
                                                                 }
                                                             }
                                                         }];
                                                }
                                            } else if (error) {
                                                if ([delegateBlock respondsToSelector:@selector(captureImageFailedWithError:)])
                                                    [delegateBlock captureImageFailedWithError:error];
                                            }

                                            [captureSessionBlock startRunning];
                                        }];
}

- (void)focusAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = self.videoInput.device;
    if (device.isFocusPointOfInterestSupported && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.focusPointOfInterest = point;
            device.focusMode = AVCaptureFocusModeAutoFocus;
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)])
                [self.delegate acquiringDeviceLockFailedWithError:error];
        }
    }
}

- (void)exposureAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = self.videoInput.device;
    if (device.isExposurePointOfInterestSupported && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.exposurePointOfInterest = point;
            device.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)])
                [self.delegate acquiringDeviceLockFailedWithError:error];
        }
    }
}

- (CGPoint)convertToPointOfInterestFrom:(CGRect)frame
                            coordinates:(CGPoint)viewCoordinates
                                  layer:(AVCaptureVideoPreviewLayer *)layer
{
    CGPoint pointOfInterest = (CGPoint) {0.5f, 0.5f};
    CGSize frameSize = frame.size;

    AVCaptureVideoPreviewLayer *videoPreviewLayer = layer;

    if ([[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResize])
        pointOfInterest =
                (CGPoint) {viewCoordinates.y / frameSize.height, 1.0f - (viewCoordinates.x / frameSize.width)};
    else {
        CGRect cleanAperture;
        for (AVCaptureInputPort *port in self.videoInput.ports) {
            if ([port mediaType] == AVMediaTypeVideo) {
                cleanAperture = CMVideoFormatDescriptionGetCleanAperture([port formatDescription], YES);
                CGSize apertureSize = cleanAperture.size;
                CGPoint point = viewCoordinates;

                CGFloat apertureRatio = apertureSize.height / apertureSize.width;
                CGFloat viewRatio = frameSize.width / frameSize.height;
                CGFloat xc = 0.5f;
                CGFloat yc = 0.5f;

                if ([[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspect]) {
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = frameSize.height;
                        CGFloat x2 = frameSize.height * apertureRatio;
                        CGFloat x1 = frameSize.width;
                        CGFloat blackBar = (x1 - x2) / 2;
                        if (point.x >= blackBar && point.x <= blackBar + x2) {
                            xc = point.y / y2;
                            yc = 1.0f - ((point.x - blackBar) / x2);
                        }
                    } else {
                        CGFloat y2 = frameSize.width / apertureRatio;
                        CGFloat y1 = frameSize.height;
                        CGFloat x2 = frameSize.width;
                        CGFloat blackBar = (y1 - y2) / 2;
                        if (point.y >= blackBar && point.y <= blackBar + y2) {
                            xc = ((point.y - blackBar) / y2);
                            yc = 1.0f - (point.x / x2);
                        }
                    }
                } else if ([[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = apertureSize.width * (frameSize.width / apertureSize.height);
                        xc = (point.y + ((y2 - frameSize.height) / 2.0f)) / y2;
                        yc = (frameSize.width - point.x) / frameSize.width;
                    } else {
                        CGFloat x2 = apertureSize.height * (frameSize.height / apertureSize.width);
                        yc = 1.0f - ((point.x + ((x2 - frameSize.width) / 2)) / x2);
                        xc = point.y / frameSize.height;
                    }
                }

                pointOfInterest = (CGPoint) {xc, yc};
                break;
            }
        }
    }

    return pointOfInterest;
}

@end
