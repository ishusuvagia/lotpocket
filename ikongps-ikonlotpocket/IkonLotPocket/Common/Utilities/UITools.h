
#import <UIKit/UIKit.h>
#import <SIAlertView/SIAlertView.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <JTSImageViewController/JTSImageInfo.h>
#import <JTSImageViewController/JTSImageViewController.h>
#import "UIImage+Tint.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString *const __nonnull kAlertButtonTitle;
extern NSString *const __nonnull kAlertButtonHandler;
extern NSString *const __nonnull kAlertButtonType;

@interface UITools : NSObject

///--------------------------------------
/// @name View
///--------------------------------------

+ (void)alert:(nullable NSString *)title message:(nullable NSString *)message type:(NotificationType)type;
+ (void)alert:(NSString *)title message:(NSString *)message type:(NotificationType)type handler:(SIAlertViewHandler)handler;
+ (void)promptAlert:(nullable NSString *)title
            message:(nullable NSString *)message
               type:(NotificationType)type
                 ok:(nullable SIAlertViewHandler)okHandler
             cancel:(nullable SIAlertViewHandler)cancelHandler;
+ (void)promptAlert:(nullable NSString *)title
            message:(nullable NSString *)message
               type:(NotificationType)type
            okTitle:(nullable NSString *)okTitle
        cancelTitle:(nullable NSString *)cancelTitle
                 ok:(nullable SIAlertViewHandler)okHandler
             cancel:(nullable SIAlertViewHandler)cancelHandler;
+ (void)promptRowAlert:(nullable NSString *)title
               message:(nullable NSString *)message
                  type:(NotificationType)type
            buttonList:(nullable NSArray<NSDictionary *> *)buttonList
           cancelTitle:(nullable NSString *)cancelTitle
                cancel:(nullable SIAlertViewHandler)cancelHandler;
+ (void)dropShadow:(nullable UIView *)view shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius offset:(CGSize)shadowOffset;
+ (void)dropShadow:(nullable UIView *)view color:(nullable UIColor *)color shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius offset:(CGSize)shadowOffset;
+ (MBProgressHUD *)showHorizontalBarProgressForView:(nullable UIView *)view withText:(nullable NSString *)text;
+ (MBProgressHUD *)showProgressForView:(nullable UIView *)view withText:(nullable NSString *)text;
+ (void)hideProgressForView:(UIView *)view;
+ (void)roundView:(UIView *)view cornerRadius:(CGFloat)cornerRadius;
+ (void)roundView:(nullable UIView *)view cornerRadius:(CGFloat)cornerRadius shadowOpacity:(CGFloat)shadowOpacity
     shadowRadius:(CGFloat)shadowRadius offset:(CGSize)shadowOffset;
+ (void)activateConstraintsForView:(nullable UIView *)view respectToParentView:(nullable UIView *)parentView;
+ (CGRect)currentDeviceRect;

///--------------------------------------
/// @name Color
///--------------------------------------

+ (nonnull UIColor *)lighterColorForColor:(nullable UIColor *)color;
+ (nonnull UIColor *)darkerColorForColor:(nullable UIColor *)color;
+ (nonnull UIColor *)colorFromHexCode:(nullable NSString *)hexString;
+ (nonnull UIColor *)blendedColorWithForegroundColor:(nullable UIColor *)foregroundColor backgroundColor:(nullable UIColor *)backgroundColor percentBlend:(CGFloat)percentBlend;
+ (nonnull UIColor *)nextColor:(nullable UIColor *)color;
+ (NSAttributedString *)highlightString:(NSString *)string inText:(NSString *)text withColor:(UIColor *)color;
+ (nonnull UIColor *)getColorForNotificationType:(NotificationType)type;

///--------------------------------------
/// @name Images
///--------------------------------------

+ (nullable UIImage *)tintImage:(nullable UIImage *)source withColor:(nullable UIColor *)color;
+ (nullable UIImage *)drawText:(nullable NSString *)text inImage:(nullable UIImage *)image font:(UIFont *)font atPoint:(CGPoint)point;
+ (void)openLightBoxForImage:(nullable UIImage *)image inConstraintToView:(nullable UIView *)view atViewController:(nullable id)viewController;

///--------------------------------------
/// @name Buttons
///--------------------------------------

+ (nonnull UIButton *)createBarButtonWithTitle:(nullable NSString *)title;
+ (nonnull UIButton *)createActivityBarButton;

@end

NS_ASSUME_NONNULL_END
