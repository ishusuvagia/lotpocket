
#import "BusinessUtils.h"

@implementation BusinessUtils

///--------------------------------------
#pragma mark - Strings
///--------------------------------------

+ (NSString *)getBatteryLevelString:(CGFloat)batteryLevel
{
    if (batteryLevel > 11.8f) {
        return LOCALIZED(@"goodbattery");
    } else if (batteryLevel >= 10.5f && batteryLevel < 11.79f) {
        return LOCALIZED(@"lowbattery");
    } else {
        return LOCALIZED(@"deadbattery");
    }
}

///--------------------------------------
#pragma mark - Brand Colors
///--------------------------------------

+ (UIColor *)getVehicleConditionColor:(NSString *)condition
{
    if (![Utils stringIsNullOrEmpty:condition]) {
        if ([condition caseInsensitiveCompare:@"new"] == NSOrderedSame) {
            return [AppColorScheme brandGreen];
        } else if ([condition caseInsensitiveCompare:@"used"] == NSOrderedSame) {
            return [AppColorScheme brandOrange];
        } else if ([condition caseInsensitiveCompare:@"certified"] == NSOrderedSame) {
            return [AppColorScheme brandBlue];
        }
    }
    return [AppColorScheme brandGray];
}

+ (UIColor *)getBatteryLevelColor:(CGFloat)batteryLevel
{
    if (batteryLevel > 11.8f) {
        return [AppColorScheme green];
    } else if (batteryLevel >= 10.5f && batteryLevel < 11.79f) {
        return [AppColorScheme orange];
    } else {
        return [AppColorScheme red];
    }
}

///--------------------------------------
#pragma mark - Images
///--------------------------------------

+ (UIImage *)getBatteryIconImage:(CGFloat)batteryLevel
{
    if (batteryLevel > 11.8f) {
        return [UIImage imageNamed:@"inventory-battery-full"];
    } else if (batteryLevel >= 10.5f && batteryLevel < 11.79f) {
        return [UIImage imageNamed:@"inventory-battery-low"];
    } else {
        return [UIImage imageNamed:@"inventory-battery-bad"];
    }
}

///--------------------------------------
#pragma mark - Data Sources
///--------------------------------------

+ (NSArray *)vehicleYears
{
    NSMutableArray *availableYears = [NSMutableArray array];
    NSInteger currentYear = [Utils getYear:[NSDate date]];
    for (NSUInteger idx = currentYear; idx >= 1990; idx--) {
        NSString *yearValue = [[@(idx) stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [availableYears addObject:yearValue];
    }
    return availableYears;
 }

+ (NSArray *)vehicleBodyStyles
{
    return @[@"sedan", @"suv", @"pickup"];
}

+ (NSArray *)vehicleConditions
{
    return @[@"all", @"new", @"used", @"certified"];
}

+ (NSArray *)vehicleTransmissions
{
    return @[@"automatic", @"manual"];
}

+ (NSArray *)vehicleFuelTypes
{
    return @[@"regular_unleaded", @"premium_unleaded", @"gasoline", @"diesel", @"flex_fuel"];
}

///--------------------------------------
#pragma mark - Custom Logics
///--------------------------------------

+ (CGFloat)getBatteryLevel:(NSString *)batteryLevel
{
    if (![Utils stringIsNullOrEmpty:batteryLevel]) {
        NSCharacterSet *numberCharset = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-"];
        NSScanner *scanner = [NSScanner scannerWithString:batteryLevel];
        float value = 0.0f;
        while (![scanner isAtEnd]) {
            [scanner scanUpToCharactersFromSet:numberCharset intoString:NULL];
            [scanner scanFloat:&value];
        }
        return value;
    } else {
        return 0.0f;
    }
}

@end
