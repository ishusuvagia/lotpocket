
#import <Foundation/Foundation.h>

static CGFloat const kMinimumVehiclePrice = 1000.0f;
static CGFloat const kMaximumVehiclePrice = 100000.0f;
static CGFloat const kMinimumVehicleMileage = 0.0f;
static CGFloat const kMaximumVehicleMileage = 300000.0f;

@interface BusinessUtils : NSObject

///--------------------------------------
/// @name Strings
///--------------------------------------

+ (nonnull NSString *)getBatteryLevelString:(CGFloat)batteryLevel;

///--------------------------------------
/// @name Brand Colors
///--------------------------------------

+ (nonnull UIColor *)getVehicleConditionColor:(nullable NSString *)condition;
+ (nonnull UIColor *)getBatteryLevelColor:(CGFloat)batteryLevel;

///--------------------------------------
/// @name Images
///--------------------------------------

+ (nonnull UIImage *)getBatteryIconImage:(CGFloat)batteryLevel;

///--------------------------------------
/// @name Data Sources
///--------------------------------------

+ (nonnull NSArray *)vehicleYears;
+ (nonnull NSArray *)vehicleBodyStyles;
+ (nonnull NSArray *)vehicleConditions;
+ (nonnull NSArray *)vehicleTransmissions;
+ (nonnull NSArray *)vehicleFuelTypes;

///--------------------------------------
/// @name Custom Logics
///--------------------------------------

+ (CGFloat)getBatteryLevel:(nullable NSString *)batteryLevel;

@end
