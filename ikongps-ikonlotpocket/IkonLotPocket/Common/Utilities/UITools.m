
#import "UITools.h"
#import "AppFont.h"
#import "AppColorScheme.h"

NSString *const kAlertButtonTitle = @"buttontitle";
NSString *const kAlertButtonHandler = @"buttonhandler";
NSString *const kAlertButtonType = @"buttontype";

@implementation UITools

///--------------------------------------
#pragma mark - Private Helpers
///--------------------------------------

+ (SIAlertView *)_buildAlert:(NSString *)title message:(NSString *)message type:(NotificationType)type
{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
    switch (type) {
        case Normal:alertView.titleColor = [AppColorScheme primary];
            break;
        case Success:alertView.titleColor = [AppColorScheme success];
            break;
        case Warning:alertView.titleColor = [AppColorScheme warning];
            break;
        case Error:alertView.titleColor = [AppColorScheme error];
            break;
        default:alertView.titleColor = [AppColorScheme primary];
            break;
    }
    
    return alertView;
}

///--------------------------------------
#pragma mark - View
///--------------------------------------

+ (void)alert:(NSString *)title message:(NSString *)message type:(NotificationType)type
{
    SIAlertView *alertView = [self _buildAlert:title message:message type:type];
    [alertView addButtonWithTitle:[LOCALIZED(@"ok") uppercaseString] type:SIAlertViewButtonTypeCancel handler:^(SIAlertView *alert) {}];
    [alertView show];
}

+ (void)alert:(NSString *)title message:(NSString *)message type:(NotificationType)type handler:(SIAlertViewHandler)handler
{
    SIAlertView *alertView = [self _buildAlert:title message:message type:type];
    [alertView addButtonWithTitle:[LOCALIZED(@"ok") uppercaseString] type:SIAlertViewButtonTypeCancel handler:handler];
    [alertView show];
}

+ (void)promptAlert:(NSString *)title
            message:(NSString *)message
               type:(NotificationType)type
                 ok:(SIAlertViewHandler)okHandler
             cancel:(SIAlertViewHandler)cancelHandler
{
    SIAlertView *alertView = [self _buildAlert:title message:message type:type];
    [alertView addButtonWithTitle:[LOCALIZED(@"yes") uppercaseString] type:SIAlertViewButtonTypeDefault handler:okHandler];
    [alertView addButtonWithTitle:[LOCALIZED(@"no") uppercaseString] type:SIAlertViewButtonTypeCancel handler:cancelHandler];
    [alertView show];
}

+ (void)promptAlert:(NSString *)title
            message:(NSString *)message
               type:(NotificationType)type
            okTitle:(NSString *)okTitle
        cancelTitle:(NSString *)cancelTitle
                 ok:(SIAlertViewHandler)okHandler
             cancel:(SIAlertViewHandler)cancelHandler
{
    SIAlertView *alertView = [self _buildAlert:title message:message type:type];
    [alertView addButtonWithTitle:okTitle ?: [LOCALIZED(@"yes") uppercaseString] type:SIAlertViewButtonTypeDefault handler:okHandler];
    [alertView addButtonWithTitle:cancelTitle ?: [LOCALIZED(@"no") uppercaseString] type:SIAlertViewButtonTypeCancel
                          handler:cancelHandler];
    [alertView show];
}

+ (void)promptRowAlert:(NSString *)title
               message:(NSString *)message
                  type:(NotificationType)type
            buttonList:(NSArray<NSDictionary *> *)buttonList
           cancelTitle:(NSString *)cancelTitle
                cancel:(SIAlertViewHandler)cancelHandler
{
    if (!buttonList || buttonList.count == 0) return;
    
    SIAlertView *alertView = [self _buildAlert:title message:message type:type];
    for (NSDictionary *dict in buttonList) {
        [alertView addButtonWithTitle:dict[kAlertButtonTitle] type:[dict[kAlertButtonType] integerValue]
                              handler:dict[kAlertButtonHandler]];
    }
    [alertView addButtonWithTitle:cancelTitle ?: [LOCALIZED(@"no") uppercaseString] type:SIAlertViewButtonTypeCancel
                          handler:cancelHandler];
    [alertView setButtonsListStyle:SIAlertViewButtonsListStyleRows];
    [alertView show];
}


+ (void)dropShadow:(UIView *)view shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius offset:(CGSize)shadowOffset
{
    [self dropShadow:view color:[UIColor blackColor] shadowOpacity:shadowOpacity shadowRadius:shadowRadius offset:shadowOffset];
}

+ (void)dropShadow:(UIView *)view color:(nullable UIColor *)color shadowOpacity:(CGFloat)shadowOpacity shadowRadius:(CGFloat)shadowRadius offset:(CGSize)shadowOffset
{
    view.layer.masksToBounds = NO;
    view.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    view.layer.shouldRasterize = YES;
    view.layer.shadowColor = [color CGColor];
    view.layer.shadowOffset = shadowOffset;
    view.layer.shadowRadius = shadowRadius;
    view.layer.shadowOpacity = shadowOpacity;
}

+ (MBProgressHUD *)showHorizontalBarProgressForView:(UIView *)view withText:(NSString *)text
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    hud.offset = CGPointMake(0.0f, -32.0f);
    hud.animationType = MBProgressHUDAnimationFade;
    hud.label.font = [AppFont titleLabelFont];
    if (text) hud.label.text = text;
    return hud;
}

+ (MBProgressHUD *)showProgressForView:(UIView *)view withText:(NSString *)text
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 37.0f, 37.0f)];
    activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicatorView startAnimating];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = activityIndicatorView;
    hud.offset = CGPointMake(0.0f, -32.0f);
    hud.animationType = MBProgressHUDAnimationFade;
    hud.label.font = [AppFont titleLabelFont];
    if (text) hud.label.text = text;
    return hud;
}

+ (void)hideProgressForView:(UIView *)view
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:view animated:YES];
    });
}

+ (void)roundView:(UIView *)view cornerRadius:(CGFloat)cornerRadius
{
    view.layer.cornerRadius = cornerRadius;
    view.layer.masksToBounds = YES;
}

+ (void)roundView:(UIView *)view
     cornerRadius:(CGFloat)cornerRadius
    shadowOpacity:(CGFloat)shadowOpacity
     shadowRadius:(CGFloat)shadowRadius
           offset:(CGSize)shadowOffset
{
    [self roundView:view cornerRadius:cornerRadius];
    [self dropShadow:view shadowOpacity:cornerRadius shadowRadius:shadowRadius offset:shadowOffset];
}

+ (void)activateConstraintsForView:(UIView *)view respectToParentView:(UIView *)parentView
{
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                        attribute:NSLayoutAttributeBottom
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:parentView
                                                                        attribute:NSLayoutAttributeBottom
                                                                       multiplier:1.0
                                                                         constant:0];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:parentView
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1.0
                                                                      constant:0];
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                      attribute:NSLayoutAttributeLeft
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:parentView
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:0];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:view
                                                                       attribute:NSLayoutAttributeRight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:parentView
                                                                       attribute:NSLayoutAttributeRight
                                                                      multiplier:1.0
                                                                        constant:0];
    
    [NSLayoutConstraint
     activateConstraints:[NSArray arrayWithObjects:topConstraint, leftConstraint, bottomConstraint,
                          rightConstraint, nil]];
}

+ (CGRect)currentDeviceRect
{
    return [[UIScreen mainScreen] bounds];
}

///--------------------------------------
#pragma mark - Color
///--------------------------------------

+ (UIColor *)lighterColorForColor:(UIColor *)color
{
    CGFloat r, g, b, a;
    if ([color getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:(CGFloat) MIN(r + 0.1, 1.0) green:(CGFloat) MIN(g + 0.1, 1.0) blue:(CGFloat) MIN(b + 0.1, 1.0) alpha:a];
    return nil;
}

+ (UIColor *)darkerColorForColor:(UIColor *)color
{
    CGFloat r, g, b, a;
    if ([color getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:(CGFloat) MAX(r - 0.1, 0.0) green:(CGFloat) MAX(g - 0.1, 0.0) blue:(CGFloat) MAX(b - 0.1, 0.0) alpha:a];
    return nil;
}

+ (UIColor *)colorFromHexCode:(NSString *)hexString
{
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];

    if ([cleanString length] == 3) {
        cleanString = [NSString
                stringWithFormat:@"%@%@%@%@%@%@", [cleanString substringWithRange:NSMakeRange(0, 1)],
                                 [cleanString substringWithRange:NSMakeRange(0, 1)],
                                 [cleanString substringWithRange:NSMakeRange(1, 1)],
                                 [cleanString substringWithRange:NSMakeRange(1, 1)],
                                 [cleanString substringWithRange:NSMakeRange(2, 1)],
                                 [cleanString substringWithRange:NSMakeRange(2, 1)]];
    }

    if ([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }

    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];

    CGFloat red = ((baseValue >> 24) & 0xFF) / 255.0f;
    CGFloat green = ((baseValue >> 16) & 0xFF) / 255.0f;
    CGFloat blue = ((baseValue >> 8) & 0xFF) / 255.0f;
    CGFloat alpha = ((baseValue >> 0) & 0xFF) / 255.0f;

    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (UIColor *)blendedColorWithForegroundColor:(UIColor *)foregroundColor backgroundColor:(UIColor *)backgroundColor percentBlend:(CGFloat)percentBlend
{
    CGFloat onRed, offRed, newRed, onGreen, offGreen, newGreen, onBlue, offBlue, newBlue, onWhite, offWhite;

    if ([foregroundColor getWhite:&onWhite alpha:nil]) {
        onRed = onWhite;
        onBlue = onWhite;
        onGreen = onWhite;
    } else {
        [foregroundColor getRed:&onRed green:&onGreen blue:&onBlue alpha:nil];
    }

    if ([backgroundColor getWhite:&offWhite alpha:nil]) {
        offRed = offWhite;
        offBlue = offWhite;
        offGreen = offWhite;
    } else {
        [backgroundColor getRed:&offRed green:&offGreen blue:&offBlue alpha:nil];
    }

    newRed = onRed * percentBlend + offRed * (1 - percentBlend);
    newGreen = onGreen * percentBlend + offGreen * (1 - percentBlend);
    newBlue = onBlue * percentBlend + offBlue * (1 - percentBlend);

    return [UIColor colorWithRed:newRed green:newGreen blue:newBlue alpha:1.0];
}

+ (UIColor *)nextColor:(UIColor *)color
{
    UIColor *nextColor;

    CGFloat hue;
    CGFloat saturation;
    CGFloat brightness;
    CGFloat alpha;

    BOOL success = [color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];

    if (success) {
        hue = fmodf(hue + kGoldenRatio, 1.0);
        nextColor = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1.0f];
    }

    return nextColor;
}

+ (NSAttributedString *)highlightString:(NSString *)string inText:(NSString *)text withColor:(UIColor *)color
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];

    NSString *pattern = [NSString stringWithFormat:@"(%@)", string];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSRange range = NSMakeRange(0, text.length);

    [regex enumerateMatchesInString:text
                            options:NSMatchingReportProgress
                              range:range
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                             NSRange subStringRange = [result rangeAtIndex:1];
                             [attributedString addAttribute:NSForegroundColorAttributeName value:color
                                                      range:subStringRange];
                         }];

    return attributedString;
}

+ (UIColor *)getColorForNotificationType:(NotificationType)type
{
    switch (type) {
        case Normal:return [AppColorScheme normal];
        case Primary:return [AppColorScheme primary];
        case Success:return [AppColorScheme success];
        case Warning:return [AppColorScheme warning];
        case Error:return [AppColorScheme error];
        default:return [AppColorScheme primary];
    }
}

///--------------------------------------
/// @name Image
///--------------------------------------

+ (UIImage *)tintImage:(UIImage *)source withColor:(UIColor *)color
{
    // Begin a new image context, to draw our colored image onto with the right scale
    UIGraphicsBeginImageContextWithOptions(source.size, NO, [UIScreen mainScreen].scale);
    
    // Get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the fill color
    [color setFill];
    
    // Translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, source.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, source.size.width, source.size.height);
    CGContextDrawImage(context, rect, source.CGImage);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // Generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Return the color-burned image
    return coloredImg;
}

+ (UIImage *)drawText:(NSString *)text inImage:(UIImage *)image font:(UIFont *)font atPoint:(CGPoint)point
{
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0, image.size.width, image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (void)openLightBoxForImage:(UIImage *)image inConstraintToView:(UIView *)view atViewController:(id)viewController
{
    JTSImageInfo *imageInfo = [JTSImageInfo new];
    imageInfo.image = image;
    imageInfo.referenceRect = view.frame;
    imageInfo.referenceView = view.superview;
    
    // Setup view controller
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc] initWithImageInfo:imageInfo
                                                                                       mode:JTSImageViewControllerMode_Image
                                                                            backgroundStyle:JTSImageViewControllerBackgroundOption_Blurred];
    
    // Present the view controller.
    [imageViewer showFromViewController:viewController
                             transition:JTSImageViewControllerTransition_FromOriginalPosition];
}

///--------------------------------------
#pragma mark - Buttons
///--------------------------------------

+ (UIButton *)createBarButtonWithTitle:(NSString *)title
{
    CGSize buttonSize = [title sizeWithAttributes:@{NSFontAttributeName:[AppFont buttonTitleFont]}];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0.0f, 0.0f, buttonSize.width, buttonSize.height);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateHighlighted];
    [button.titleLabel setFont:[AppFont buttonTitleFont]];
    return button;
}

+ (UIButton *)createActivityBarButton
{
    UIButton *button =
            [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kBarButtonDefaultWidth, kBarButtonDefaultHeight)];
    UIActivityIndicatorView *indicator =
            [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    CGFloat halfButtonHeight = button.bounds.size.height / 2;
    CGFloat buttonWidth = button.bounds.size.width;
    indicator.center = CGPointMake(buttonWidth - halfButtonHeight, halfButtonHeight);
    [button addSubview:indicator];
    [indicator startAnimating];
    return button;
}

@end
