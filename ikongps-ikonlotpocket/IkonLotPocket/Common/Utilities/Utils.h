
#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>
#import <DateTools/DateTools.h>
#import "Constants.h"
#import "DateFormatter.h"
#import "Application.h"

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject

///--------------------------------------
/// @name Strings
///--------------------------------------

+ (nonnull NSString *)getBooleanString:(BOOL)value;
+ (nonnull NSString *)formatFirstname:(nullable NSString *)firstname andLastName:(nullable NSString *)lastname;
+ (BOOL)stringIsNullOrEmpty:(nullable NSString *)string;
+ (nonnull NSDictionary *)convertToJsonDict:(nonnull NSString *)value;
+ (nonnull NSString *)convertToJsonString:(nonnull NSDictionary *)dict;
+ (nonnull NSString *)getFormattedPhoneNumber:(nullable NSString *)phoneNumber;
+ (nonnull NSString *)getUnformattedPhoneNumber:(nullable NSString *)formattedPhoneNumber;
+ (nonnull NSString *)getCurrencyString:(CGFloat)currencyAmount withDecimal:(BOOL)withDecimal;
+ (nonnull NSString *)getNumberString:(CGFloat)number;
+ (nonnull NSString *)capitalizedOnlyFirstLetter:(nullable NSString *)string;
+ (nullable NSString *)formatCommaSpaceValue:(nullable NSArray *)values;
+ (nullable NSArray *)commaSpaceValueToArray:(nullable NSString *)value;

///--------------------------------------
/// @name Maths
///--------------------------------------

+ (double)calculateDistanceInMetersBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2;
+ (double)calculateDistanceInMilesBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2;

///--------------------------------------
/// @name System
///--------------------------------------

+ (nonnull id)objectOrNull:(nullable id)object;
+ (void)saveDictionaryToUserDefaults:(nullable NSDictionary *)dict;
+ (void)saveObjectToUserDefaults:(nullable id)object forKey:(nullable NSString *)key;
+ (nonnull id)getObjectFromUserDefaults:(nullable NSString *)key;
+ (void)removeObjectFromUserDefaults:(nullable NSString *)key;
+ (nonnull NSDictionary *)dictWithPropertiesOfObject:(nullable id)obj;
+ (nonnull NSMutableDictionary *)getPlist:(nullable NSString *)plistName;
+ (nonnull NSMutableDictionary *)getParamPreferences:(nullable NSString *)plistName;
+ (nonnull NSMutableDictionary *)getParamPreferences:(nullable NSString *)plistName filterKeys:(nullable NSArray *)filterKeys;
+ (nullable NSURL *)getLocalDirectory:(nullable NSString *)folderName;

///--------------------------------------
/// @name Date
///--------------------------------------

+ (BOOL)isDayTime;
+ (NSInteger)getYear:(nullable NSDate *)date;
+ (NSInteger)getMonth:(nullable NSDate *)date;
+ (NSInteger)getDay:(nullable NSDate *)date;
+ (nonnull NSDate *)getDateOnly:(nullable NSDate *)date;
+ (nullable NSDate *)getStartOfBusinessWeek:(nullable NSDate *)date;
+ (nullable NSDate *)nextWeekFromDate:(nullable NSDate *)date;
+ (nullable NSDate *)lastWeekFromDate:(nullable NSDate *)date;
+ (nullable NSDate *)getDateFrom:(nullable NSDate *)date numDays:(NSInteger)numDays;
+ (nullable NSDate *)getEndOfBusinessWeek:(nullable NSDate *)date;
+ (NSInteger)dateDifference:(nullable NSDate *)firstDate secondDate:(nullable NSDate *)secondDate;
+ (NSInteger)hourDifference:(nullable NSDate *)firstDate secondDate:(nullable NSDate *)secondDate;
+ (NSInteger)minuteDifference:(nullable NSDate *)firstDate secondDate:(nullable NSDate *)secondDate;
+ (nullable NSDate *)convertToUTCDate:(nullable NSDate *)date;
+ (nonnull NSDate *)convertStringToNSDate:(nullable NSString *)dateStr;
+ (nonnull NSDate *)convertDateTimeToNSDate:(nullable NSString *)dateTime;
+ (nonnull id)convertNSDateToDateTime:(nullable NSDate *)date;
+ (nonnull NSString *)convertNSDateToString:(nullable NSDate *)date;
+ (nonnull NSString *)convertNSDateToUTCString:(NSDate *)date;
+ (nonnull NSString *)convertNSDatetoISO8601String:(nullable NSDate *)date;
+ (nonnull NSString *)convertNSDateToGMTString:(nullable NSDate *)date;
+ (nonnull NSString *)convertNSDateToString:(nullable NSDate *)date withFormat:(nonnull NSString *)format;
+ (nonnull NSDate *)convertUTCStringToNSDate:(nullable NSString *)dateStr;
+ (nonnull NSDate *)convertISO8601StringtoNSDate:(nullable NSString *)dateStr;
+ (nonnull NSDate *)convertGMTStringToNSDate:(nullable NSString *)dateStr;
+ (nullable NSDate *)convertStringToNSDate:(nullable NSString *)dateStr withFormat:(nonnull NSString *)format;
+ (nullable NSDate *)convertDotNetDateToNSDate:(nullable NSString *)dotnetDate;
+ (nullable NSString *)convertNSDateToDotNetDate:(nullable NSDate *)date;

///--------------------------------------
/// @name Validation
///--------------------------------------

+ (BOOL)validateAlpha:(nullable NSString *)candidate;
+ (BOOL)validateAlphaSpaces:(nullable NSString *)candidate;
+ (BOOL)validateAlphanumeric:(nullable NSString *)candidate;
+ (BOOL)validateAlphanumericDash:(nullable NSString *)candidate;
+ (BOOL)validateName:(nullable NSString *)candidate;
+ (BOOL)validateStringInCharacterSet:(nullable NSString *)string characterSet:(nullable NSCharacterSet *)characterSet;
+ (BOOL)validateNotEmpty:(nullable NSString *)candidate;
+ (BOOL)validateEmail:(nullable NSString *)candidate;
+ (BOOL)validatePhoneNumber:(nullable NSString *)candidate;

///--------------------------------------
/// @name External
///--------------------------------------

+ (void)openExternalNavigationFromLocation:(CLLocationCoordinate2D)fromLocation
                                toLocation:(CLLocationCoordinate2D)toLocation;
+ (void)call:(nullable NSString *)phoneNumber;
+ (void)text:(nullable NSString *)phoneNumber;
+ (void)email:(nullable NSString *)address subject:(nullable NSString *)subject body:(nullable NSString *)body;
+ (void)showLocalNotification:(nullable NSString *)notificationBody withDate:(nullable NSDate *)notificationDate;

@end

NS_ASSUME_NONNULL_END
