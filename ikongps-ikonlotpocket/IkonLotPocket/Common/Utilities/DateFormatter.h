
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DateFormatter : NSObject

///--------------------------------------
/// @name Class
///--------------------------------------

+ (instancetype)sharedFactory;

///--------------------------------------
/// @name Factory Methods
///--------------------------------------

- (nonnull NSDateFormatter *)dateFormatterWithFormat:(nullable NSString *)format andLocale:(nullable NSLocale *)locale;
- (nonnull NSDateFormatter *)dateFormatterWithFormat:(nullable NSString *)format andLocaleIdentifier:(nullable NSString *)localeIdentifier;
- (nonnull NSDateFormatter *)dateFormatterWithFormat:(nullable NSString *)format;
- (nonnull NSDateFormatter *)dateFormatterWithDateStyle:(NSDateFormatterStyle)dateStyle
                                              timeStyle:(NSDateFormatterStyle)timeStyle
                                              andLocale:(nullable NSLocale *)locale;
- (nonnull NSDateFormatter *)dateFormatterWithDateStyle:(NSDateFormatterStyle)dateStyle
                                              timeStyle:(NSDateFormatterStyle)timeStyle
                                    andLocaleIdentifier:(nullable NSString *)localeIdentifier;
- (nonnull NSDateFormatter *)dateFormatterWithDateStyle:(NSDateFormatterStyle)dateStyle andTimeStyle:(NSDateFormatterStyle)timeStyle;

@end

NS_ASSUME_NONNULL_END
