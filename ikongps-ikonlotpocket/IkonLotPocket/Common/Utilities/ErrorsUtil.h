
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

///--------------------------------------
/// @name Error Constants
///--------------------------------------

extern NSString *const __nonnull GeneralErrorDomain;
extern NSString *const __nonnull WebRequestErrorDomain;

extern NSString *const __nonnull ErrorCodeKey;
extern NSString *const __nonnull ErrorMessageKey;
extern NSString *const __nonnull ErrorDomainKey;

///--------------------------------------
/// @name Error Codes
///--------------------------------------

typedef NS_ENUM(NSInteger, ErrorCode)
{
    kErrorParsingException = 700,
    kErrorAPICompatible = 800,
    kErrorAPIInconsistentData = 801,
    kErrorAPINullData = 802,
    kErrorInternalServer = 999,
    kErrorInvalidRequest = 1001,
    kErrorInvalidAuthentication = 1002,
    kErrorDuplicatedUsername = 1003,
    kErrorInvalidRegistration = 1004,
    kErrorInvalidAuthToken = 1007,
    kErrorExpiredAuthToken = 1008,
    kErrorInvalidEmailAddress = 1009,
    kErrorUnsupportedFile = 1010,
    kErrorRequestExpired = 1011,
    kErrorPersistenceException = 2001,
    kErrorPersistenceInvalidData = 2002
};

@interface ErrorsUtil : NSObject

+ (nullable NSError *)errorWithCode:(ErrorCode)code message:(nullable NSString *)message;
+ (nullable NSError *)errorWithCode:(ErrorCode)code message:(nullable NSString *)message shouldLog:(BOOL)shouldLog;
+ (nullable NSError *)errorWithCode:(ErrorCode)code domain:(nullable NSString *)domain message:(nullable NSString *)message;
+ (nullable NSError *)errorWithCode:(ErrorCode)code
                             domain:(nullable NSString *)domain
                            message:(nullable NSString *)message
                          shouldLog:(BOOL)shouldLog;
+ (nullable NSError *)errorFromResult:(nullable NSDictionary *)result;
+ (nullable NSError *)errorFromResult:(nullable NSDictionary *)result shouldLog:(BOOL)shouldLog;
+ (NSString *)getErrorMessage:(ErrorCode)code;

NS_ASSUME_NONNULL_END

@end
