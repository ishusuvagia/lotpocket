
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

#define GEOFENCEMANAGER [GeofenceManager sharedManager]

@class GeofenceManager;

typedef NS_ENUM(NSInteger, GeofenceManagerState)
{
    GeofenceManagerStateIdle, GeofenceManagerStateProcessing, GeofenceManagerStateFailed
};

@protocol GeofenceManagerDataSource <NSObject>

@required
- (nonnull NSArray *)geofencesForGeofenceManager:(nonnull GeofenceManager *)geofenceManager;

@end

@protocol GeofenceManagerDelegate <NSObject>

@optional
- (void)geofenceManager:(nullable GeofenceManager *)geofenceManager isInsideGeofence:(nullable CLRegion *)geofence;
- (void)geofenceManager:(nullable GeofenceManager *)geofenceManager didExitGeofence:(nullable CLRegion *)geofence;
- (void)geofenceManager:(nullable GeofenceManager *)geofenceManager didChangeState:(GeofenceManagerState)state;
- (void)geofenceManager:(nullable GeofenceManager *)geofenceManager didFailWithError:(nullable NSError *)error;

@end

@interface GeofenceManager : NSObject

@property (nonatomic, weak) id <GeofenceManagerDataSource> dataSource;
@property (nonatomic, weak) id <GeofenceManagerDelegate> delegate;
@property (nonatomic, assign) GeofenceManagerState state;

+ (nonnull instancetype)sharedManager;
- (void)reloadGeofences;
- (nonnull NSString *)geofenceStateDescription;

@end

NS_ASSUME_NONNULL_END
