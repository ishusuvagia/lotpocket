
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ImageActionButton)
{
    IMAGE_BUTTON_CHOOSEFROMALBUM,
    IMAGE_BUTTON_TAKEPICTURE,
    IMAGE_BUTTON_LOGOUT
};

NS_ASSUME_NONNULL_BEGIN

@class ActionSheet;
@protocol ActionSheetProtocol <NSObject>
@optional
- (void)actionSheet:(nonnull ActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
@end

@interface ActionSheet : NSObject

@property(nonatomic, readonly) NSInteger cancelButtonIndex;

+ (nonnull NSString *)getLabel:(NSInteger)imageActionButton;
- (instancetype)initWithTitle:(nullable NSString *)title
                   sourceView:(nullable UIView *)sourceView
                     protocol:(nullable __weak id<ActionSheetProtocol>)protocol
             availableButtons:(nullable NSArray *)availableButtons
            cancelButtonTitle:(nullable NSString *)cancelButtonTitle;
- (void)showInViewController:(nullable UIViewController *)viewController;
- (void)dismissActionSheet;

@end

NS_ASSUME_NONNULL_END
