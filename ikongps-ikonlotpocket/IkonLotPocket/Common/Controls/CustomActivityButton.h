
#import "CustomButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomActivityButton : CustomButton

@property (nonatomic, assign, readonly) BOOL isLoading;
@property (nonatomic, assign) UIActivityIndicatorViewStyle activityStyle;
@property (nullable, nonatomic, strong) NSString *identifier;

- (void)setActivityIndicatorStyle:(UIActivityIndicatorViewStyle)style;
- (void)showActivity;
- (void)hideActivity;

@end

NS_ASSUME_NONNULL_END
