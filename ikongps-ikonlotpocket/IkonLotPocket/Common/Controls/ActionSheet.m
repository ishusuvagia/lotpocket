
#import <QuartzCore/QuartzCore.h>
#import "ActionSheet.h"
#import "Constants.h"
#import "Logger.h"

@interface ActionSheet ()

@property (nonatomic, strong) UIAlertController *_alertController;
@property (nonatomic, strong) NSArray *_neededButtons;
@property (nonatomic, assign) id<ActionSheetProtocol> _protocol;

@end

@implementation ActionSheet

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (NSString *)getLabel:(NSInteger)imageActionButton
{
    switch (imageActionButton)
    {
        case IMAGE_BUTTON_CHOOSEFROMALBUM:
            return LOCALIZED(@"choosefromalbum");
        case IMAGE_BUTTON_TAKEPICTURE:
            return LOCALIZED(@"takepicture");
        case IMAGE_BUTTON_LOGOUT:
            return LOCALIZED(@"logout");
        default:
            return kEmptyString;
    }
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithTitle:(NSString *)title
                   sourceView:(UIView *)sourceView
                     protocol:(__weak id<ActionSheetProtocol>)protocol
             availableButtons:(NSArray *)availableButtons
            cancelButtonTitle:(NSString *)cancelButtonTitle
{
    self = [super init];
    
    if (self)
    {
        __protocol = protocol;
        __neededButtons = availableButtons;
        __alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (NSInteger i = 0; i < self._neededButtons.count; i++)
        {
            NSInteger buttonValue = [self._neededButtons[i] integerValue];
            NSString *buttonText = [ActionSheet getLabel:buttonValue];
            
            [self._alertController addAction:[UIAlertAction actionWithTitle:buttonText
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction *action) {
                                                                        [self actionSheet: self clickedButtonAtIndex:buttonValue];
                                                                    }]];
        }
        
        [self._alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle
                                                                  style:UIAlertActionStyleCancel
                                                                handler:^(UIAlertAction *action) {
                                                                    [self actionSheet: self clickedButtonAtIndex: self.cancelButtonIndex];
                                                                }]];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            self._alertController.popoverPresentationController.sourceView = sourceView;
            self._alertController.popoverPresentationController.sourceRect = sourceView.bounds;
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissActionSheet)
                                                     name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    
    return self;
}

- (void)dealloc
{
    TRACE_HERE;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)actionSheet:(ActionSheet *)sender clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([self._protocol respondsToSelector:@selector(actionSheet:clickedButtonAtIndex:)]) {
        [self._protocol actionSheet:self clickedButtonAtIndex:buttonIndex];
    }
}

- (void)showInViewController:(UIViewController *)viewController
{
    [viewController presentViewController:self._alertController animated:YES completion:nil];
}

- (void)dismissActionSheet
{
    [self._alertController dismissViewControllerAnimated:NO completion:nil];
}

- (NSInteger)cancelButtonIndex
{
    return -1;
}

@end
