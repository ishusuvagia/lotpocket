
#import "CustomButton.h"
#import "UITools.h"

@implementation CustomButton

///--------------------------------------
#pragma mark - Init
///--------------------------------------

+ (instancetype)button
{
    return [self buttonWithType:UIButtonTypeSystem];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customize];
    }
    return self;
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (void)layoutIfNeeded
{
    // TRACE_HERE;
}

- (void)customize
{
    // Corner radius
    self.layer.cornerRadius = 5.0f;
    self.clipsToBounds = YES;
}

///--------------------------------------
#pragma mark - Accessors
///--------------------------------------

- (void)setHasShadow:(BOOL)hasShadow
{
    _hasShadow = hasShadow;
    if (_hasShadow) {
        [UITools dropShadow:self shadowOpacity:0.5f shadowRadius:1.0f offset:CGSizeZero];
    }
}

- (void)setHasBorder:(BOOL)hasBorder
{
    _hasBorder = hasBorder;
    if (_hasBorder) {
        self.layer.borderColor = [UITools darkerColorForColor:self.backgroundColor].CGColor;
        self.layer.borderWidth = 0.5f;
    }
}

@end
