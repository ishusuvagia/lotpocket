
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomButton : UIButton

+ (nonnull instancetype)button;

@property (nonatomic, assign) BOOL hasShadow;
@property (nonatomic, assign) BOOL hasBorder;

@property (nullable, nonatomic, strong) NSString *btnTag;

- (void)customize;

@end

NS_ASSUME_NONNULL_END
