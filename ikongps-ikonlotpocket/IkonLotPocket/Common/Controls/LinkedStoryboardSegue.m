
#import "LinkedStoryboardSegue.h"
#import "Logger.h"
#import "Utils.h"

NSString *const __nonnull kECBNavigationPresent = @"present";
NSString *const __nonnull kECBNavigationPush = @"push";
NSString *const __nonnull kECBSeparatorCharacter = @"@";

@interface LinkedStoryboardSegue ()

@property (nonatomic, strong) NSString *_segueType;

@end

@implementation LinkedStoryboardSegue

///--------------------------------------
#pragma mark - Class
///--------------------------------------

+ (UIViewController *)sceneNamed:(NSString *)identifier
{
    NSArray *info = [identifier componentsSeparatedByString:kECBSeparatorCharacter];

    NSString *storyboardName = info[1];
    NSString *sceneId = info[2];

    TRACE("Scene Id: %@", [Utils validateNotEmpty:sceneId] ? sceneId : @"Not provided, use the initial view controller");
    TRACE("Storyboard Name: %@", storyboardName);

    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *scene = nil;

    if (sceneId.length == 0) {
        scene = [storyBoard instantiateInitialViewController];
    } else {
        scene = [storyBoard instantiateViewControllerWithIdentifier:sceneId];
    }

    return scene;
}

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination
{
    NSArray *info = [identifier componentsSeparatedByString:kECBSeparatorCharacter];
    __segueType = info[0];

    return (LinkedStoryboardSegue *) [super initWithIdentifier:identifier source:source destination:[LinkedStoryboardSegue sceneNamed:identifier]];
}

- (void)perform
{
    UIViewController *source = self.sourceViewController;

    if ([self._segueType isEqualToString:kECBNavigationPresent]) {
        [source presentViewController:self.destinationViewController animated:YES completion:nil];
    } else if ([self._segueType isEqualToString:kECBNavigationPush]) {
        [source.navigationController pushViewController:self.destinationViewController animated:YES];
    } else {
        TRACE("%s: Unrecognized Segue Type: %@", __func__, self._segueType);
    }
}

- (void)dealloc
{
    TRACE_HERE;
    self._segueType = nil;
}

@end
