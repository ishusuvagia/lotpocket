//
//  GuardMainViewController.m
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "GuardMainViewController.h"

@interface GuardMainViewController ()
@property (nonatomic, strong) NSString *_currentSortType;
@property (nonatomic, strong) NSMutableDictionary *_currentFilterDict;
@end

@implementation GuardMainViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __currentSortType = kExternal_SortBy_Model;
        __currentFilterDict = [NSMutableDictionary dictionary];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"GuardLoad");
}

@end
