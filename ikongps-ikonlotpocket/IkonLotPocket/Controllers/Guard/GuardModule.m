//
//  GuardModule.m
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "GuardModule.h"
#import "GuardMainViewController.h"


@interface GuardModule ()

@property (nonatomic, strong) GuardMainViewController *_controller;

@end

@implementation GuardModule

- (NSString *)getTitle
{
    return LOCALIZED(@"title_guard");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-guard"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-guard"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-guard-selected"];
}

- (UIViewController *)getMainViewController
{
    if (!self._controller) {
        __controller = INIT_CONTROLLER_XIB(GuardMainViewController);
    }
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeGuard;
}

- (BOOL)alwaysVisible
{
    return NO;
}
@end
