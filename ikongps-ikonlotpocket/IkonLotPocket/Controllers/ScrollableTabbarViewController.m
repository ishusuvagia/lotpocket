
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "ScrollableTabbarViewController.h"
#import "ModulesManager.h"

@interface ScrollableTabbarViewController () <BarButtonsDelegate>

@property (weak, nonatomic) IBOutlet UIView *_containerView;
@property (weak, nonatomic) IBOutlet UIView *_footerView;

@property (nonatomic, strong) HMSegmentedControl *_tabbar;
@property (nonatomic, strong) NSMutableArray *_titles;
@property (nonatomic, strong) NSMutableArray *_images;
@property (nonatomic, strong) NSMutableArray *_selectedImages;

@property (nonatomic, assign) ModuleType _moduleType;
@property (nonatomic, strong) UIViewController *_currentController;

@end

@implementation ScrollableTabbarViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil moduleType:(ModuleType)moduleType
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil barButtonsDelegate:self])) {
        __moduleType = moduleType;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self _setupTabbar];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_setupTabbar
{
    __images = [NSMutableArray array];
    __titles = [NSMutableArray array];
    __selectedImages = [NSMutableArray array];
    for (id <ModuleDelegate> module in [MODULESMANAGER getAllowedModules]) {
        [self._images addObject:[UITools tintImage:[module getTabbarIcon] withColor:[AppColorScheme blue]]];
        [self._selectedImages addObject:[UITools tintImage:[module getTabbarSelectedIcon] withColor:[AppColorScheme white]]];
        [self._titles addObject:[module getTitle]];
    }
    
    if (!self._tabbar) {
        __tabbar = [[HMSegmentedControl alloc] initWithSectionImages:self._images sectionSelectedImages:self._selectedImages titlesForSections:self._titles];
        __tabbar.imagePosition = HMSegmentedControlImagePositionAboveText;
        __tabbar.frame = CGRectMake(0, 0, self.view.frame.size.width, self._footerView.frame.size.height);
        __tabbar.titleTextAttributes = @{NSForegroundColorAttributeName: [AppColorScheme blue], NSFontAttributeName: [AppFont tabbarTitleFont]};
        __tabbar.selectedTitleTextAttributes = @{NSForegroundColorAttributeName: [AppColorScheme white], NSFontAttributeName: [AppFont tabbarSelectedTitleFont]};
        __tabbar.userDraggable = YES;
        __tabbar.selectionIndicatorHeight = 3.0f;
        __tabbar.backgroundColor = [AppColorScheme white];
        __tabbar.selectionIndicatorLocation = HMSegmentedControlSelectionStyleBox;
        __tabbar.selectionStyle = HMSegmentedControlSelectionStyleBox;
        __tabbar.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
//        __tabbar.selectionIndicatorColor = [AppColorScheme purple];
        __tabbar.selectionIndicatorBoxColor=[AppColorScheme orange];
        __tabbar.selectionIndicatorBoxOpacity=1.0f;
        __tabbar.type = HMSegmentedControlSelectionStyleBox;

        
        [__tabbar addTarget:self action:@selector(_handleSegmentEvent:) forControlEvents:UIControlEventValueChanged];
        [self._footerView addSubview:__tabbar];
    }
    
    id<ModuleDelegate> module = [self _getModuleByType:self._moduleType];
    __block NSUInteger currentSegmentIdx = 0;
    [self._titles enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
        if ([title isEqualToString:[module getTitle]]) {
            currentSegmentIdx = idx;
            *stop = YES;
        }
    }];
    [self._tabbar setSelectedSegmentIndex:currentSegmentIdx];
    
    [self _setupViewController:module];
}

- (void)_setupViewController:(id<ModuleDelegate>)module
{
    if (self._currentController) {
        [self._currentController.view removeFromSuperview];
        [self._currentController removeFromParentViewController];
    }
    
    self._currentController = [module getMainViewController];
    
    self._currentController.view.frame = CGRectMake(0, 0, self._containerView.frame.size.width, self._containerView.frame.size.height);
    [self addChildViewController:self._currentController];
    [self._containerView addSubview:self._currentController.view];
    
    self.title = [[module getTitle] uppercaseString];
    
    NSString *rightbarBtnTitle = [module getNavbarButtonTitle];
    if (![Utils stringIsNullOrEmpty:rightbarBtnTitle]) {
        [self setRightBarButton:[UITools createBarButtonWithTitle:rightbarBtnTitle]];
    }
}

- (id<ModuleDelegate>)_getModuleByTitle:(NSString *)title
{
    for (id <ModuleDelegate> module in [MODULESMANAGER getAllowedModules]) {
        if ([[module getTitle] isEqualToString:title]) {
            return module;
        }
    }
    return nil;
}

- (id<ModuleDelegate>)_getModuleByType:(ModuleType)type
{
    for (id <ModuleDelegate> module in [MODULESMANAGER getAllowedModules]) {
        if ([module getModuleType] == type) {
            return module;
        }
    }
    return nil;
}

///--------------------------------------
#pragma mark - Event
///--------------------------------------

- (void)_handleSegmentEvent:(id)sender
{
    NSString *title = self._titles[self._tabbar.selectedSegmentIndex];
    id<ModuleDelegate> module = [self _getModuleByTitle:title];
    self._moduleType = [module getModuleType];
    
    [self _setupViewController:module];
}

///--------------------------------------
#pragma mark - <BarButtonsDelegate>
///--------------------------------------

- (UIButton *)setupLeftBarButton
{
    return [UITools createBarButtonWithTitle:[LOCALIZED(@"btn_home") uppercaseString]];
}

- (void)handleLeftButtonEvent:(id)sender
{
    if ([self._currentController conformsToProtocol:@protocol(ScrollableTabbarDelegate)]) {
        id <ScrollableTabbarDelegate> delegate = (id <ScrollableTabbarDelegate>) self._currentController;
        [delegate handleLeftNavigationBarButtonEvent];
    } else {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)handleRightButtonEvent:(id)sender
{
    if ([self._currentController conformsToProtocol:@protocol(ScrollableTabbarDelegate)]) {
        id <ScrollableTabbarDelegate> delegate = (id <ScrollableTabbarDelegate>) self._currentController;
        [delegate handleRightNavigationBarButtonEvent];
    }
}

@end
