
#import "VehicleLotModule.h"
#import "VehicleLotMainViewController.h"

@interface VehicleLotModule ()

@property (nonatomic, strong) VehicleLotMainViewController *_controller;

@end

@implementation VehicleLotModule

- (NSString *)getTitle
{
    return LOCALIZED(@"title_vehiclelot");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-vehiclelot"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-vehiclelot"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-vehiclelot-selected"];
}

- (UIViewController *)getMainViewController
{
    if (!self._controller) {
        __controller = INIT_CONTROLLER_XIB(VehicleLotMainViewController);
    }
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeVehicleLot;
}

- (BOOL)alwaysVisible
{
    return NO;
}

@end
