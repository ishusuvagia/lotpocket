
#import "MapMainViewController.h"

@interface MapMainViewController ()
@property (nonatomic, strong) NSString *_currentSortType;
@property (nonatomic, strong) NSMutableDictionary *_currentFilterDict;
@end

@implementation MapMainViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __currentSortType = kExternal_SortBy_Model;
        __currentFilterDict = [NSMutableDictionary dictionary];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"mapLoad");
}

@end
