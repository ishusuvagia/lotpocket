#import "MapModule.h"
#import "MapMainViewController.h"

@interface MapModule ()

@property (nonatomic, strong) MapMainViewController *_controller;

@end

@implementation MapModule

- (NSString *)getTitle
{
    return LOCALIZED(@"title_map");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-map"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-map"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-map-selected"];
}

- (UIViewController *)getMainViewController
{
    if (!self._controller) {
        __controller = INIT_CONTROLLER_XIB(MapMainViewController);
    }
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeMap;
}

- (BOOL)alwaysVisible
{
    return NO;
}

@end
