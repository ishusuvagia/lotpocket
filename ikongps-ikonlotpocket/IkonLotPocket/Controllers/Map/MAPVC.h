//
//  MAPVC.h
//  IkonLotPocket
//
//  Created by BAPS on 21/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//
#import "BaseViewController.h"
#import "NavigationBarViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface MAPVC : NavigationBarViewController<ScrollableTabbarDelegate>

@end

NS_ASSUME_NONNULL_END
