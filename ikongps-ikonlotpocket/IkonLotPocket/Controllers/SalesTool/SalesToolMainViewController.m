
#import "SalesToolMainViewController.h"

@interface SalesToolMainViewController ()
@property (nonatomic, strong) NSString *_currentSortType;
@property (nonatomic, strong) NSMutableDictionary *_currentFilterDict;
@end

@implementation SalesToolMainViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __currentSortType = kExternal_SortBy_Model;
        __currentFilterDict = [NSMutableDictionary dictionary];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"SalesLoad");
}

@end
