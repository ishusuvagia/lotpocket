
#import "SalesToolModule.h"
#import "SalesToolMainViewController.h"

@interface SalesToolModule ()

@property (nonatomic, strong) SalesToolMainViewController *_controller;

@end

@implementation SalesToolModule

- (NSString *)getTitle
{
    return LOCALIZED(@"title_salestool");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-salestool"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-salestool"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-salestool-selected"];
}

- (UIViewController *)getMainViewController
{
    if (!self._controller) {
        __controller = INIT_CONTROLLER_XIB(SalesToolMainViewController);
    }
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeSalesTool;
}

- (BOOL)alwaysVisible
{
    return NO;
}

@end
