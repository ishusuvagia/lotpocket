
#import "ChildViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryDetailViewController : ChildViewController

@property (nonatomic, strong) NSString *deviceSerialNumber;

@end

NS_ASSUME_NONNULL_END
