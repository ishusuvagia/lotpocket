
#import "NavigationBarViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol InventorySortFilterDelegate <NSObject>
@optional
- (void)updateSortString:(nullable NSString *)string filterDict:(nullable NSDictionary *)filterDict;
@end

@interface InventorySortFilterViewController : NavigationBarViewController

@property (nonatomic, strong) NSString *currentSortType;
@property (nonatomic, strong) NSMutableDictionary *currentFilterDict;
@property (nonatomic, weak) id<InventorySortFilterDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
