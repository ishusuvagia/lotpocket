
#import "ChildViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryLocateViewController : ChildViewController

@property (nonatomic, assign) NSInteger deviceId;
@property (nullable, nonatomic, strong) NSNumber *lastKnownLatitude;
@property (nullable, nonatomic, strong) NSNumber *lastKnownLongitude;
@property (nullable, nonatomic, strong) NSString *lastKnownLocation;
@property (nullable, nonatomic, strong) NSString *serialNumber;
@property (nullable, nonatomic, strong) NSString *imeiNumber;
@property (nullable, nonatomic, strong) NSString *inventoryTitle;

@end

NS_ASSUME_NONNULL_END
