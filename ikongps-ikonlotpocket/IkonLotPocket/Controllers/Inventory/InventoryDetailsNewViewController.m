//
//  InventoryDetailsNewViewController.m
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "InventoryDetailsNewViewController.h"
#import "InventoryDetailViewController.h"
#import "InventoryLocateViewController.h"
#import "DeviceService.h"
#import "SectionTableView.h"
#import "BatteryTableCell.h"
#import "InfoTableCell.h"
#import "InventorySummaryTableCell.h"
#import "IconLabelTableCell.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "ScrollableTabbarViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "LandingViewController.h"
#import "InventoryListViewController.h"
#import "EngageMainViewController.h"
#import "SalesToolMainViewController.h"
#import "VehicleLotMainViewController.h"
#import "MapMainViewController.h"
#import "AlertsViewController.h"
#import "MaintenanceViewController.h"
#import "GuardMainViewController.h"
#import "MAPVC.h"
#import "NavigationController.h"
#import "ScrollableTabbarViewController.h"
#import "AppDelegate.h"

static NSString *const kBatteryCellIdentifier = @"batterytablecell";
static NSString *const kBatteryCellNibName = @"BatteryTableCell";
static NSString *const kInfoTableCellIdentifier = @"infotablecell";
static NSString *const kInfoTableCellNibName = @"InfoTableCell";
static NSString *const kInventorySummaryCellIdentifier = @"inventorysummarytablecell";
static NSString *const kInventorySummaryCellNibName = @"InventorySummaryTableCell";
static NSString *const kIconLabelTableCellIdentifier = @"iconlabeltablecell";
static NSString *const kIconLabelTableCellNibName = @"IconLabelTableCell";

static NSString *const kCellDataKey_ConditionStatus = @"conditionstatus";
static NSString *const kCellDataKey_Latitude = @"latitude";
static NSString *const kCellDataKey_Location = @"location";
static NSString *const kCellDataKey_Longitude = @"longitude";
static NSString *const kCellDataKey_Msrp = @"msrp";
static NSString *const kCellDataKey_PhotoLinks = @"photolinks";
static NSString *const kCellDataKey_Price = @"price";
static NSString *const kCellDataKey_Title = @"title";
static NSString *const kCellDataKey_DeviceId = @"deviceid";
static NSString *const kCellDataKey_SerialNumber = @"serialnumber";
static NSString *const kCellDataKey_Imei = @"imei";

static NSString *const kCellTag_Battery = @"battery";
static NSString *const kCellTag_Location = @"location";

@interface InventoryDetailsNewViewController ()<RestResponseDelegate, SectionTableViewDelegate, UITableViewDelegate,BarButtonsDelegate>
{
    BOOL _isRequesting;
    NSMutableArray *photoLinks;
    UIImageView *imgVw;
    NSMutableArray *SideMenuTitile,*SideMenuImg;
    AppDelegate *app;
}
@property (nonatomic, strong) InventoryDetailsNewViewController *_controller;
@property (nonatomic, assign) ModuleType _moduleType;
@property (weak, nonatomic) IBOutlet SectionTableView *_tableView;

@property (nonatomic, strong) UIViewController *_parentViewController;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *segmentControl;

@property (strong, nonatomic) NSArray * items;

@end

@implementation InventoryDetailsNewViewController
@synthesize items = _items;
///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
        app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    SideMenuTitile=[[NSMutableArray alloc]initWithObjects:
                    @"INVENTORY",@"CRM",@"SALES TOOLS",
                    @"CATALOG",@"MAP",@"GUARD",
                    @"MAINTENANCE",@"ALERTS", nil];
    SideMenuImg=[[NSMutableArray alloc]initWithObjects:
                 @"icon-tabbar-inventory",@"icon-tabbar-engage",@"icon-tabbar-salestool",
                 @"icon-tabbar-vehiclelot",@"icon-tabbar-map",@"icon-tabbar-guard",
                 @"icon-tabbar-maintenance",@"icon-tabbar-alert", nil];
    
    // create effect
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    // add effect to an effect view
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = self.view.frame;
    
    // add the effect view to the image view
    [SideMenuBg addSubview:effectView];
    [self addTitle:LOCALIZED(@"title_inventory") andSubtitle:@"-"];
    [self._tableView initWithCellPrototypes:self._prototypes
                           hasHeaderSection:NO
                           hasFooterSection:NO
                              enableRefresh:NO
                                enableFetch:NO
                              emptyDataText:LOCALIZED(@"no_availabledata")
                                   delegate:self];
    [self _requestData];

    pageControl.transform = CGAffineTransformMakeScale(0.5, 0.5);
    pageControl.pageIndicatorTintColor = [AppColorScheme darkGray];
    pageControl.currentPageIndicatorTintColor=[AppColorScheme orange];
    [statsBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [StateScrollView setContentSize:CGSizeMake(self.view.frame.size.width,self.view.frame.size.height+350)];
    [TabbarScroll setContentSize:CGSizeMake(self.view.frame.size.width+200,footerView.frame.size.height)];
}
///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[@{kCellIdentifierKey: kInfoTableCellIdentifier, kCellNibNameKey: kInfoTableCellNibName, kCellClassKey: InfoTableCell.class}
             ];
}

- (void)_requestData
{
    if (_isRequesting) return;
    [self startLoading:self._tableView loadingText:LOCALIZED(@"loading_data")];
    DeviceService *service = GET_SERVICE(DeviceService);
    [service getDeviceBySerialNumber:self.deviceSerialNumber isRefreshing:self._tableView.isRefreshing responder:self];
}

///--------------------------------------
#pragma mark - <RestResponseDelegate>
///--------------------------------------

- (void)responseHandler:(id)response
{
    _isRequesting = NO;
    [self endLoading:self._tableView];
    
    DeviceEntity *deviceEntity = (DeviceEntity *) response;
    NSMutableArray *cellSectionData = [NSMutableArray array];
    
    if (deviceEntity) {
        
        InventoryEntity *inventoryEntity = deviceEntity.vehicle;
        
        ///--------------------------------------
        /// SUMMARY TITLE
        ///--------------------------------------
        
        NSString *title = inventoryEntity ? inventoryEntity.getDetailedInventoryTitle : deviceEntity.getVehicleInventoryTitle;
        [self addTitle:LOCALIZED(@"title_inventory") andSubtitle:title];
        
        ModelNameLbl.text=[NSString stringWithFormat:@"%@",deviceEntity.getVehicleInventoryTitle];
        HeaderStockNoLbl.text=[NSString stringWithFormat:@"Stock Number #: %@",deviceEntity.stockNumber];
        
        ///--------------------------------------
        /// SUMMARY SECTION
        ///--------------------------------------
        
        photoLinks = [NSMutableArray array];
        NSNumber *price, *msrp;
        NSString *conditionStatus = kEmptyString;
        
        if (inventoryEntity) {
            for (NSString *photoLink in inventoryEntity.getPhotoLinkUrls) {
                [photoLinks addObject:photoLink];
            }
              [self paginationImg];
            price = @(inventoryEntity.price);
            msrp = @(inventoryEntity.msrp);
            conditionStatus = inventoryEntity.getConditionStatus;
        } else {
            NSString *photoLink = [NSString stringWithFormat:@"%@%@", kSkyPatrolAvatarImageRepoUrl, deviceEntity.avatar];
            [photoLinks addObject:photoLink];
            price = @(0);
            msrp = @(0);
        }
        ///--------------------------------------
        /// INFO SECTION
        ///--------------------------------------
        
        NSMutableArray *infoData = [NSMutableArray array];
        
        /// MODEL
        NSString *model = inventoryEntity ? inventoryEntity.heading : deviceEntity.getVehicleInventoryTitle;
        if (!model) model = @"-";
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"model")],
                              kCellInformationKey: model,
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// VIN
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"vinnumber")],
                              kCellInformationKey: deviceEntity.vin ? [deviceEntity.vin uppercaseString] : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// CONDITION & MILES
        NSString *condition = inventoryEntity ? inventoryEntity.getConditionStatus : nil;
        NSString *mileage = inventoryEntity ? [Utils getNumberString:inventoryEntity.miles] : nil;
        NSString *conditionMilage = condition && mileage ? [NSString stringWithFormat:@"%@ / %@ %@", LOCALIZED(condition), mileage, LOCALIZED(@"miles")] : @"-";
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@ / %@:", LOCALIZED(@"condition"), LOCALIZED(@"mileage")],
                              kCellInformationKey: conditionMilage,
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// SELLER TYPE
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"sellertype")],
                              kCellInformationKey: inventoryEntity.sellerType ? [Utils capitalizedOnlyFirstLetter:inventoryEntity.sellerType] : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// DAYS ON MARKET
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"daysonmarket")],
                              kCellInformationKey: inventoryEntity ? [NSString stringWithFormat:@"%@ %@", [@(inventoryEntity.daysOnMarket) stringValue], LOCALIZED(@"days")] : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// BODY & DOORS
        NSString *body = (inventoryEntity && inventoryEntity.bodyType) ? inventoryEntity.bodyType : nil;
        NSString *doors = inventoryEntity ? [NSString stringWithFormat:@"%ld %@", (long) inventoryEntity.doors, LOCALIZED(@"doors")] : nil;
        NSString *bodyDoors = (body && doors) ? [NSString stringWithFormat:@"%@ %@", body, doors] : @"-";
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"bodystyle")],
                              kCellInformationKey: bodyDoors,
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// HIGHWAY MILES
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"highwaymiles")],
                              kCellInformationKey: (inventoryEntity && inventoryEntity.highwayMiles) ? inventoryEntity.highwayMiles : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// CITY MILES
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"citymiles")],
                              kCellInformationKey: (inventoryEntity && inventoryEntity.cityMiles) ? inventoryEntity.cityMiles : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// EXTERIOR COLOR
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"exterior")],
                              kCellInformationKey: inventoryEntity ? (inventoryEntity.exteriorColor ? [Utils capitalizedOnlyFirstLetter:inventoryEntity.exteriorColor] : @"-") : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// INTERIOR COLOR
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"interior")],
                              kCellInformationKey: inventoryEntity ? (inventoryEntity.interiorColor ? [Utils capitalizedOnlyFirstLetter:inventoryEntity.interiorColor] : @"-") : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// ENGINE & TRANSMISSION
        NSString *engine = (inventoryEntity && inventoryEntity.engine) ? inventoryEntity.engine : nil;
        NSString *transmission = (inventoryEntity && inventoryEntity.transmission) ? inventoryEntity.transmission : nil;
        NSString *engineTransmission = (engine && transmission) ? [NSString stringWithFormat:@"%@ / %@", engine, transmission] : @"-";
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@ / %@", LOCALIZED(@"engine"), LOCALIZED(@"transmission")],
                              kCellInformationKey: engineTransmission,
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        /// FUEL TYPE
        [infoData addObject:@{
                              kCellIdentifierKey: kInfoTableCellIdentifier,
                              kCellClassKey: InfoTableCell.class,
                              kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"fueltype")],
                              kCellInformationKey: (inventoryEntity && inventoryEntity.fuelType) ? inventoryEntity.fuelType : @"-",
                              kCellHeightKey: @([InfoTableCell height])
                              }];
        
        NSString *stockNumber = inventoryEntity ? inventoryEntity.stockNumber : deviceEntity.stockNumber;
        NSDictionary *infoSection = @{
                                      kSectionTitleKey: [NSString stringWithFormat:@"%@ %@", LOCALIZED(@"stocknumber"), stockNumber ?: @"-"],
                                      kSectionDataKey: infoData
                                      };
        [cellSectionData addObject:infoSection];
        
/*       ///--------------------------------------
        /// GPS DEVICE
        ///--------------------------------------

        NSMutableArray *gpsData = [NSMutableArray array];

        /// BATTERY LEVEL
        [gpsData addObject:@{
                             kCellIdentifierKey: kIconLabelTableCellIdentifier,
                             kCellClassKey: IconLabelTableCell.class,
                             kCellInformationKey: deviceEntity.batteryLevel ?: kEmptyString,
                             kCellHeightKey: @([IconLabelTableCell height]),
                             kCellTagKey: kCellTag_Battery,
                             kCellShowDisclosureIndicatorKey: @(NO)
                             }];

        /// LOCATION
        NSNumber *latitude = deviceEntity.latitude ?: @(0);
        NSNumber *longitude = deviceEntity.longitude ?: @(0);
        NSDictionary *locationData = @{
                                       kCellDataKey_DeviceId: @(deviceEntity.deviceId),
                                       kCellDataKey_SerialNumber: deviceEntity.serialNumber ?: kEmptyString,
                                       kCellDataKey_Imei: deviceEntity.imei ?: kEmptyString,
                                       kCellDataKey_Title: title,
                                       kCellDataKey_Location: deviceEntity.location ?: @"-",
                                       kCellDataKey_Latitude: latitude,
                                       kCellDataKey_Longitude: longitude
                                       };
        [gpsData addObject:@{
                             kCellIdentifierKey: kIconLabelTableCellIdentifier,
                             kCellClassKey: IconLabelTableCell.class,
                             kCellInformationKey: LOCALIZED(@"location"),
                             kCellObjectDataKey: locationData,
                             kCellIconImageKey: [UIImage imageNamed:@"icon-location"],
                             kCellColorCodeKey: [AppColorScheme brandBlack],
                             kCellHeightKey: @([IconLabelTableCell height]),
                             kCellTagKey: kCellTag_Location,
                             kCellShowDisclosureIndicatorKey: @(YES)
                             }];


        NSDictionary *gpsSection = @{
                                     kSectionTitleKey: LOCALIZED(@"gpsdevice"),
                                     kSectionDataKey: gpsData
                                     };
        [cellSectionData addObject:gpsSection];

        ///--------------------------------------
        /// FINANCIAL OPTIONS SECTION
        ///--------------------------------------

        if (inventoryEntity) {

            for (NSUInteger idx = 0; idx < inventoryEntity.financingOptions.count; idx++) {
                FinancingOptionEntity *financingOptionEntity = [inventoryEntity.financingOptions objectAtIndex:idx];
                NSMutableArray *financingOptionData = [NSMutableArray array];

                /// LOAN TERM
                [financingOptionData addObject:@{
                                                 kCellIdentifierKey: kInfoTableCellIdentifier,
                                                 kCellClassKey: InfoTableCell.class,
                                                 kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"loanterm")],
                                                 kCellInformationKey: [NSString stringWithFormat:@"%.ld %@", financingOptionEntity.loanTerm, LOCALIZED(@"months")],
                                                 kCellHeightKey: @([InfoTableCell height])
                                                 }];

                /// LOAN APR
                [financingOptionData addObject:@{
                                                 kCellIdentifierKey: kInfoTableCellIdentifier,
                                                 kCellClassKey: InfoTableCell.class,
                                                 kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"loanapr")],
                                                 kCellInformationKey: [NSString stringWithFormat:@"%.0f %%", financingOptionEntity.loanApr],
                                                 kCellHeightKey: @([InfoTableCell height])
                                                 }];

                /// DOWNPAYMENT PERCETANGE
                [financingOptionData addObject:@{
                                                 kCellIdentifierKey: kInfoTableCellIdentifier,
                                                 kCellClassKey: InfoTableCell.class,
                                                 kCellTitleKey: [NSString stringWithFormat:@"%@:", LOCALIZED(@"downpaymentpercentage")],
                                                 kCellInformationKey: [NSString stringWithFormat:@"%.0f %%", financingOptionEntity.downPaymentPercentage],
                                                 kCellHeightKey: @([InfoTableCell height])
                                                 }];

                NSDictionary *financingOptionSection;
                if (inventoryEntity.financingOptions.count == 1) {
                    financingOptionSection = @{
                                               kSectionTitleKey: LOCALIZED(@"financingoption"),
                                               kSectionDataKey: financingOptionData
                                               };
                } else {
                    financingOptionSection = @{
                                               kSectionTitleKey: [NSString stringWithFormat:@"%@: %ld", LOCALIZED(@"financingoption"), (long) idx + 1],
                                               kSectionDataKey: financingOptionData
                                               };
                }
                [cellSectionData addObject:financingOptionSection];
            }
        }

        ///--------------------------------------*/
    }
    
    [self._tableView loadData:cellSectionData];
  
    
}

- (void)errorHandler:(NSError *)error
{
    _isRequesting = NO;
    [self endLoading:self._tableView error:error];
}


///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------

- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
/*   if ([cell isKindOfClass:InventorySummaryTableCell.class]) {
        NSDictionary *dict = cellDict[kCellObjectDataKey];
        [(InventorySummaryTableCell *) cell setupCellWithImages:dict[kCellDataKey_PhotoLinks]
                                                          title:dict[kCellDataKey_Title]
                                                          price:[dict[kCellDataKey_Price] integerValue]
                                                      msrpPrice:[dict[kCellDataKey_Msrp] integerValue]
                                                      condition:dict[kCellDataKey_ConditionStatus]];
    } else if ([cell isKindOfClass:InfoTableCell.class]) {*/
        NSString *title = cellDict[kCellTitleKey];
        NSString *value = cellDict[kCellInformationKey];
        UIColor *colorCode = cellDict[kCellColorCodeKey];
        [(InfoTableCell *) cell setupCellWithTitle:title information:value customColor:colorCode showDisclosure:NO];
/*    } else if ([cell isKindOfClass:IconLabelTableCell.class]) {
        NSString *value = cellDict[kCellInformationKey];
        NSString *key = cellDict[kCellTagKey];
        BOOL showDisclosure = [cellDict[kCellShowDisclosureIndicatorKey] boolValue];
        if ([key isEqualToString:kCellTag_Battery]) {
            CGFloat level = [BusinessUtils getBatteryLevel:value];
            NSString *title = [[BusinessUtils getBatteryLevelString:level] uppercaseString];
            UIImage *iconImage = [BusinessUtils getBatteryIconImage:level];
            UIColor *colorCode = [BusinessUtils getBatteryLevelColor:level];
            [(IconLabelTableCell *) cell setupCellWithTitle:title iconImage:iconImage colorCode:colorCode key:key showDisclosure:showDisclosure];
        } else {
            UIImage *iconImage = cellDict[kCellIconImageKey];
            UIColor *colorCode = cellDict[kCellColorCodeKey];
            [(IconLabelTableCell *) cell setupCellWithTitle:value iconImage:iconImage colorCode:colorCode key:key showDisclosure:showDisclosure];
        }
    }*/
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle
       withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:IconLabelTableCell.class]) {
        BOOL showDisclosure = [cellDict[kCellShowDisclosureIndicatorKey] boolValue];
        NSString *tag = cellDict[kCellTagKey];
        if (showDisclosure && [tag isEqualToString:kCellTag_Location]) {
            NSDictionary *dict = cellDict[kCellObjectDataKey];
            InventoryLocateViewController *controller = INIT_CONTROLLER_XIB(InventoryLocateViewController);
            controller.deviceId = [dict[kCellDataKey_DeviceId] integerValue];
            controller.lastKnownLatitude = dict[kCellDataKey_Latitude];
            controller.lastKnownLongitude = dict[kCellDataKey_Longitude];
            controller.lastKnownLocation = dict[kCellDataKey_Location];
            controller.serialNumber = dict[kCellDataKey_SerialNumber];
            controller.imeiNumber = dict[kCellDataKey_Imei];
            controller.inventoryTitle = dict[kCellDataKey_Title];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (void)refreshData
{
    [self _requestData];
}

///--------------------------------------
#pragma mark - <Segment Controll Method>
///--------------------------------------

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld", (long)segmentedControl.selectedSegmentIndex);
}


-(void)paginationImg
{
    
    ImgPagination.delegate=self;
    //set scrollview content size
    [ImgPagination setContentSize:CGSizeMake(ImgPagination.frame.size.width*[photoLinks count],ImgPagination.frame.size.height)];
    
    ImgPagination.pagingEnabled =YES;
    
    pageControl.numberOfPages= [photoLinks count];
    
    
    
    //crate imageview to display images
    for (int i =0 ;i <[photoLinks count];i++)
    {
        NSLog(@"i = %d",i);
        imgVw=[[UIImageView alloc] initWithFrame:CGRectMake(i*ImgPagination.frame.size.width,ImgPagination.frame.origin.y, ImgPagination.frame.size.width, ImgPagination.frame.size.height)];
        NSString *phtLinks=[photoLinks objectAtIndex:i];
        
       /* [imgVw sd_setImageWithURL:[NSURL URLWithString:phtLinks]
                                 placeholderImage:[UIImage imageNamed:@"img-inventory-placeholder"]
                                          options:SDWebImageRetryFailed|SDWebImageRefreshCached|SDWebImageContinueInBackground
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];*/

        
                [imgVw sd_setImageWithURL:[NSURL URLWithString:phtLinks]
                         placeholderImage:[UIImage imageNamed:@"img-inventory-placeholder"]
                                  options:SDWebImageRetryFailed|SDWebImageRefreshCached|SDWebImageContinueInBackground
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
        
        //set image content mode
        imgVw.contentMode = UIViewContentModeScaleToFill;
        
        //clips to bounds
        imgVw.clipsToBounds=YES;
        [ImgPagination addSubview:imgVw];
    }
}
-(IBAction)clickPageControl:(id)sender
{
    int page=pageControl.currentPage;
    CGRect frame=ImgPagination.frame;
    frame.origin.x=frame.size.width=page;
    frame.origin.y=0;
    [ImgPagination scrollRectToVisible:frame animated:YES];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
       /* int page = scrollView.contentOffset.x/scrollView.frame.size.width;
        pageControl.currentPage=page;*/
    pageControlBeingUsed = NO;
}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = ImgPagination.frame.size.width;
        int page = floor((ImgPagination.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControl.currentPage = page;
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

-(IBAction)backBrnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)statsBtnPressed:(id)sender
{
    [statsBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [featuresBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [locationBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
}
-(IBAction)featureBtnPressed:(id)sender
{
    [featuresBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
    [statsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [locationBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
}
-(IBAction)locationBtnPressed:(id)sender
{
    [locationBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
    [featuresBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [statsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
}


///--------------------------------------
#pragma mark - <side menu>
///--------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [SideMenuTitile count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideMenuTableViewCell * cell = [_menuTable dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"SideMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [_menuTable dequeueReusableCellWithIdentifier:@"myCell"];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(SideMenuTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell._menutitleLbl.text = [SideMenuTitile objectAtIndex:indexPath.row];
    
    NSString *ImgName=[NSString stringWithFormat:@"%@",[SideMenuImg objectAtIndex:indexPath.row]];
    cell._menuImg.image=[UIImage imageNamed:ImgName];
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Title=[NSString stringWithFormat:@"%@", [SideMenuTitile objectAtIndex:indexPath.row]];
    if ([Title isEqualToString:@"INVENTORY"])
    {
        ModuleType type = ModuleTypeInventory;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else if ([Title isEqualToString:@"CRM"])
    {
        ModuleType type = ModuleTypeEngage;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else if ([Title isEqualToString:@"SALES TOOLS"])
    {
        ModuleType type = ModuleTypeSalesTool;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else if ([Title isEqualToString:@"VEHICLE INFO"])
    {
        ModuleType type = ModuleTypeVehicleLot;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else if ([Title isEqualToString:@"MAP"])
    {
        ModuleType type = ModuleTypeMap;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else if ([Title isEqualToString:@"GUARD"])
    {
        ModuleType type = ModuleTypeGuard;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else if ([Title isEqualToString:@"MAINTENANCE"])
    {
        ModuleType type = ModuleTypeMaintenance;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else
    {
        ModuleType type = ModuleTypeAlerts;
        ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                                  moduleType:type];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}
-(IBAction)MenuBtnPressed:(id)sender
{
    if (menuBtn.isSelected ==  YES)
    {
        [menuBtn setSelected:NO];
        SideMenuView.hidden=YES;
        [self animationhide];

    }
    else
    {
        [menuBtn setSelected:YES];
            SideMenuView.hidden=NO;
            [self animationView];
    }
}
-(void)animationView
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         self->SideMenuView.frame =CGRectMake(self.view.frame.size.width- 200,self->HeaderView.frame.size.height+1,self->SideMenuView.frame.size.width,self.view.frame.size.height-self->HeaderView.frame.size.height);
                         self->SideMenuView.alpha=1;
                     }];
}
-(void)animationhide
{
    [UIView animateWithDuration:0.1
                     animations:^{
                         self->SideMenuView.frame =CGRectMake(self.view.frame.size.width +200,self->HeaderView.frame.size.height+1,self->SideMenuView.frame.size.width,self.view.frame.size.height-self->HeaderView.frame.size.height);
                         self->SideMenuView.alpha=0.0;
                     }];
}
-(IBAction)BrochuresBtnPressed:(id)sender
{
    [brochureBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [carfaxBtn setBackgroundColor:[UIColor whiteColor]];
    [salesBtn setBackgroundColor:[UIColor whiteColor]];
    [leadsBtn setBackgroundColor:[UIColor whiteColor]];
    [engageBtn setBackgroundColor:[UIColor whiteColor]];
    [appraisalBtn setBackgroundColor:[UIColor whiteColor]];
    [safetyBtn setBackgroundColor:[UIColor whiteColor]];
    [brochureBtn setSelected:YES];
    [carfaxBtn setSelected:NO];
    [salesBtn setSelected:NO];
    [leadsBtn setSelected:NO];
    [engageBtn setSelected:NO];
    [appraisalBtn setSelected:NO];
    [safetyBtn setSelected:NO];
    brochureImg.image=[UIImage imageNamed:@"Inv-Details-brousher-sel"];
    carfaxImg.image=[UIImage imageNamed:@"Inv-Details-carfax-desel"];
    salesImg.image=[UIImage imageNamed:@"Inv-Details-sales-desel"];
    leadsImg.image=[UIImage imageNamed:@"Inv-Details-lead-desel"];
    engageImg.image=[UIImage imageNamed:@"Inv-Details-engage-desel"];
    appraisalImg.image=[UIImage imageNamed:@"Inv-Details-apprial-desel"];
    safetyImg.image=[UIImage imageNamed:@"Inv-Details-safty-desel"];
}
-(IBAction)CarFaxBtnPressed:(id)sender
{
    [carfaxBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [brochureBtn setBackgroundColor:[UIColor whiteColor]];
    [salesBtn setBackgroundColor:[UIColor whiteColor]];
    [leadsBtn setBackgroundColor:[UIColor whiteColor]];
    [engageBtn setBackgroundColor:[UIColor whiteColor]];
    [appraisalBtn setBackgroundColor:[UIColor whiteColor]];
    [safetyBtn setBackgroundColor:[UIColor whiteColor]];
    brochureImg.image=[UIImage imageNamed:@"Inv-Details-brousher-desel"];
    carfaxImg.image=[UIImage imageNamed:@"Inv-Details-carfax-sel"];
    salesImg.image=[UIImage imageNamed:@"Inv-Details-sales-desel"];
    leadsImg.image=[UIImage imageNamed:@"Inv-Details-lead-desel"];
    engageImg.image=[UIImage imageNamed:@"Inv-Details-engage-desel"];
    appraisalImg.image=[UIImage imageNamed:@"Inv-Details-apprial-desel"];
    safetyImg.image=[UIImage imageNamed:@"Inv-Details-safty-desel"];
    [brochureBtn setSelected:NO];
    [carfaxBtn setSelected:YES];
    [salesBtn setSelected:NO];
    [leadsBtn setSelected:NO];
    [engageBtn setSelected:NO];
    [appraisalBtn setSelected:NO];
    [safetyBtn setSelected:NO];
}
-(IBAction)SalesBtnPressed:(id)sender
{
    [salesBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [brochureBtn setBackgroundColor:[UIColor whiteColor]];
    [carfaxBtn setBackgroundColor:[UIColor whiteColor]];
    [leadsBtn setBackgroundColor:[UIColor whiteColor]];
    [engageBtn setBackgroundColor:[UIColor whiteColor]];
    [appraisalBtn setBackgroundColor:[UIColor whiteColor]];
    [safetyBtn setBackgroundColor:[UIColor whiteColor]];
    brochureImg.image=[UIImage imageNamed:@"Inv-Details-brousher-desel"];
    carfaxImg.image=[UIImage imageNamed:@"Inv-Details-carfax-desel"];
    salesImg.image=[UIImage imageNamed:@"Inv-Details-sales-sel"];
    leadsImg.image=[UIImage imageNamed:@"Inv-Details-lead-desel"];
    engageImg.image=[UIImage imageNamed:@"Inv-Details-engage-desel"];
    appraisalImg.image=[UIImage imageNamed:@"Inv-Details-apprial-desel"];
    safetyImg.image=[UIImage imageNamed:@"Inv-Details-safty-desel"];
    [brochureBtn setSelected:NO];
    [carfaxBtn setSelected:NO];
    [salesBtn setSelected:YES];
    [leadsBtn setSelected:NO];
    [engageBtn setSelected:NO];
    [appraisalBtn setSelected:NO];
    [safetyBtn setSelected:NO];
}
-(IBAction)LeadBtnPressed:(id)sender
{
    [leadsBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [brochureBtn setBackgroundColor:[UIColor whiteColor]];
    [carfaxBtn setBackgroundColor:[UIColor whiteColor]];
    [salesBtn setBackgroundColor:[UIColor whiteColor]];
    [engageBtn setBackgroundColor:[UIColor whiteColor]];
    [appraisalBtn setBackgroundColor:[UIColor whiteColor]];
    [safetyBtn setBackgroundColor:[UIColor whiteColor]];
    brochureImg.image=[UIImage imageNamed:@"Inv-Details-brousher-desel"];
    carfaxImg.image=[UIImage imageNamed:@"Inv-Details-carfax-desel"];
    salesImg.image=[UIImage imageNamed:@"Inv-Details-sales-desel"];
    leadsImg.image=[UIImage imageNamed:@"Inv-Details-lead-sel"];
    engageImg.image=[UIImage imageNamed:@"Inv-Details-engage-desel"];
    appraisalImg.image=[UIImage imageNamed:@"Inv-Details-apprial-desel"];
    safetyImg.image=[UIImage imageNamed:@"Inv-Details-safty-desel"];
    [brochureBtn setSelected:NO];
    [carfaxBtn setSelected:NO];
    [salesBtn setSelected:NO];
    [leadsBtn setSelected:YES];
    [engageBtn setSelected:NO];
    [appraisalBtn setSelected:NO];
    [safetyBtn setSelected:NO];
}
-(IBAction)EngageBtnPressed:(id)sender
{
    [engageBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [brochureBtn setBackgroundColor:[UIColor whiteColor]];
    [carfaxBtn setBackgroundColor:[UIColor whiteColor]];
    [salesBtn setBackgroundColor:[UIColor whiteColor]];
    [leadsBtn setBackgroundColor:[UIColor whiteColor]];
    [appraisalBtn setBackgroundColor:[UIColor whiteColor]];
    [safetyBtn setBackgroundColor:[UIColor whiteColor]];
    brochureImg.image=[UIImage imageNamed:@"Inv-Details-brousher-desel"];
    carfaxImg.image=[UIImage imageNamed:@"Inv-Details-carfax-desel"];
    salesImg.image=[UIImage imageNamed:@"Inv-Details-sales-desel"];
    leadsImg.image=[UIImage imageNamed:@"Inv-Details-lead-desel"];
    engageImg.image=[UIImage imageNamed:@"Inv-Details-engage-sel"];
    appraisalImg.image=[UIImage imageNamed:@"Inv-Details-apprial-desel"];
    safetyImg.image=[UIImage imageNamed:@"Inv-Details-safty-desel"];
    [brochureBtn setSelected:NO];
    [carfaxBtn setSelected:NO];
    [salesBtn setSelected:NO];
    [leadsBtn setSelected:NO];
    [engageBtn setSelected:YES];
    [appraisalBtn setSelected:NO];
    [safetyBtn setSelected:NO];
    
    app.ChatStatusStr=@"on";
    
    ModuleType type = ModuleTypeEngage;
    ScrollableTabbarViewController *controller = [[ScrollableTabbarViewController alloc] initWithNibName:@"ScrollableTabbarViewController" bundle:nil
                                                                                              moduleType:type];
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:controller];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
    
}
-(IBAction)ApprasialBtnPressed:(id)sender
{
    [appraisalBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [brochureBtn setBackgroundColor:[UIColor whiteColor]];
    [carfaxBtn setBackgroundColor:[UIColor whiteColor]];
    [salesBtn setBackgroundColor:[UIColor whiteColor]];
    [leadsBtn setBackgroundColor:[UIColor whiteColor]];
    [engageBtn setBackgroundColor:[UIColor whiteColor]];
    [safetyBtn setBackgroundColor:[UIColor whiteColor]];
    brochureImg.image=[UIImage imageNamed:@"Inv-Details-brousher-desel"];
    carfaxImg.image=[UIImage imageNamed:@"Inv-Details-carfax-desel"];
    salesImg.image=[UIImage imageNamed:@"Inv-Details-sales-desel"];
    leadsImg.image=[UIImage imageNamed:@"Inv-Details-lead-desel"];
    engageImg.image=[UIImage imageNamed:@"Inv-Details-engage-desel"];
    appraisalImg.image=[UIImage imageNamed:@"Inv-Details-apprial-sel"];
    safetyImg.image=[UIImage imageNamed:@"Inv-Details-safty-desel"];
    [brochureBtn setSelected:NO];
    [carfaxBtn setSelected:NO];
    [salesBtn setSelected:NO];
    [leadsBtn setSelected:NO];
    [engageBtn setSelected:NO];
    [appraisalBtn setSelected:YES];
    [safetyBtn setSelected:NO];
}
-(IBAction)SafetyBtnPressed:(id)sender
{
    [safetyBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];//orange
    [brochureBtn setBackgroundColor:[UIColor whiteColor]];
    [carfaxBtn setBackgroundColor:[UIColor whiteColor]];
    [salesBtn setBackgroundColor:[UIColor whiteColor]];
    [leadsBtn setBackgroundColor:[UIColor whiteColor]];
    [engageBtn setBackgroundColor:[UIColor whiteColor]];
    [appraisalBtn setBackgroundColor:[UIColor whiteColor]];
    brochureImg.image=[UIImage imageNamed:@"Inv-Details-brousher-desel"];
    carfaxImg.image=[UIImage imageNamed:@"Inv-Details-carfax-desel"];
    salesImg.image=[UIImage imageNamed:@"Inv-Details-sales-desel"];
    leadsImg.image=[UIImage imageNamed:@"Inv-Details-lead-desel"];
    engageImg.image=[UIImage imageNamed:@"Inv-Details-engage-desel"];
    appraisalImg.image=[UIImage imageNamed:@"Inv-Details-apprial-desel"];
    safetyImg.image=[UIImage imageNamed:@"Inv-Details-safty-sel"];
    [brochureBtn setSelected:NO];
    [carfaxBtn setSelected:NO];
    [salesBtn setSelected:NO];
    [leadsBtn setSelected:NO];
    [engageBtn setSelected:NO];
    [appraisalBtn setSelected:NO];
    [safetyBtn setSelected:YES];
}
@end
