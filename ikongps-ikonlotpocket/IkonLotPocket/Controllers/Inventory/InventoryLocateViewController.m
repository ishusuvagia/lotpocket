

#import <Mapbox/Mapbox.h>
#import "CustomActivityButton.h"
#import "MapboxPointAnnotation.h"
#import "MapBoxCalloutView.h"
#import "InventoryLocateViewController.h"
#import "DataService.h"
#import "DeviceEntity.h"
#import "DeviceService.h"

static NSString *const kAnnotationIdentifier = @"annotation_dotview";
static CGFloat const kRequestSecondsLimit = 180.0f;
static CGFloat const kMapHeight = 450.0f;
static CGFloat const kHeaderSpacing = 20.0f;
static CGFloat const kBodySpacing = 5.0f;

@interface InventoryLocateViewController () <MGLMapViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *_scrollView;
@property (nonatomic, strong) MGLMapView *_mapView;
@property (nonatomic, strong) UITextView *_textView;

@property (nonatomic, strong) DeviceEntity *_currentDevice;

@property (nonatomic, assign) NSInteger _currentTransactionId;
@property (nonatomic, assign) NSTimeInterval _requestedTimeStamp;
@property (nonatomic, strong) MapboxCalloutView *_calloutView;

@end

@implementation InventoryLocateViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __currentTransactionId = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addTitle:LOCALIZED(@"title_locate") andSubtitle:self.inventoryTitle];
    [self _setupMapView];
    [self _setupTextView];
    [self _zoomToCoordinate];
    [self _requestData];
}

///--------------------------------------
#pragma mark - Map
///--------------------------------------

- (void)_setupMapView
{
    __mapView = [[MGLMapView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, kMapHeight)];
    self._mapView.styleURL = [MGLStyle satelliteStyleURL];
    self._mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self._mapView.delegate = self;
    self._mapView.showsScale = YES;
    self._mapView.logoView.hidden = YES;
    self._mapView.attributionButton.hidden = YES;

    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img-logo-white"]];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoImageView.clipsToBounds = YES;
    logoImageView.frame = CGRectMake(0.0f, 0.0f, self._mapView.logoView.frame.size.width, self._mapView.frame.size.height);
    logoImageView.center = CGPointMake(58.5f, self._mapView.frame.size.height - 24.0f);
    [self._mapView addSubview:logoImageView];

    __calloutView = [[MapboxCalloutView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, MapboxCalloutView.width, MapboxCalloutView.height)];

    [self._scrollView addSubview:self._mapView];
}

- (void)_loadDataForMapView
{
    InventoryEntity *vehicleEntity = self._currentDevice.vehicle;
    NSString *title, *inventoryCode, *vin, *imageUrl = nil;
    NSString *batteryLevel = self._currentDevice.batteryLevel;

    if (vehicleEntity.inventoryId != nil) {
        title = vehicleEntity.getInventoryTitle;
        inventoryCode = vehicleEntity.stockNumber;
        vin = vehicleEntity.vin;
        if (vehicleEntity.photoLinks.count > 0) {
            imageUrl = [[vehicleEntity.photoLinks objectAtIndex:0] stringValue];
        }
    } else {
        title = self._currentDevice.getVehicleInventoryTitle;
        vin = self._currentDevice.vin;
        inventoryCode = self._currentDevice.stockNumber;
        imageUrl = [NSString stringWithFormat:@"%@%@", kSkyPatrolAvatarImageRepoUrl, self._currentDevice.avatar];
    }
    [self._calloutView setupViewWithTitle:title inventoryCode:inventoryCode vin:vin batteryLevel:batteryLevel imageUrl:imageUrl];
}

- (void)_changeMapViewStyleUrl:(NSURL *)styleUrl
{
    self._mapView.styleURL = styleUrl;
    [self._mapView reloadStyle:styleUrl];
}

- (void)_zoomToCoordinate
{
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([self.lastKnownLatitude floatValue], [self.lastKnownLongitude floatValue]);
    MapboxPointAnnotation *annotation = [MapboxPointAnnotation new];
    annotation.title = self.inventoryTitle;
    annotation.willUseImage = YES;
    annotation.coordinate = coordinate;
    [self._mapView addAnnotation:annotation];
    [self._mapView setCenterCoordinate:coordinate zoomLevel:kDefaultMapZoomNear animated:YES];
}

- (void)_showActivity
{
    [self setRightBarButton:[UITools createActivityBarButton]];
}

- (void)_hideActivity
{
    [self setRightBarButton:[UITools createBarButtonWithTitle:[LOCALIZED(@"btn_locate") uppercaseString]]];
}

///--------------------------------------
#pragma mark - TextView
///--------------------------------------

- (void)_setupTextView
{
    __textView = [[UITextView alloc] initWithFrame:CGRectMake(10.0f, kMapHeight+20.0f, self.view.frame.size.width-20.0f, self.view.frame.size.height - kMapHeight) textContainer:nil];
    self._textView.selectable = NO;
    self._textView.editable = NO;
    self._textView.scrollEnabled = NO;
    self._textView.showsHorizontalScrollIndicator = NO;
    self._textView.showsVerticalScrollIndicator = NO;
    self._textView.backgroundColor = [UIColor clearColor];

    [self._scrollView addSubview:self._textView];
}

- (void)_loadDataForTextView
{
    if (self._currentDevice.vehicle) {
        
        InventoryEntity *vehicle = self._currentDevice.vehicle;
        NSMutableAttributedString *textAttrString = [NSMutableAttributedString new];
        
        /// Standard Features
        if (vehicle.standardFeatures.count > 0) {
            [textAttrString appendAttributedString:[self _headerAttributedString:LOCALIZED(@"standardfeatures")]];
            for (RealmString *realmString in vehicle.standardFeatures) {
                NSAttributedString *bodyAttrString = [self _bodyAttributedString:realmString.stringValue];
                [textAttrString appendAttributedString:bodyAttrString];
            }
        }
        
        /// Exterior Features
        if (vehicle.exteriorFeatures.count > 0) {
            [textAttrString appendAttributedString:[self _headerAttributedString:LOCALIZED(@"exteriorfeatures")]];
            for (RealmString *realmString in vehicle.exteriorFeatures) {
                NSAttributedString *bodyAttrString = [self _bodyAttributedString:realmString.stringValue];
                [textAttrString appendAttributedString:bodyAttrString];
            }
        }
        
        /// Interior Features
        if (vehicle.interiorFeatures.count > 0) {
            [textAttrString appendAttributedString:[self _headerAttributedString:LOCALIZED(@"interiorfeatures")]];
            for (RealmString *realmString in vehicle.interiorFeatures) {
                NSAttributedString *bodyAttrString = [self _bodyAttributedString:realmString.stringValue];
                [textAttrString appendAttributedString:bodyAttrString];
            }
        }
        
        /// Safety Features
        if (vehicle.interiorFeatures.count > 0) {
            [textAttrString appendAttributedString:[self _headerAttributedString:LOCALIZED(@"safetyfeatures")]];
            for (RealmString *realmString in vehicle.safetyFeatures) {
                NSAttributedString *bodyAttrString = [self _bodyAttributedString:realmString.stringValue];
                [textAttrString appendAttributedString:bodyAttrString];
            }
        }
        
        self._textView.attributedText = textAttrString;
        [self _updateTextViewHeight];
    }
}

- (NSAttributedString *)_headerAttributedString:(NSString *)text
{
    NSString *formattedText = [NSString stringWithFormat:@"%@\n", text];

    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.paragraphSpacingBefore = kHeaderSpacing;
    style.alignment = NSTextAlignmentLeft;
    UIFont *font = [AppFont navigationBarTitleFont];
    UIColor *textColor = [AppColor headerTitleColor];

    NSDictionary *attributes = @{
            NSParagraphStyleAttributeName: style,
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: textColor
    };

    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:formattedText attributes:attributes];

    return attributedString;
}

- (NSAttributedString *)_bodyAttributedString:(NSString *)text
{
    NSString *formattedText = [NSString stringWithFormat:@"\u2022 %@\n", text];

    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.lineSpacing = kBodySpacing;
    style.paragraphSpacingBefore = kBodySpacing;
    style.paragraphSpacing = kBodySpacing;
    style.alignment = NSTextAlignmentLeft;
    UIFont *font = [AppFont textViewFont];
    UIColor *textColor = [AppColor textViewTextColor];

    NSDictionary *attributes = @{
            NSParagraphStyleAttributeName: style,
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: textColor
    };

    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:formattedText attributes:attributes];

    return attributedString;
}

- (void)_updateTextViewHeight
{
    CGFloat fixedWidth = self._textView.frame.size.width;
    CGSize newSize = [self._textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = self._textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    self._textView.frame = newFrame;
    self._scrollView.contentSize = CGSizeMake(self._scrollView.frame.size.width, self._mapView.frame.size.height+newFrame.size.height+20.0f);
}

///--------------------------------------
#pragma mark - Data Request
///--------------------------------------

- (void)_requestData
{
    DeviceService *service = GET_SERVICE(DeviceService);
    [service getDeviceBySerialNumber:self.serialNumber callback:^(DeviceEntity *deviceEntity, NSError *error) {
        if (!error) {
            self._currentDevice = deviceEntity;
            [self _loadDataForMapView];
            [self _loadDataForTextView];
        }
    }];
}

- (void)_requestLocateDevice
{
    self._requestedTimeStamp = [[NSDate date] timeIntervalSince1970];
    [self._mapView removeAnnotations:self._mapView.annotations];
    [DataService locateDeviceBySerialNumber:self.serialNumber imei:self.imeiNumber callback:^(DeviceLocateContainerModel *model, NSError *error) {
        if (!error && model.isValid) {
            DeviceLocateModel *deviceLocateModel = model.result;
            self._currentTransactionId = deviceLocateModel.transactionId;
            [self _requestCommandStatus:self._currentTransactionId];
        } else {
            [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:error.localizedDescription confirm:^{
                [self _hideActivity];
            }];
        }
    }];
}

- (void)_requestCommandStatus:(NSInteger)transactionId
{
    [DataService getCommandStatusByTransactionId:transactionId callback:^(CommandStatusModel *model, NSError *error) {
        NSTimeInterval currentTimeStamp = [[NSDate date] timeIntervalSince1970];
        double secondDiff = currentTimeStamp - self._requestedTimeStamp;
        if (secondDiff < kRequestSecondsLimit) {
            if (!error && model) {
                if (model.commandStatus == CommandStatus_Pending) {
                    [self _requestCommandStatus:transactionId];
                } else if (model.commandStatus == CommandStatus_Success) {
                    self.lastKnownLatitude = model.latitude;
                    self.lastKnownLongitude = model.longitude;
                    self.lastKnownLocation = model.location;
                    [self _hideActivity];
                    [self _zoomToCoordinate];
                } else if (model.commandStatus == CommandStatus_Failed) {
                    NSError *failedError = [ErrorsUtil errorWithCode:kErrorRequestExpired message:LOCALIZED(@"error_failedtolocate")];
                    [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:failedError.localizedDescription confirm:^{
                        [self _hideActivity];
                    }];
                } else if (model.commandStatus == CommandStatus_Expired) {
                    NSError *expiredError = [ErrorsUtil errorWithCode:kErrorRequestExpired message:LOCALIZED(@"error_locaterequestexpire")];
                    [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:expiredError.localizedDescription confirm:^{
                        [self _hideActivity];
                    }];
                }
            } else {
                [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:error.localizedDescription confirm:^{
                    [self _hideActivity];
                }];
            }
        } else {
            NSError *expiredError = [ErrorsUtil errorWithCode:kErrorRequestExpired message:LOCALIZED(@"error_locaterequestexpire")];
            [AlertViewBlocks confirmWithTitle:LOCALIZED(@"error") message:expiredError.localizedDescription confirm:^{
                [self _hideActivity];
            }];
        }
    }];
}

///--------------------------------------
#pragma mark - <BarButtonsDelegate>
///--------------------------------------

- (UIButton *)setupRightBarButton
{
    return [UITools createBarButtonWithTitle:[LOCALIZED(@"btn_locate") uppercaseString]];
}

- (void)handleRightButtonEvent:(id)sender
{
    [self _showActivity];
    [self _requestLocateDevice];
}

///--------------------------------------
#pragma mark - <MGLMapViewDelegate>
///--------------------------------------

- (MGLAnnotationImage *)mapView:(MGLMapView *)mapView imageForAnnotation:(id <MGLAnnotation>)annotation
{

    if ([annotation isKindOfClass:MapboxPointAnnotation.class]) {
        MapboxPointAnnotation *mapboxAnnotation = (MapboxPointAnnotation *) annotation;
        if (!mapboxAnnotation.willUseImage) {
            return nil;
        }
    }

    MGLAnnotationImage *annotationImage = [mapView dequeueReusableAnnotationImageWithIdentifier:kAnnotationIdentifier];
    if (!annotationImage) {
        UIImage *image = [UIImage imageNamed:@"img-inventory-annotation"];
        image = [image imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, image.size.height / 2, 0)];
        annotationImage = [MGLAnnotationImage annotationImageWithImage:image reuseIdentifier:kAnnotationIdentifier];
    }

    return annotationImage;
}

- (BOOL)mapView:(MGLMapView *)mapView annotationCanShowCallout:(id <MGLAnnotation>)annotation
{
    return YES;
}

- (UIView <MGLCalloutView> *)mapView:(__unused MGLMapView *)mapView calloutViewForAnnotation:(id <MGLAnnotation>)annotation
{
    self._calloutView.representedObject = annotation;
    return self._calloutView;
}

- (void)mapView:(MGLMapView *)mapView tapOnCalloutForAnnotation:(id <MGLAnnotation>)annotation
{
    [mapView deselectAnnotation:annotation animated:YES];
}

@end
