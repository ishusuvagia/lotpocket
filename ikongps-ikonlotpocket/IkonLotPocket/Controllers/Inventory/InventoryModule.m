
#import "InventoryModule.h"
#import "InventoryMainViewController.h"
#import "InventoryListViewController.h"
#import "InventoryDetailsNewViewController.h"

@interface InventoryModule ()

@property (nonatomic, strong) InventoryMainViewController *_controller;

@end

@implementation InventoryModule

- (NSString *)getTitle
{
    return LOCALIZED(@"title_inventory");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-inventory"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-inventory"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-inventory-selected"];
}

- (UIViewController *)getMainViewController
{
       if (!__controller) __controller = INIT_CONTROLLER_XIB(InventoryMainViewController);
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
//    return [LOCALIZED(@"btn_sortfilter") uppercaseString];
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeInventory;
}

- (BOOL)alwaysVisible
{
    return NO;
}

@end
