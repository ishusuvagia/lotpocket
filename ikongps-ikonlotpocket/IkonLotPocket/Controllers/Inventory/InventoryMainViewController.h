
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryMainViewController : BaseViewController <ScrollableTabbarDelegate>
- (void)_addInventoryListView:(NSString *)type;
@end

NS_ASSUME_NONNULL_END
