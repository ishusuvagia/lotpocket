//
//  InventoryDetailsNewViewController.h
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "ChildViewController.h"
#import "SideMenuTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryDetailsNewViewController : ChildViewController
{
    IBOutlet UIScrollView *ImgPagination,*StateScrollView;
    IBOutlet UIPageControl *pageControl;
    BOOL pageControlBeingUsed;
    IBOutlet UIButton *statsBtn,*featuresBtn,*locationBtn,*menuBtn;
    IBOutlet UIImageView *SideMenuBg;
    IBOutlet UIView *SideMenuView,*HeaderView,*footerView;
    IBOutlet UIScrollView *TabbarScroll;
    IBOutlet UIButton *brochureBtn,*carfaxBtn,*salesBtn,*leadsBtn,*engageBtn,*appraisalBtn,*safetyBtn;
    IBOutlet UIImageView *brochureImg,*carfaxImg,*salesImg,*leadsImg,*engageImg,*appraisalImg,*safetyImg;
    IBOutlet UILabel *HeaderStockNoLbl,*ModelNameLbl;

}
@property (nonatomic, strong) NSString *deviceSerialNumber;
@property (strong, nonatomic) IBOutlet UITableView *menuTable;



@end

NS_ASSUME_NONNULL_END
