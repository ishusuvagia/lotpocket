
#import "InventorySortFilterViewController.h"
#import "SingleSelectionViewController.h"
#import "MultipleSelectionViewController.h"
#import "SectionTableView.h"
#import "CheckMarkTableCell.h"
#import "IconLabelTableCell.h"
#import "RangeSliderTableCell.h"
#import "CustomButton.h"

static NSString *const kCheckMarkCellIdentifier = @"checkmarktablecell";
static NSString *const kCheckMarkCellNibName = @"CheckMarkTableCell";
static NSString *const kIconLabelCellIdentifier = @"iconlabeltablecell";
static NSString *const kIconLabelCellNibName = @"IconLabelTableCell";
static NSString *const kRangeSliderCellIdentifer = @"rangeslidertablecell";
static NSString *const kRangeSliderCellNibName = @"RangeSliderTableCell";

static NSString *const kSliderMinumumKey = @"minimum";
static NSString *const kSliderMaximumKey = @"maximum";
static CGFloat const kSliderScale = 1000.0f;

@interface InventorySortFilterViewController () <BarButtonsDelegate, SectionTableViewDelegate, SelectionDelegate, RangeSliderDelegate>

@property (weak, nonatomic) IBOutlet SectionTableView *_tableView;
@property (weak, nonatomic) IBOutlet UIView *_bottomContainer;
@property (weak, nonatomic) IBOutlet CustomButton *_doneBtn;

- (IBAction)_doneBtnClicked:(id)sender;

@end

@implementation InventorySortFilterViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil barButtonsDelegate:self])) {
        _currentSortType = kExternal_SortBy_Model;
        _currentFilterDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [LOCALIZED(@"title_sortfilter") uppercaseString];
    [self._tableView initWithCellPrototypes:self._prototypes
                           hasHeaderSection:YES
                           hasFooterSection:NO
                              enableRefresh:NO
                                enableFetch:NO
                              emptyDataText:kEmptyString
                                   delegate:self];
    if ([Utils stringIsNullOrEmpty:_currentSortType]) {
        _currentSortType = kExternal_SortBy_Model;
    }

    [self _loadData];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[
            @{kCellIdentifierKey: kCheckMarkCellIdentifier, kCellNibNameKey: kCheckMarkCellNibName, kCellClassKey: CheckMarkTableCell.class},
            @{kCellIdentifierKey: kIconLabelCellIdentifier, kCellNibNameKey: kIconLabelCellNibName, kCellClassKey: IconLabelTableCell.class},
            @{kCellIdentifierKey: kRangeSliderCellIdentifer, kCellNibNameKey: kRangeSliderCellNibName, kCellClassKey: RangeSliderTableCell.class},
    ];
}

- (NSArray *)_sortTypes
{
    return @[
            kExternal_SortBy_Model,
            // TODO: The external API has some error in this sorting
            kExternal_SortBy_HighestPrice,
            kExternal_SortBy_LowestPrice,
            kExternal_SortBy_NewestYear,
            kExternal_SortBy_OldestYear,
            kExternal_SortBy_LowestMileage,
            kExternal_SortBy_HighestMileage
    ];
}

- (void)_loadData
{
    NSMutableArray *cellSectionData = [NSMutableArray array];

    ///--------------------------------------
    /// SORT SECTION
    ///--------------------------------------

    NSMutableArray *sortData = [NSMutableArray array];
    for (NSString *sortType in self._sortTypes) {
        [sortData addObject:@{
                kCellIdentifierKey: kCheckMarkCellIdentifier,
                kCellClassKey: CheckMarkTableCell.class,
                kCellInformationKey: sortType,
                kCellHeightKey: @([CheckMarkTableCell height])
        }];
    }

    NSDictionary *sortSection = @{kSectionTitleKey: [NSString stringWithFormat:@"%@:", [LOCALIZED(@"sortby") uppercaseString]], kSectionDataKey: sortData};
    [cellSectionData addObject:sortSection];

    ///--------------------------------------
    /// FILTER SECTION
    ///--------------------------------------

    NSMutableArray *filterData = [NSMutableArray array];
    [filterData addObject:@{
            kCellIdentifierKey: kIconLabelCellIdentifier,
            kCellClassKey: IconLabelTableCell.class,
            kCellTitleKey: LOCALIZED(@"year"),
            kCellObjectDataKey: [BusinessUtils vehicleYears],
            kCellTagKey: kExternal_FilterBy_Year,
            kCellInformationKey: self.currentFilterDict[kExternal_FilterBy_Year] ?: kEmptyString,
            kCellIconImageKey: [UIImage imageNamed:@"icon-calendar"] ?: [UIImage new],
            kCellShowDisclosureIndicatorKey: @(YES),
            kCellHeightKey: @([IconLabelTableCell height])
    }];
    [filterData addObject:@{
            kCellIdentifierKey: kRangeSliderCellIdentifer,
            kCellClassKey: RangeSliderTableCell.class,
            kCellTitleKey: LOCALIZED(@"price"),
            kCellObjectDataKey : @{kSliderMinumumKey: @(kMinimumVehiclePrice), kSliderMaximumKey: @(kMaximumVehiclePrice)},
            kCellTagKey: kExternal_FilterBy_PriceRange,
            kCellInformationKey: self.currentFilterDict[kExternal_FilterBy_PriceRange] ?: kEmptyString,
            kCellIconImageKey: [UIImage imageNamed:@"icon-money"] ?: [UIImage new],
            kCellHeightKey: @([RangeSliderTableCell height])
    }];
    [filterData addObject:@{
            kCellIdentifierKey: kRangeSliderCellIdentifer,
            kCellClassKey: RangeSliderTableCell.class,
            kCellTitleKey: LOCALIZED(@"mileage"),
            kCellObjectDataKey : @{kSliderMinumumKey: @(kMinimumVehicleMileage), kSliderMaximumKey: @(kMaximumVehicleMileage)},
            kCellTagKey: kExternal_FilterBy_MilesRange,
            kCellInformationKey: self.currentFilterDict[kExternal_FilterBy_MilesRange] ?: kEmptyString,
            kCellIconImageKey: [UIImage imageNamed:@"icon-odometer"] ?: [UIImage new],
            kCellHeightKey: @([RangeSliderTableCell height])
    }];
    [filterData addObject:@{
            kCellIdentifierKey: kIconLabelCellIdentifier,
            kCellClassKey: IconLabelTableCell.class,
            kCellTitleKey: LOCALIZED(@"bodystyle"),
            kCellObjectDataKey: [BusinessUtils vehicleBodyStyles],
            kCellTagKey: kExternal_FilterBy_BodyStyle,
            kCellInformationKey: self.currentFilterDict[kExternal_FilterBy_BodyStyle] ?: kEmptyString,
            kCellIconImageKey: [UIImage imageNamed:@"icon-car"] ?: [UIImage new],
            kCellShowDisclosureIndicatorKey: @(YES),
            kCellHeightKey: @([IconLabelTableCell height])
    }];
    [filterData addObject:@{
            kCellIdentifierKey: kIconLabelCellIdentifier,
            kCellClassKey: IconLabelTableCell.class,
            kCellTitleKey: LOCALIZED(@"condition"),
            kCellObjectDataKey: [BusinessUtils vehicleConditions],
            kCellTagKey: kExternal_FilterBy_Condition,
            kCellInformationKey: self.currentFilterDict[kExternal_FilterBy_Condition] ?: kEmptyString,
            kCellIconImageKey: [UIImage imageNamed:@"icon-condition"] ?: [UIImage new],
            kCellShowDisclosureIndicatorKey: @(YES),
            kCellHeightKey: @([IconLabelTableCell height])
    }];
    [filterData addObject:@{
            kCellIdentifierKey: kIconLabelCellIdentifier,
            kCellClassKey: IconLabelTableCell.class,
            kCellTitleKey: LOCALIZED(@"transmission"),
            kCellObjectDataKey: [BusinessUtils vehicleTransmissions],
            kCellTagKey: kExternal_FilterBy_Transmission,
            kCellInformationKey: self.currentFilterDict[kExternal_FilterBy_Transmission] ?: kEmptyString,
            kCellIconImageKey: [UIImage imageNamed:@"icon-transmission"] ?: [UIImage new],
            kCellShowDisclosureIndicatorKey: @(YES),
            kCellHeightKey: @([IconLabelTableCell height])
    }];
    [filterData addObject:@{
            kCellIdentifierKey: kIconLabelCellIdentifier,
            kCellClassKey: IconLabelTableCell.class,
            kCellTitleKey: LOCALIZED(@"fueltype"),
            kCellObjectDataKey: [BusinessUtils vehicleFuelTypes],
            kCellTagKey: kExternal_FilterBy_FuelType,
            kCellInformationKey: self.currentFilterDict[kExternal_FilterBy_FuelType] ?: kEmptyString,
            kCellIconImageKey: [UIImage imageNamed:@"icon-fuel"] ?: [UIImage new],
            kCellShowDisclosureIndicatorKey: @(YES),
            kCellHeightKey: @([IconLabelTableCell height])
    }];

    NSDictionary *filterSection = @{kSectionTitleKey: [NSString stringWithFormat:@"%@:", [LOCALIZED(@"filterby") uppercaseString]], kSectionDataKey: filterData};
    [cellSectionData addObject:filterSection];

    ///--------------------------------------

    [self._tableView loadData:cellSectionData];
}

///--------------------------------------
#pragma mark - Events
///--------------------------------------

- (IBAction)_doneBtnClicked:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(updateSortString:filterDict:)]) {
        [self.delegate updateSortString:self.currentSortType filterDict:self.currentFilterDict];
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

///--------------------------------------
#pragma mark - <BarButtonsDelegate>
///--------------------------------------

- (UIButton *)setupLeftBarButton
{
    return [UITools createBarButtonWithTitle:[LOCALIZED(@"btn_cancel") uppercaseString]];
}

- (UIButton *)setupRightBarButton
{
    return [UITools createBarButtonWithTitle:[LOCALIZED(@"btn_reset") uppercaseString]];
}

- (void)handleLeftButtonEvent:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleRightButtonEvent:(id)sender
{
    _currentSortType = kExternal_SortBy_Model;
    [self.currentFilterDict removeAllObjects];
    [self _loadData];
}

///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------

- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:CheckMarkTableCell.class]) {
        NSString *sortType = cellDict[kCellInformationKey];
        [(CheckMarkTableCell *) cell setupCellWithTitle:LOCALIZED(sortType) colorCode:nil isSelected:[self.currentSortType isEqualToString:sortType]];
    } else if ([cell isKindOfClass:IconLabelTableCell.class]) {
        NSString *title = cellDict[kCellTitleKey];
        UIImage *iconImage = cellDict[kCellIconImageKey];
        NSString *key = cellDict[kCellTagKey];
        BOOL showDisclosure = [cellDict[kCellShowDisclosureIndicatorKey] boolValue];
        [(IconLabelTableCell *) cell setupCellWithTitle:title iconImage:iconImage key:key
                                         showDisclosure:showDisclosure];
    } else if ([cell isKindOfClass:RangeSliderTableCell.class]) {
        NSString *title = cellDict[kCellTitleKey];
        UIImage *iconImage = cellDict[kCellIconImageKey];
        NSString *key = cellDict[kCellTagKey];
        NSDictionary *dict = cellDict[kCellObjectDataKey];
        CGFloat minimum = [dict[kSliderMinumumKey] floatValue]/kSliderScale;
        CGFloat maximum = [dict[kSliderMaximumKey] floatValue]/kSliderScale;
        CGFloat selectedMinimum = minimum;
        CGFloat selectedMaximum = maximum;
        NSString *selectedValue = cellDict[kCellInformationKey];
        if (![Utils stringIsNullOrEmpty:selectedValue]) {
            NSArray *values = [selectedValue componentsSeparatedByString:@"-"];
            if (values.count == 2) {
                selectedMinimum = [values[0] floatValue]/kSliderScale;
                selectedMaximum = [values[1] floatValue]/kSliderScale;
            }
        }
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        if ([key isEqualToString:kExternal_FilterBy_PriceRange])
            formatter.positivePrefix = @"$";
        formatter.positiveSuffix = @"K";
        [(RangeSliderTableCell *)cell setupCellWithTitle:title iconImage:iconImage
                                                 minimum:minimum maximum:maximum
                                         selectedMinimum:selectedMinimum selectedMaximum:selectedMaximum
                                    numberFormatter:formatter key:key delegate:self];
    }
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle
       withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:CheckMarkTableCell.class]) {
        NSString *sortType = cellDict[kCellInformationKey];
        self.currentSortType = sortType;
        [self._tableView reload];
    } else if ([cell isKindOfClass:IconLabelTableCell.class]) {
        if ([cellDict[kCellTagKey] isEqualToString:kExternal_FilterBy_Condition]) {
            SingleSelectionViewController *controller = INIT_CONTROLLER_XIB(SingleSelectionViewController);
            controller.delegate = self;
            controller.key = cellDict[kCellTagKey];
            controller.data = cellDict[kCellObjectDataKey];
            controller.selectedValue = cellDict[kCellInformationKey];
            controller.navTitle = cellDict[kCellTitleKey];
            [self.navigationController pushViewController:controller animated:YES];
        } else {
            MultipleSelectionViewController *controller = INIT_CONTROLLER_XIB(MultipleSelectionViewController);
            controller.delegate = self;
            controller.key = cellDict[kCellTagKey];
            controller.data = cellDict[kCellObjectDataKey];
            controller.selectedData = [[Utils commaSpaceValueToArray:cellDict[kCellInformationKey]] mutableCopy];
            controller.navTitle = cellDict[kCellTitleKey];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

///--------------------------------------
#pragma mark - <SelectionDelegate>
///--------------------------------------

- (void)updateSelectedValue:(NSString *)value forKey:(NSString *)key
{
    if (value != nil) self.currentFilterDict[key] = value;
    [self _loadData];
}

- (void)updateSelectedValues:(NSArray *)values forKey:(NSString *)key
{
    if (values != nil) self.currentFilterDict[key] = [Utils formatCommaSpaceValue:values];
    [self _loadData];
}

///--------------------------------------
#pragma mark - <RangeSliderDelegate>
///--------------------------------------

- (void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(CGFloat)selectedMinimum andMaximumValue:(CGFloat)selectedMaximum key:(NSString *)key
{
    if ([key isEqualToString:kExternal_FilterBy_PriceRange]) {
        NSString *value = [NSString stringWithFormat:@"%.0f-%.0f", selectedMinimum*kSliderScale, selectedMaximum*kSliderScale];
        self.currentFilterDict[kExternal_FilterBy_PriceRange] = value;
    } else if ([key isEqualToString:kExternal_FilterBy_MilesRange]) {
        NSString *value = [NSString stringWithFormat:@"%.0f-%.0f", selectedMinimum*kSliderScale, selectedMaximum*kSliderScale];
        self.currentFilterDict[kExternal_FilterBy_MilesRange] = value;
    }
}

- (void)didEndTouchesInRangeSlider:(TTRangeSlider *)sender
{
    [self _loadData];
}

@end
