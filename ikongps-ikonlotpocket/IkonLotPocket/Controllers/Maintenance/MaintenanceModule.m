//
//  MaintenanceModule.m
//  IkonLotPocket
//
//  Created by BAPS on 20/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "MaintenanceModule.h"
#import "MaintenanceViewController.h"

@interface MaintenanceModule ()

@property (nonatomic, strong) MaintenanceViewController *_controller;

@end

@implementation MaintenanceModule
- (NSString *)getTitle
{
    return LOCALIZED(@"title_maintanance");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-maintenance"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-maintenance"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-maintenance-selected"];
}

- (UIViewController *)getMainViewController
{
    if (!self._controller) {
        __controller = INIT_CONTROLLER_XIB(MaintenanceViewController);
    }
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeMaintenance;
}

- (BOOL)alwaysVisible
{
    return NO;
}

@end
