
#import "NavigationBarViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScrollableTabbarViewController : NavigationBarViewController

- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil moduleType:(ModuleType)moduleType;
- (void)_setupTabbar;
@end

NS_ASSUME_NONNULL_END
