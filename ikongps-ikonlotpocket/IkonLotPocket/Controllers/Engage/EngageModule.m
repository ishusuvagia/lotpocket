
#import "EngageModule.h"
#import "EngageMainViewController.h"

@interface EngageModule ()

@property (nonatomic, strong) EngageMainViewController *_controller;

@end

@implementation EngageModule

- (NSString *)getTitle
{
    return LOCALIZED(@"title_engage");
}

- (UIImage *)getDashboardIcon
{
    return [UIImage imageNamed:@"icon-dashboard-engage"];
}

- (UIImage *)getTabbarIcon
{
    return [UIImage imageNamed:@"icon-tabbar-engage"];
}

- (UIImage *)getTabbarSelectedIcon
{
    return [UIImage imageNamed:@"icon-tabbar-engage-selected"];
}

- (UIViewController *)getMainViewController
{
    if (!self._controller) {
        __controller = INIT_CONTROLLER_XIB(EngageMainViewController);
    }
    return self._controller;
}

- (NSString *)getNavbarButtonTitle
{
    return nil;
}

- (ModuleType)getModuleType
{
    return ModuleTypeEngage;
}

- (BOOL)alwaysVisible
{
    return NO;
}

@end
