//
//  EngageChatView.m
//  IkonLotPocket
//
//  Created by BAPS on 25/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import "EngageChatView.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "NavigationController.h"
#import "ScrollableTabbarViewController.h"

@interface EngageChatView ()<UITextViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>
{
    AppDelegate *app;
    NSMutableArray *chatarray,*Oldchatarray,*ImgArray,*pendingMsg;
    NSString *userid,*msgTYPE,*Base64Str,*msgstr,*isapiRunning;
    int textViewmaxheight;
    CGRect bottomViewFrame,CollectionVIewFrame;
    NSTimer *Get_msg_Timer,*Get_msg_Timer1;
    int padding,msgtvY,indexnumber;
}
@end

@implementation EngageChatView
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    TitleLbl.text=[NSString stringWithFormat:@"%@",self.HeaderName];
    isapiRunning=@"no";
    _R_id =@"1";
    self.colleView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    Oldchatarray =[[NSMutableArray alloc] init];
    chatarray = [[NSMutableArray alloc] init];
    textViewmaxheight = 60;
//    userid=app.UserID;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    msgtvY=_msgTV.frame.origin.y;
    padding =msgtvY+msgtvY;// _BottomView.frame.size.height-_msgTV.frame.size.height;
    msgTYPE=@"msg";
    _BigImg_VIew.hidden=YES;
    UINib *nib = [UINib nibWithNibName:@"sendtextcell" bundle:nil];
    [_colleView registerNib:nib forCellWithReuseIdentifier:@"sendtextcell"];
    UINib *nib1 = [UINib nibWithNibName:@"ReciveTextCell" bundle:nil];
    [_colleView registerNib:nib1 forCellWithReuseIdentifier:@"ReciveTextCell"];
    UINib *nib2 = [UINib nibWithNibName:@"DateCell" bundle:nil];
    [_colleView registerNib:nib2 forCellWithReuseIdentifier:@"DateCell"];
    UINib *nib3 = [UINib nibWithNibName:@"Imagecell" bundle:nil];
    [_colleView registerNib:nib3 forCellWithReuseIdentifier:@"Imagecell"];
    UINib *nib4 = [UINib nibWithNibName:@"MapCell" bundle:nil];
    [_colleView registerNib:nib4 forCellWithReuseIdentifier:@"MapCell"];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.BigImg_VIew addGestureRecognizer:tapRecognizer];
    _colleView.frame=CGRectMake(0,   _Headerview.frame.size.height+5, _colleView.frame.size.width, self.view.frame.size.height-_BottomView.frame.size.height-_colleView.frame.origin.y);
    _msgTV.layer.borderColor=[UIColor grayColor].CGColor;
    _msgTV.layer.borderWidth=1.0;
    _msgTV.layer.cornerRadius=5.0;
    _msgTV.clipsToBounds=YES;
    _msgTV.delegate=self;
//    [self SetArrayValue];
    bottomViewFrame=_BottomView.frame;
    CollectionVIewFrame=_colleView.frame;
    _name_lbl.text=_name_lblSTr;
    [_msgTV setText:@"Message..."];
    [_msgTV setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
    [_msgTV setTextColor:[UIColor lightGrayColor]];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [_BigImg_VIew addGestureRecognizer:swipeleft];
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [_BigImg_VIew addGestureRecognizer:swiperight];
    _cantsendMSGLBL.hidden=YES;
    if([_IsAdmin isEqualToString:@"1"])
    {
        _cantsendMSGLBL.hidden=NO;
    }
    else
    {
        _cantsendMSGLBL.hidden=YES;
    }
}
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"swipeleft");
    indexnumber++;
    NSLog(@"index number is ++  %d",indexnumber);
    if (indexnumber <ImgArray.count) {
        [self setimage_in_imageview];
    }
    else
    {
        indexnumber--;
    }
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"swiperight");
    indexnumber--;
    NSLog(@"index number is --  %d",indexnumber);
    if (indexnumber >= 0) {
        [self setimage_in_imageview];
    }
    else
    {
        indexnumber++;
    }
}
-(void)setimage_in_imageview
{
    //    NSURL *imageurl=[NSURL URLWithString:[ImgArray objectAtIndex:indexnumber]];
    NSString *imageurl=[NSString stringWithFormat:@"%@",[ImgArray objectAtIndex:indexnumber]];
    _Big_img.image = [self decodeBase64ToImage:imageurl];
    if (_Big_img.image == nil)
    {
//        [_Big_img sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"w_load.png"]];
        [_Big_img setImage:[UIImage imageNamed:@"w_load.png"]];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//    Get_msg_Timer1=[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(Get_Msg_api_Call) userInfo:nil repeats:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [Get_msg_Timer invalidate];
    [Get_msg_Timer1 invalidate];
}
-(void) keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    if ([_msgTV.text isEqual:@"Message"])
    {
        _msgTV.text = @"";
        _msgTV.textColor = [UIColor blackColor];
    }
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
    CGRect containerFrame = _BottomView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    // set views with new info
    _BottomView.frame = containerFrame;
    _colleView.frame=CGRectMake(_colleView.frame.origin.x,   _Headerview.frame.size.height, _colleView.frame.size.width,_BottomView.frame.origin.y-_Headerview.frame.size.height);
    [_colleView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (chatarray.count !=0)
        {
            int i = chatarray.count-1;
            [_colleView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
        }
    });
    bottomViewFrame=_BottomView.frame;
    CollectionVIewFrame=_colleView.frame;
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    if(_msgTV.text.length == 0)
    {
        [_msgTV setText:@"Message"];
        [_msgTV setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
        [_msgTV setTextColor:[UIColor lightGrayColor]];
    }
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // get a rect for the textView frame
    CGRect containerFrame = _BottomView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    // set views with new info
    _BottomView.frame = containerFrame;
    _colleView.frame=CGRectMake(_colleView.frame.origin.x,  _Headerview.frame.size.height, _colleView.frame.size.width, self.view.frame.size.height-_BottomView.frame.size.height-_Headerview.frame.size.height);
    // commit animations
    [UIView commitAnimations];
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
    _BigImg_VIew.hidden=YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([textView.text isEqualToString:@""]) {
//        _colleView.frame=CollectionVIewFrame;
//        _BottomView.frame=bottomViewFrame;
    }
    if (range.length==1 && text.length==0)
    {
        NSLog(@"backspace tapped");
        int textviewY= _BottomView.frame.origin.y;
        int textviewframe= _BottomView.frame.size.height;//-_BottomView.frame.origin.y;
/*        if (textView.frame.size.height <= textViewmaxheight)
        {
            CGFloat fixedWidth = textView.frame.size.width;
            CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
            CGRect newFrame = textView.frame;
            newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
            textView.frame = newFrame;
            int newtextviewframeheight= (textView.frame.size.height+padding);
            int finalheight=newtextviewframeheight-textviewframe;
            int finalY=textviewY-finalheight;
            _BottomView.frame=CGRectMake(_BottomView.frame.origin.x, finalY, _BottomView.frame.size.width, textView.frame.size.height+padding);
            _colleView.frame=CGRectMake(_colleView.frame.origin.x,   _Headerview.frame.size.height, _colleView.frame.size.width,_BottomView.frame.origin.y-_Headerview.frame.size.height);
            int height=_BottomView.frame.size.height-(2*msgtvY);
            _msgTV.frame=CGRectMake(_msgTV.frame.origin.x, msgtvY, _msgTV.frame.size.width, height);
        }*/
        [self Load_to_bottom];
        return YES;
    }
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
//    int textviewY= _BottomView.frame.origin.y;
//    int textviewframe= _BottomView.frame.size.height;//-_BottomView.frame.origin.y;
    if (textView.frame.size.height <= textViewmaxheight)
    {
/*        CGFloat fixedWidth = textView.frame.size.width;
        CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = textView.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        textView.frame = newFrame;
        //        [textView sizeToFit];
        int newtextviewframeheight= (textView.frame.size.height+padding);
        int finalheight=newtextviewframeheight-textviewframe;
        int finalY=textviewY-finalheight;
        _BottomView.frame=CGRectMake(_BottomView.frame.origin.x, finalY, _BottomView.frame.size.width, textView.frame.size.height+padding);
        _colleView.frame=CGRectMake(_colleView.frame.origin.x,   _Headerview.frame.size.height, _colleView.frame.size.width,_BottomView.frame.origin.y-_Headerview.frame.size.height);
        int height=_BottomView.frame.size.height-(2*msgtvY);
        _msgTV.frame=CGRectMake(_msgTV.frame.origin.x, msgtvY, _msgTV.frame.size.width, height);*/
        [self Load_to_bottom];
    }
}
/*-(void)OpenCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *) kUTTypeImage, nil];
    [self presentViewController:picker animated:YES completion:NULL];
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    //    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    [self presentViewController:picker animated:YES completion:nil];
}
-(void)OpenGallery
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    [self presentViewController:picker animated:YES completion:NULL];
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    //    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    [self presentViewController:picker animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    NSData *imageData = UIImageJPEGRepresentation(chosenImage, 1.0);
    Base64Str = [imageData base64EncodedStringWithOptions:kNilOptions];
    msgTYPE=@"img";
    [picker dismissViewControllerAnimated:YES completion:nil];
    if (![Base64Str isEqualToString:@""])
    {
        msgstr=Base64Str;
//        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Send_Msg_api_Call) userInfo:nil repeats:NO];
        NSMutableDictionary *dir1 = [[NSMutableDictionary alloc]init];
        [dir1 setObject:_msgTV.text forKey:@"msg"];
        [dir1 setObject:msgTYPE forKey:@"type"];
//        [dir1 setObject:app.UserID forKey:@"sender_id"];
        [dir1 setObject:_R_id forKey:@"receiver_id"];
        [dir1 setObject:@"0 sec ago" forKey:@"chat_duration"];
        [chatarray addObject:dir1];
        [self Load_to_bottom];
/*                NSDate * now = [NSDate date];
                NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
                [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *CurrentDate = [outputFormatter stringFromDate:now];
                [database open];
                NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO chat (msg, type,  sender_id , receiver_id ,date, dt ) VALUES ('%@', '%@', '%@', '%@', '%@','%@' )",msgstr,msgTYPE,app.UserID,_R_id,@"0 sec ago",CurrentDate];
                [database executeUpdate:insertQuery];
                [database close];
                [self Get_Data_to_DB ];
    }
}
-(void)OpenActionShit
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Choose Picture" preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        [self OpenCamera];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose Existing Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // OK button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        [self OpenGallery];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return chatarray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reciveruserid=[NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"receiver_id"] objectAtIndex:indexPath.row]];
    NSString *celltypestr=[NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"type"] objectAtIndex:indexPath.row]];
    if ([celltypestr isEqualToString:@"dt"]) {
        static NSString *identifier = @"DateCell";
        DateCell *cell = (DateCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        return cell;
    }
    if ([reciveruserid isEqualToString:userid])
    {
        if([celltypestr isEqualToString:@"msg"])
        {
            static NSString *identifier = @"ReciveTextCell";
            ReciveTextCell *cell = (ReciveTextCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            NSString *text = [NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row]];
            CGFloat width = self.view.frame.size.width-75;
            UIFont *font = [UIFont systemFontOfSize:16];
            NSAttributedString *attributedText =
            [[NSAttributedString alloc]
             initWithString:text
             attributes:@
             {
             NSFontAttributeName: font
             }];
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            CGSize size = rect.size;
            cell.msglbl.text=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
            cell.chatbubble.image=[[UIImage imageNamed:@"Whiterecevebubble.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            cell.msglbl.frame=CGRectMake(20, cell.msglbl.frame.origin.y, cell.msglbl.frame.size.width,cell.msglbl.frame.size.height);
            cell.mainview.frame=CGRectMake(0, 5, size.width+50, cell.frame.size.height-25);
            cell.TimeLBL.text=[[chatarray valueForKey:@"chat_duration"] objectAtIndex:indexPath.row];
            return cell;
        }
        else
        {
            static NSString *identifier = @"Imagecell";
            Imagecell *cell = (Imagecell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            cell.TIMELBL2.hidden=YES;
            cell.TimeLBL.hidden=NO;
            cell.TIMELBL2.text=[[chatarray valueForKey:@"chat_duration"] objectAtIndex:indexPath.row];
            cell.TimeLBL.text=[[chatarray valueForKey:@"chat_duration"] objectAtIndex:indexPath.row];
            cell.mainview.frame=CGRectMake(5, 0, 100, 100);
            cell.mainview.layer.cornerRadius=10;
            cell.mainview.layer.masksToBounds=YES;
            if ([celltypestr isEqualToString:@"img"]) {
                NSString *imageurl=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
//                [cell.Imagevw sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"b_load.png"]];
                [cell.Imagevw setImage:[UIImage imageNamed:@"b_load.png"]];
            }
            else
            {
                NSString *imageurl=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
//                [cell.Imagevw sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"b_load.png"]];
                [cell.Imagevw setImage:[UIImage imageNamed:@"b_load.png"]];
            }
            return cell;
        }
    }
    else //if([issender isEqualToString:@"yes"])
    {
        if([celltypestr isEqualToString:@"msg"])
        {
            static NSString *identifier = @"sendtextcell";
            sendtextcell *cell = (sendtextcell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            NSString *text = [NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row]];
            CGFloat width = self.view.frame.size.width-75;
            UIFont *font = [UIFont systemFontOfSize:16];
            NSAttributedString *attributedText =
            [[NSAttributedString alloc]
             initWithString:text
             attributes:@
             {
             NSFontAttributeName: font
             }];
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            CGSize size = rect.size;
            
            cell.msglbl.text=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
            cell.msglbl.textColor=[UIColor whiteColor];
            cell.chatbubble.image=[[UIImage imageNamed:@"SEND_bubble.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
            cell.msglbl.frame=CGRectMake(20, cell.msglbl.frame.origin.y, cell.msglbl.frame.size.width,cell.msglbl.frame.size.height);
            int xset =self.view.frame.size.width;
            cell.mainview.frame=CGRectMake(xset-(size.width +50),5,size.width+50,cell.frame.size.height-25);
            cell.TimeLBL.text=[[chatarray valueForKey:@"chat_duration"] objectAtIndex:indexPath.row];
            return cell;
        }
        else
        {
            static NSString *identifier = @"Imagecell";
            Imagecell *cell = (Imagecell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            int xset =self.view.frame.size.width;
            cell.mainview.frame=CGRectMake(xset-105,0,100, 100);
            cell.mainview.layer.cornerRadius=10;
            cell.mainview.layer.masksToBounds=YES;
            if ([celltypestr isEqualToString:@"img"])
            {
                NSString *imageurl=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
                //                [cell.Imagevw sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"b_load.png"]];
                cell.Imagevw.image = [self decodeBase64ToImage:imageurl];
                if (cell.Imagevw.image == nil)
                {
//                    [cell.Imagevw sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"b_load.png"]];
                    [cell.Imagevw setImage:[UIImage imageNamed:@"b_load.png"]];
                }
            }
            else
            {
                NSString *imageurl=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
//                [cell.Imagevw sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"b_load.png"]];
                [cell.Imagevw setImage:[UIImage imageNamed:@"b_load.png"]];
            }
            cell.TimeLBL.hidden=YES;
            cell.TIMELBL2.hidden=NO;
            cell.TIMELBL2.text=[[chatarray valueForKey:@"chat_duration"] objectAtIndex:indexPath.row];
            cell.TimeLBL.text=[[chatarray valueForKey:@"chat_duration"] objectAtIndex:indexPath.row];
            return cell;
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
/*        NSString *celltypestr=[celltype objectAtIndex:indexPath.row];
        NSString *isdate=[issenderArry objectAtIndex:indexPath.row];
    
        if([isdate isEqualToString:@"dt"])
        {
            return CGSizeMake(self.view.frame.size.width  , 30);
        }
    
        if([celltypestr isEqualToString:@"img"])
        {
            return CGSizeMake(self.view.frame.size.width  , 120);
        }
    
        NSString *text = [NSString stringWithFormat:@"%@",[msgarray objectAtIndex:indexPath.row]];
        CGFloat width = self.view.frame.size.width-75;
        UIFont *font = [UIFont systemFontOfSize:16];
        NSAttributedString *attributedText =
        [[NSAttributedString alloc]
         initWithString:text
         attributes:@
         {
            NSFontAttributeName: font
         }];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize size = rect.size;
        return CGSizeMake(self.view.frame.size.width  , size.height+50);*/
    NSString *reciveruserid=[NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"receiver_id"] objectAtIndex:indexPath.row]];
    NSString *celltypestr=[NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"type"] objectAtIndex:indexPath.row]];
    if ([celltypestr isEqualToString:@"dt"])
    {
        return CGSizeMake(self.view.frame.size.width  , 30);
    }
    if (![reciveruserid isEqualToString:userid])
    {
        if([celltypestr isEqualToString:@"msg"])
        {
            NSString *text = [NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row]];
            CGFloat width = self.view.frame.size.width-75;
            UIFont *font = [UIFont systemFontOfSize:16];
            NSAttributedString *attributedText =
            [[NSAttributedString alloc]
             initWithString:text
             attributes:@
             {
             NSFontAttributeName: font
             }];
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            CGSize size = rect.size;
            return CGSizeMake(self.view.frame.size.width  , size.height+50);
        }
        else
        {
            return CGSizeMake(self.view.frame.size.width  , 120);
        }
    }
    else //if([issender isEqualToString:@"yes"])
    {
        if([celltypestr isEqualToString:@"msg"])
        {
            NSString *text = [NSString stringWithFormat:@"%@",[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row]];
            CGFloat width = self.view.frame.size.width-75;
            UIFont *font = [UIFont systemFontOfSize:16];
            NSAttributedString *attributedText =
            [[NSAttributedString alloc]
             initWithString:text
             attributes:@
             {
             NSFontAttributeName: font
             }];
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            CGSize size = rect.size;
            return CGSizeMake(self.view.frame.size.width  , size.height+50);
        }
        else
        {
            return CGSizeMake(self.view.frame.size.width  , 120);
        }
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *celltypestr=[[chatarray valueForKey:@"type"] objectAtIndex:indexPath.row];
    if ([celltypestr isEqualToString:@"img"])
    {
        NSString *imageurl=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
        for (int i=0; i<ImgArray.count; i++) {
            if([imageurl isEqualToString:[ImgArray objectAtIndex:i]])
            {
                indexnumber=i;
                _BigImg_VIew.hidden=NO;
                NSString *imageurl=[ImgArray objectAtIndex:i];
                _Big_img.image = [self decodeBase64ToImage:imageurl];
                if (_Big_img.image == nil)
                {
//                    [_Big_img sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"w_load.png"]];
                    [_Big_img setImage:[UIImage imageNamed:@"w_load.png"]];
                }
                //                [_Big_img sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"w_load.png"]];
            }
        }
/*                indexnumber=indexPath.row;
                [self setimage_in_imageview];
                _BigImg_VIew.hidden=NO;
                NSString *imageurl=[[chatarray valueForKey:@"msg"] objectAtIndex:indexPath.row];
                [_Big_img sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"w_load.png"]];*/
    }
}
- (IBAction)Close_btn_Click:(id)sender
{
    indexnumber=0;
    _BigImg_VIew.hidden=YES;
}
-(void)Load_to_bottom
{
    if(chatarray.count != 0)
    {
        //        if (chatarray.count != Oldchatarray.count)
        if ( Oldchatarray.count < chatarray.count)
        {
            Oldchatarray = [chatarray mutableCopy];
            [_colleView reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                int i = chatarray.count-1;
                [_colleView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
            });
        }
        else
        {
            //            [_colleView reloadData];
        }
    }
//     [_colleView reloadData];
}
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

- (IBAction)Send_btn_click:(id)sender
{
    if (![_msgTV.text isEqualToString:@""] || [msgTYPE isEqualToString:@"img"])
    {
        msgstr=_msgTV.text;
//        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Send_Msg_api_Call) userInfo:nil repeats:NO];
        NSMutableDictionary *dir1 = [[NSMutableDictionary alloc]init];
        [dir1 setObject:_msgTV.text forKey:@"msg"];
        [dir1 setObject:msgTYPE forKey:@"type"];
//        [dir1 setObject:app.UserID forKey:@"sender_id"];
        [dir1 setObject:_R_id forKey:@"receiver_id"];
        [dir1 setObject:@"0 sec ago" forKey:@"chat_duration"];
        [chatarray addObject:dir1];
        [self Load_to_bottom];
/*                NSDate * now = [NSDate date];
                NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
                [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *CurrentDate = [outputFormatter stringFromDate:now];
        
                [database open];
                NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO chat (msg, type,  sender_id , receiver_id ,date, dt) VALUES ('%@', '%@', '%@', '%@', '%@' , '%@')",msgstr,msgTYPE,app.UserID,_R_id,@"0 sec ago",CurrentDate];
                [database executeUpdate:insertQuery];
                [database close];
                [self Get_Data_to_DB ];*/
        _msgTV.text=@"";
/*        CGFloat fixedWidth = _msgTV.frame.size.width;
        CGSize newSize = [_msgTV sizeThatFits:CGSizeMake(fixedWidth-50, MAXFLOAT)];
        CGRect newFrame = _msgTV.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        _msgTV.frame = newFrame;
        _colleView.frame=CollectionVIewFrame;
        _BottomView.frame=bottomViewFrame;
        int height=_BottomView.frame.size.height-(2*msgtvY);
        _msgTV.frame=CGRectMake(_msgTV.frame.origin.x, msgtvY, _msgTV.frame.size.width, height);*/
    }
}
/*-(void)Get_Msg_api_Call
{
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Get_Msg_api_Call) userInfo:nil repeats:NO];
                                       
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        [alert addAction:OkButton];
        [alert addAction:canclebtn];
        
        [self presentViewController:alert animated:YES completion:nil];
        [self Hud_hide];
        
    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSDate * now = [NSDate date];
            NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
            [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *CurrentDate = [outputFormatter stringFromDate:now];
            
            NSString *URLString =[NSString stringWithFormat:@"%@receive_msg.php",app.BASEURL];
            NSString *parameters = [NSString stringWithFormat:@"{\"sender_id\":\"%@\",\"receiver_id\":\"%@\",\"dt\":\"%@\"}",app.UserID,_R_id,CurrentDate];
            
            jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
            NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:parameters];
            NSLog(@"===================================================================");
            NSLog(@"%@",URLString);
            NSLog(@"%@",parameters);
            NSLog(@"%@",jsonDictionary);
            NSLog(@"===================================================================");
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                NSString *success=[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"success"]];
                if ([success isEqualToString:@"1"])
                {
                    
                    //                    NSMutableArray *demo=[[jsonDictionary valueForKey:@"data"] mutableCopy];
                    //                    for (int i=0; i<demo.count; i++)
                    //                    {
                    //                        NSString *msg=[[demo valueForKey:@"msg"]objectAtIndex:i];
                    //                        NSString *sender_id=[[demo valueForKey:@"sender_id"]objectAtIndex:i];
                    //                        NSString *type=[[demo valueForKey:@"type"]objectAtIndex:i];
                    //                        NSString *receiver_id=[[demo valueForKey:@"receiver_id"]objectAtIndex:i];
                    //                        NSString *chat_duration=[[demo valueForKey:@"chat_duration"]objectAtIndex:i];
                    //                        NSString *dt=[[demo valueForKey:@"dt"]objectAtIndex:i];
                    //
                    //                        [database open];
                    //                        NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO chat (msg, type,  sender_id , receiver_id ,date ,dt ,issend) VALUES ('%@', '%@', '%@', '%@', '%@' ,'%@' ,'1' )", msg,type,sender_id,receiver_id,chat_duration,dt];
                    //                        [database executeUpdate:insertQuery];
                    //                        [database close];
                    //                    }
                    //                        [self Get_Data_to_DB ];
                    
                    
                    chatarray = [[NSMutableArray alloc] init];
                    ImgArray = [[NSMutableArray alloc] init];
                    NSMutableArray *demo=[[NSMutableArray alloc] init];
                    NSMutableArray * recchatarray = [[jsonDictionary valueForKey:@"data"] mutableCopy];
                    for (int i =0; i<recchatarray.count; i++)
                    {
                        if (i==0)
                        {
                            NSString *currentdate=[[recchatarray valueForKey:@"dt"]objectAtIndex:i];
                            currentdate= [currentdate substringToIndex:10];
                            
                            NSMutableDictionary *dir1 = [[NSMutableDictionary alloc]init];
                            [dir1 setObject:@"dt" forKey:@"type"];
                            [dir1 setObject:currentdate forKey:@"date"];
                            [demo addObject:dir1];
                        }
                        else
                        {
                            int k=i-1;
                            NSString *Predate=[[recchatarray valueForKey:@"dt"]objectAtIndex:k];
                            NSString *currentdate1=[[recchatarray valueForKey:@"dt"]objectAtIndex:i];
                            Predate= [Predate substringToIndex:10];
                            currentdate1= [currentdate1 substringToIndex:10];
                            
                            if (![Predate isEqualToString:currentdate1])
                            {
                                NSMutableDictionary *dir1 = [[NSMutableDictionary alloc]init];
                                [dir1 setObject:@"dt" forKey:@"type"];
                                [dir1 setObject:currentdate1 forKey:@"date"];
                                [demo addObject:dir1];
                            }
                        }
                        NSMutableDictionary *dir1 = [[NSMutableDictionary alloc]init];
                        [dir1 setObject:[[recchatarray valueForKey:@"msg"]objectAtIndex:i] forKey:@"msg"];
                        [dir1 setObject:[[recchatarray valueForKey:@"type"]objectAtIndex:i] forKey:@"type"];
                        [dir1 setObject:[[recchatarray valueForKey:@"sender_id"]objectAtIndex:i] forKey:@"sender_id"];
                        [dir1 setObject:[[recchatarray valueForKey:@"receiver_id"]objectAtIndex:i] forKey:@"receiver_id"];
                        [dir1 setObject:[[recchatarray valueForKey:@"chat_duration"]objectAtIndex:i] forKey:@"chat_duration"];
                        [demo addObject:dir1];
                        //                        if([[[recchatarray valueForKey:@"type"]objectAtIndex:i] isEqualToString:@"img"])
                        //                        {
                        //                            [ImgArray addObject:[[recchatarray valueForKey:@"msg"]objectAtIndex:i]];
                        //                        }
                    }
                    if ( chatarray.count < demo.count)
                    {
                        chatarray=[demo mutableCopy];
                        for (int i =0; i<recchatarray.count; i++)
                        {
                            if([[[recchatarray valueForKey:@"type"]objectAtIndex:i] isEqualToString:@"img"])
                            {
                                [ImgArray addObject:[[recchatarray valueForKey:@"msg"]objectAtIndex:i]];
                            }
                        }
                        [self Load_to_bottom ];
                    }
                }
                else
                {
                }
                [self Hud_hide];
                
            });
        });
    }
}
-(void)Send_Msg_api_Call
{
    Reachability* reachability;
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Internet"
                                     message:@"Check your internet connection."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:@"Retry"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Send_Msg_api_Call) userInfo:nil repeats:NO];
                                   }];
        UIAlertAction* canclebtn = [UIAlertAction
                                    actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    { }];
        [alert addAction:OkButton];
        [alert addAction:canclebtn];
        
        [self presentViewController:alert animated:YES completion:nil];
        [self Hud_hide];
    }
    else
    {
        isapiRunning=@"yes";
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
            NSString *MsgText;
            NSDate * now = [NSDate date];
            NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
            [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *CurrentDate = [outputFormatter stringFromDate:now];
            
            if ([msgTYPE isEqualToString:@"msg"])
            {
                MsgText=msgstr;
            }
            else
            {
                MsgText=Base64Str;
            }
            
            //                MsgText=[[pendingMsg valueForKey:@"msg"] objectAtIndex:i];
            //                msgTYPE=[[pendingMsg valueForKey:@"type"] objectAtIndex:i];
            NSString * givenid=@"0";
            
            NSString *URLString =[NSString stringWithFormat:@"%@send_msg.php",app.BASEURL];
            NSString *parameters = [NSString stringWithFormat:@"{\"sender_id\":\"%@\",\"receiver_id\":\"%@\",\"type\":\"%@\",\"msg\":\"%@\",\"dt\":\"%@\",\"id\":\"%@\"}",app.UserID,_R_id,msgTYPE,MsgText,CurrentDate,givenid];
            
            msgstr=@"";
            msgTYPE=@"msg";
            
            jsonServiceNSObjectCall = [[ServiceNSObject alloc]init];
            NSDictionary *jsonDictionary =[jsonServiceNSObjectCall JsonPostServiceCall:URLString PostTagSet:parameters];
            NSLog(@"===================================================================");
            NSLog(@"%@",URLString);
            NSLog(@"%@",parameters);
            NSLog(@"%@",jsonDictionary);
            NSLog(@"===================================================================");
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                NSString *success=[NSString stringWithFormat:@"%@",[jsonDictionary valueForKey:@"success"]];
                if ([success isEqualToString:@"1"])
                {
                    //                        [database open];
                    //                        NSString *givenuserid=[[jsonDictionary valueForKey:@"data"] valueForKey:@"given_id"];
                    //
                    //                        NSString *insertQuery = [NSString stringWithFormat:@"UPDATE chat SET issend = '1' WHERE id = %@ ",givenuserid];
                    //                        [database executeUpdate:insertQuery];
                    //                        [database close];
                    
                }
                else
                {
                }
                [self Hud_hide];
                isapiRunning=@"no";
            });
        });
    }
}*/
- (IBAction)Back_Btn_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)Choose_photo_btn_Click:(id)sender {
//    [self.view endEditing:YES];
//    [self OpenActionShit];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Face for Perform Dance step"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera", @"Select from Library", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}

#pragma mark -
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    int i = buttonIndex;
    switch(i)
    {
        case 0:
        {
            UIImagePickerController * picker = [[UIImagePickerController alloc] init] ;
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:^{}];
        }
            break;
        case 1:
        {
            UIImagePickerController * picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:^{}];
        }
        default:
            // Do Nothing.........
            break;
    }
}
#pragma mark -
#pragma - mark Selecting Image from Camera and Library
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    NSData *imageData = UIImageJPEGRepresentation(chosenImage, 1.0);
    Base64Str = [imageData base64EncodedStringWithOptions:kNilOptions];
    msgTYPE=@"img";
    [picker dismissViewControllerAnimated:YES completion:nil];
    if (![Base64Str isEqualToString:@""])
    {
        msgstr=Base64Str;
        //        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(Send_Msg_api_Call) userInfo:nil repeats:NO];
        NSMutableDictionary *dir1 = [[NSMutableDictionary alloc]init];
        [dir1 setObject:_msgTV.text forKey:@"msg"];
        [dir1 setObject:msgTYPE forKey:@"type"];
        //        [dir1 setObject:app.UserID forKey:@"sender_id"];
        [dir1 setObject:_R_id forKey:@"receiver_id"];
        [dir1 setObject:@"0 sec ago" forKey:@"chat_duration"];
        [chatarray addObject:dir1];
        [self Load_to_bottom];
        /*                NSDate * now = [NSDate date];
         NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
         [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
         NSString *CurrentDate = [outputFormatter stringFromDate:now];
         [database open];
         NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO chat (msg, type,  sender_id , receiver_id ,date, dt ) VALUES ('%@', '%@', '%@', '%@', '%@','%@' )",msgstr,msgTYPE,app.UserID,_R_id,@"0 sec ago",CurrentDate];
         [database executeUpdate:insertQuery];
         [database close];
         [self Get_Data_to_DB ];*/
    }
    
}
-(IBAction)BackBtnPressed:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}
@end
