
#import "BaseViewController.h"
#import "NavigationBarViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface EngageMainViewController : NavigationBarViewController<ScrollableTabbarDelegate>
{
    IBOutlet UIImageView *imageV,*bgimage,*check;
    IBOutlet UIButton *playbtn,*sendMailBtn;
    IBOutlet UIView *tapview,*leadsView,*mediaClickView;
    IBOutlet UITextField *Search_friend_tf;
    IBOutlet UILabel *TotalCountLbl;
    IBOutlet UIButton *dashboardBtn,*leadsBtn,*accountsBtn,*tasksBtn,*mediaBtn;
}
@property (strong, nonatomic) NSURL *videoURL;
@property (strong, nonatomic) MPMoviePlayerController *videoController;
@property (strong, nonatomic) IBOutlet UITableView *LeadsTable;
@end

NS_ASSUME_NONNULL_END
