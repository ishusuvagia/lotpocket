//
//  EngageChatView.h
//  IkonLotPocket
//
//  Created by BAPS on 25/05/19.
//  Copyright © 2019 IkonGPS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sendtextcell.h"
#import "DateCell.h"
#import "ReciveTextCell.h"
#import "Imagecell.h"
#import "MapCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface EngageChatView : UIViewController
{
    IBOutlet UILabel *TitleLbl;
}
@property (weak, nonatomic) IBOutlet UICollectionView *colleView;
@property (weak, nonatomic) IBOutlet UIView *BigImg_VIew;
- (IBAction)Close_btn_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *Big_img;
@property (weak, nonatomic) IBOutlet UIView *BottomView;
@property (weak, nonatomic) IBOutlet UITextView *msgTV;
@property (weak, nonatomic) IBOutlet UIView *Headerview;
- (IBAction)Send_btn_click:(id)sender;

@property (strong, nonatomic) NSString *R_id;
- (IBAction)Back_Btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *name_lbl;
@property (strong, nonatomic) NSString *name_lblSTr;
- (IBAction)Choose_photo_btn_Click:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *cantsendMSGLBL;

@property (strong, nonatomic) NSString *IsAdmin;
@property(strong,nonatomic)NSString *HeaderName;


@end

NS_ASSUME_NONNULL_END
