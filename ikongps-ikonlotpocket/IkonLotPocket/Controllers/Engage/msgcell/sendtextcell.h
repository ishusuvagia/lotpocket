//
//  sendtextcell.h
//  ChatDemo
//
//  Created by iSquare infoTech on 01/02/19.
//  Copyright © 2019 Isquare Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sendtextcell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *msglbl;
@property (weak, nonatomic) IBOutlet UILabel *TimeLBL;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIImageView *chatbubble;

@end
