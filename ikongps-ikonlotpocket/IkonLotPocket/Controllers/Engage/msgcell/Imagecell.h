//
//  Imagecell.h
//  ChatDemo
//
//  Created by iSquare infoTech on 01/02/19.
//  Copyright © 2019 Isquare Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Imagecell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *TimeLBL;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIImageView *Imagevw;
@property (weak, nonatomic) IBOutlet UILabel *TIMELBL2;

@end
