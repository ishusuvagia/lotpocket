//
//  LocationCell.h
//  ChatDemo
//
//  Created by iSquare infoTech on 01/02/19.
//  Copyright © 2019 Isquare Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LocationCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet MKMapView *MapView;
@property (weak, nonatomic) IBOutlet UILabel *TimeLBL;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UIButton *Click_Btn;

@end
