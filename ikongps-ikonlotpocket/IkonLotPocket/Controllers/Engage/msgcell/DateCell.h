//
//  DateCell.h
//  ChatDemo
//
//  Created by iSquare infoTech on 01/02/19.
//  Copyright © 2019 Isquare Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *datelbl;

@end
