//
//  MapCell.h
//  ChatDemo
//
//  Created by iSquare infoTech on 01/02/19.
//  Copyright © 2019 Isquare Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface MapCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *TimeLBL;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (weak, nonatomic) IBOutlet UILabel *TIMELBL2;
@end
