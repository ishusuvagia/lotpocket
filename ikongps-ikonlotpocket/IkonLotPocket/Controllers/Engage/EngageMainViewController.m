
#import "EngageMainViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SectionTableView.h"
#import "EnagageListTableViewCell.h"
#import "NavigationController.h"
#import <HMSegmentedControl/HMSegmentedControl.h>
#import "AppDelegate.h"
#import "EngageChatView.h"


static NSString *const kCellIdentifier = @"EnagageListTableViewCell";
static NSString *const kCellNibName = @"EnagageListTableViewCell";
static CGFloat const kCellSpacing = 5.0f;

@interface EngageMainViewController () <UINavigationControllerDelegate,UIImagePickerControllerDelegate, SectionTableViewDelegate, UITableViewDelegate, RestResponseDelegate>
{
    AppDelegate *app;
    NSMutableArray *SideMenuTitile,*SideMenuImg;
}

@property (weak, nonatomic) IBOutlet SectionTableView *tblList;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *segmentControl;

@property (nonatomic, strong) NSString *_currentSortType;
@property (nonatomic, strong) NSMutableDictionary *_currentFilterDict;

@end

@implementation EngageMainViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __currentSortType = kExternal_SortBy_Model;
        __currentFilterDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    SideMenuTitile=[[NSMutableArray alloc]initWithObjects:
                    @"MS Dhoni",@"Kohli",@"shikhar D",
                    @"KL Rahul",@"Hardik Pandya",@"UV Singh",
                    @"DJ bravo",@"Russel", nil];
    SideMenuImg=[[NSMutableArray alloc]initWithObjects:
                 @"icon-tabbar-inventory",@"icon-tabbar-engage",@"icon-tabbar-salestool",
                 @"icon-tabbar-vehiclelot",@"icon-tabbar-map",@"icon-tabbar-guard",
                 @"icon-tabbar-maintenance",@"icon-tabbar-alert", nil];
    
    if([app.ChatStatusStr isEqualToString:@"on"])
    {
        app.ChatStatusStr=@"off";
        [leadsBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
         leadsView.hidden=NO;
        
    }
    else
    {
        [dashboardBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
        leadsView.hidden=YES;
    }
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(Search_friend_tf.frame.size.width+60,-2, 15, 15)];
    imgView.image = [UIImage imageNamed:@"icon-search"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [paddingView addSubview:imgView];
    [Search_friend_tf setLeftViewMode:UITextFieldViewModeAlways];
    [Search_friend_tf setLeftView:paddingView];
    Search_friend_tf.layer.cornerRadius = 2;
    Search_friend_tf.clipsToBounds=YES;
    Search_friend_tf.backgroundColor =[AppColorScheme white];
    
    [self.tblList initWithCellPrototypes:self._prototypes
                           hasHeaderSection:YES
                           hasFooterSection:NO
                              enableRefresh:YES
                                enableFetch:NO
                              emptyDataText:LOCALIZED(@"no_availableitems")
                                   delegate:self];
    self.tblList.tableViewProxyDelegate = self;
    self.tblList.contentInset = UIEdgeInsetsMake(kCellSpacing, 0.0f, 0.0f, 0.0f);
    
    self.segmentControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Dashboard", @"Leads", @"Accounts",@"Task"]];
    [self.segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
   
    
    NSLog(@"engageLoad");
    [self responseHandler];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[@{kCellIdentifierKey: kCellIdentifier, kCellNibNameKey: kCellNibName, kCellClassKey: EnagageListTableViewCell.class}];
}

- (void)_updateTitle:(NSInteger)items
{
    NSString *title = @"Leads";
    
    UIViewController *moduleRootController = self.parentViewController.parentViewController;
    if ([moduleRootController isKindOfClass:NavigationBarViewController.class]) {
        [(NavigationBarViewController *) moduleRootController addTitle:title andSubtitle:@""];
    }
}

///--------------------------------------
#pragma mark - <RestResponseDelegate>
///--------------------------------------

- (void)responseHandler
{
    [self endLoading:self.tblList];
    [self.tblList reloadData];
    
    [self _updateTitle:15];
    
    NSMutableArray *cellSectionData = [NSMutableArray array];
    NSDictionary *dict = @{
                           kCellIdentifierKey: kCellIdentifier,
                           kCellClassKey: EnagageListTableViewCell.class,
                           kCellHeightKey: @([EnagageListTableViewCell height]),
                           kCellObjectDataKey: @""
                           };
    NSDictionary *section = @{kSectionTitleKey: kEmptyString, kSectionDataKey: @[dict]};
    [cellSectionData addObject:section];
    [self.tblList loadData:cellSectionData];
}

///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------

- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *title, *description, *imageUrl =nil;
    title = @"Eileen Asaro";
    description = @"Referral through current customer";
    imageUrl = @"";



    [(EnagageListTableViewCell *) cell setupCellWithTitle:title description:description imageUrl:@""];
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
}

///--------------------------------------
#pragma mark - <Segment Controll Method>
///--------------------------------------

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld", (long)segmentedControl.selectedSegmentIndex);
}

-(IBAction)captureNewMediaBtnPressed:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *) kUTTypeImage, nil];
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.image"])
    {
        UIImage *capturedImg=info[UIImagePickerControllerOriginalImage];
        imageV.image=capturedImg;
        [picker dismissViewControllerAnimated:YES completion:NULL];
        bgimage.hidden=YES;
        playbtn.hidden=YES;
    }
    else
    {
        _videoURL = info[UIImagePickerControllerMediaURL];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        AVAsset *asset = [AVAsset assetWithURL:_videoURL];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        CMTime time = CMTimeMake(1, 1);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        imageV.image=thumbnail;
        bgimage.hidden=NO;
        playbtn.hidden=NO;
    }
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    recognizer.minimumPressDuration = .5;
    recognizer.delegate = self;
    [tapview addGestureRecognizer:recognizer];
}

- (void)move:(UILongPressGestureRecognizer *)sender
{
    check.hidden=NO;
    sendMailBtn.hidden=NO;
}
-(IBAction)btnplayPressed:(id)sender
{
    AVPlayerViewController * _moviePlayer1 = [[AVPlayerViewController alloc] init];
    _moviePlayer1.player = [AVPlayer playerWithURL:_videoURL];
    [self presentViewController:_moviePlayer1 animated:YES completion:^{
        [_moviePlayer1.player play];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(IBAction)sendMailbtnPressed:(id)sender
{
    NSString *recipients = @"mailto:skrutik87@gmail.com?subject=Spartan Dealer";
    NSString *body = [NSString stringWithFormat:@"&body=%@",[NSURL URLWithString:@"https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"]];
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    sendMailBtn.hidden=YES;
    check.hidden=YES;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(IBAction)backBrnPressed:(id)sender
{
    NSLog(@"backbtnpressed in crm");
//    [self.navigationController popViewControllerAnimated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMain bundle:nil];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    UIViewController *ec = [storyboard instantiateViewControllerWithIdentifier:kSceneLanding];;
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:ec];
    window.rootViewController = navigationController;
    [window makeKeyAndVisible];
}

///--------------------------------------
#pragma mark - <side menu>
///--------------------------------------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [SideMenuTitile count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EnagageListTableViewCell * cell = [_LeadsTable dequeueReusableCellWithIdentifier:@"myCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"EnagageListTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [_LeadsTable dequeueReusableCellWithIdentifier:@"myCell"];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(EnagageListTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblName.text = [SideMenuTitile objectAtIndex:indexPath.row];
    cell.lblDescription.text = [NSString stringWithFormat:@"%@ description",[SideMenuTitile objectAtIndex:indexPath.row]];
    
    NSString *ImgName=[NSString stringWithFormat:@"%@",[SideMenuImg objectAtIndex:indexPath.row]];
    cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.height/2;
    cell.imgProfile.clipsToBounds=YES;
    cell.imgProfile.image=[UIImage imageNamed:@"dc12"];
    
    
    
    cell.imgIndicator.image=[UIImage imageNamed:@"btn-go"];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Title=[NSString stringWithFormat:@"%@", [SideMenuTitile objectAtIndex:indexPath.row]];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMain bundle:nil];
//    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
//    UIViewController *ec = [storyboard instantiateViewControllerWithIdentifier:@"chatVC"];
//    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:ec];
//    window.rootViewController = navigationController;
//    [window makeKeyAndVisible];
    EngageChatView *chatVC=[[EngageChatView alloc]init];
    chatVC.HeaderName=Title;
    [self.navigationController presentViewController:chatVC animated:YES completion:nil];
}
-(IBAction)DashboardBtnPressed:(id)sender
{
    [dashboardBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
    [leadsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [accountsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [tasksBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [mediaBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    leadsView.hidden=YES;
    mediaClickView.hidden=YES;
}
-(IBAction)LeadBtnPressed:(id)sender
{
    [leadsBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
    [dashboardBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [accountsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [tasksBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [mediaBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    leadsView.hidden=NO;
    mediaClickView.hidden=YES;
}
-(IBAction)AccountBtnPressed:(id)sender
{
    [accountsBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
    [leadsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [dashboardBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [tasksBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [mediaBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    leadsView.hidden=YES;
    mediaClickView.hidden=YES;
}
-(IBAction)TasksBtnPressed:(id)sender
{
    [tasksBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
    [dashboardBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [accountsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [leadsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [mediaBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    leadsView.hidden=YES;
    mediaClickView.hidden=YES;
}
-(IBAction)MediaBtnPressed:(id)sender
{
    [mediaBtn setBackgroundColor:[UIColor colorWithRed:239/255.0f green:64/255.0f blue:35/255.0f alpha:1]];
    [dashboardBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [accountsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [leadsBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    [tasksBtn setBackgroundColor:[UIColor colorWithRed:97/255.0f green:101/255.0f blue:112/255.0f alpha:1]];//gray
    leadsView.hidden=YES;
    mediaClickView.hidden=NO;
}

@end
