
#import "NavigationController.h"
#import "Logger.h"
#import "AppColorScheme.h"

@implementation NavigationController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self _initialize];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        [self _initialize];
    }
    return self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    TRACE("Segue Id: %@ - Destination: %@",
            segue.identifier, segue.destinationViewController);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self _setBottomBorderColor:[AppColorScheme brandLightBlue] height:2.0f];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)dealloc
{
//    TRACE_HERE;
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (void)_initialize
{
    self.interactivePopGestureRecognizer.enabled = YES;
}

- (void)_setBottomBorderColor:(UIColor *)color height:(CGFloat)height
{
    CGRect bottomBorderRect = CGRectMake(0, CGRectGetHeight(self.navigationBar.frame)-height,
            CGRectGetWidth([UIScreen mainScreen].bounds), height);
    UIView *bottomBorder = [[UIView alloc] initWithFrame:bottomBorderRect];
    [bottomBorder setBackgroundColor:color];
    [self.navigationBar addSubview:bottomBorder];
}

@end
