
#import <ChameleonFramework/Chameleon.h>
#import "NavigationBarViewController.h"
#import "Utils.h"
#import "AppColorScheme.h"
#import "AppColor.h"
#import "AppFont.h"

@interface NavigationBarViewController ()

@property (nonatomic, strong) UIButton *_leftBarBtn;
@property (nonatomic, strong) UIButton *_rightBarBtn;
@property (nonatomic, weak) id <BarButtonsDelegate> _barButtonsDelegate;

@end

@implementation NavigationBarViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithBarButtonsDelegate:(id <BarButtonsDelegate>)delegate
{
    if ((self = [super init])) {
        __barButtonsDelegate = delegate;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder barButtonsDelegate:(id <BarButtonsDelegate>)delegate
{
    if ((self = [super initWithCoder:aDecoder])) {
        __barButtonsDelegate = delegate;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil barButtonsDelegate:(id <BarButtonsDelegate>)delegate
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        __barButtonsDelegate = delegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Config left bar button
    if ([self._barButtonsDelegate respondsToSelector:@selector(setupLeftBarButton)] && !self._leftBarBtn) {
        __leftBarBtn = [self._barButtonsDelegate setupLeftBarButton];
        if (self._leftBarBtn) {
            [self._leftBarBtn addTarget:self action:@selector(handleLeftButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self._leftBarBtn];
        }
    }

    // Config right bar button
    if ([self._barButtonsDelegate respondsToSelector:@selector(setupRightBarButton)] && !self._rightBarBtn) {
        __rightBarBtn = [self._barButtonsDelegate setupRightBarButton];
        if (self._rightBarBtn) {
            [self._rightBarBtn addTarget:self action:@selector(handleRightButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self._rightBarBtn];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewDidAppear:animated];
}

- (void)dealloc
{
    // TRACE_HERE;
    self._leftBarBtn = nil;
    self._rightBarBtn = nil;
}

///--------------------------------------
#pragma mark - Public
///--------------------------------------

- (void)handleLeftButtonEvent:(id)sender
{
    // Implement at subclass
}

- (void)handleRightButtonEvent:(id)sender
{
    // Implement at subclass
}

- (void)setLeftBarButton:(UIButton *)button
{
//    self._leftBarBtn = button;
//    [self._leftBarBtn addTarget:self action:@selector(handleLeftButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self._leftBarBtn];
}

- (void)setLeftBarButtonTitle:(NSString *)text
{
//    [self._leftBarBtn setTitle:text forState:UIControlStateNormal];
}

- (void)setLeftBarButtonImage:(UIImage *)image
{
//    [self._leftBarBtn setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)setRightBarButton:(UIButton *)button
{
//    self._rightBarBtn = button;
//    [self._rightBarBtn addTarget:self action:@selector(handleRightButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self._rightBarBtn];
}

- (void)setRightBarButtonTitle:(NSString *)text
{
//    [self._rightBarBtn setTitle:text forState:UIControlStateNormal];
}

- (void)setRightBarButtonImage:(UIImage *)image
{
//    [self._rightBarBtn setImage:image forState:UIControlStateNormal];
}

- (void)setEnableLeftBarButton:(BOOL)enabled
{
//    self._leftBarBtn.enabled = enabled;
}

- (void)setEnableRightBarButton:(BOOL)enabled
{
//    self._rightBarBtn.enabled = enabled;
}

- (void)addTitle:(NSString *)title andSubtitle:(NSString *)subtitle
{
    if (![Utils stringIsNullOrEmpty:subtitle]) {
        
        // Make sure the current navigation title is empty string
        self.title = kEmptyString;
        
        // Title
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, -5.0f, 0.0f, 0.0f)];
        titleLbl.backgroundColor = [AppColorScheme clear];
        titleLbl.textColor = [AppColorScheme brandWhite];
        titleLbl.font = [AppFont navigationBarTitleFont];
        titleLbl.text = ![Utils stringIsNullOrEmpty:title] ? title : @"-";
        titleLbl.text = [titleLbl.text uppercaseString];
        [titleLbl sizeToFit];
        
        // SubTitle
        UILabel *subtitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 17.0f, 0.0f, 0.0f)];
        subtitleLbl.backgroundColor = [AppColorScheme clear];
        subtitleLbl.textColor = [AppColorScheme brandWhite];
        subtitleLbl.font = [AppFont navigationBarSubtitleFont];
        subtitleLbl.text = ![Utils stringIsNullOrEmpty:subtitle] ? subtitle : @"-";
        [subtitleLbl sizeToFit];
        
        // Container
        UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subtitleLbl.frame.size.width, titleLbl.frame.size.width), 30)];
        [twoLineTitleView addSubview:titleLbl];
        [twoLineTitleView addSubview:subtitleLbl];
        
        CGFloat widthDiff = subtitleLbl.frame.size.width - titleLbl.frame.size.width;
        if (widthDiff > 0) {
            CGRect frame = titleLbl.frame;
            frame.origin.x = widthDiff / 2;
            titleLbl.frame = CGRectIntegral(frame);
        } else {
            CGRect frame = subtitleLbl.frame;
            frame.origin.x = fabs(widthDiff) / 2;
            subtitleLbl.frame = CGRectIntegral(frame);
        }
        self.navigationItem.titleView = twoLineTitleView;
        
    } else {
        
        self.title = [title uppercaseString];
    }
}

- (void)setNavigationBarCustomColor:(UIColor *)color
{
    if (color != [AppColorScheme clear]) {
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor colorWithContrastingBlackOrWhiteColorOn:color isFlat:YES];
        [self.navigationController.navigationBar setTitleTextAttributes:@{
                NSForegroundColorAttributeName: [UIColor colorWithContrastingBlackOrWhiteColorOn:color isFlat:YES],
                NSFontAttributeName: [AppFont navigationBarTitleFont]
        }];
    }

}

- (void)resetDefaultNavigationBarColor
{
    self.navigationController.navigationBar.barTintColor = [AppColor navigationBarBackgroundColor];
    self.navigationController.navigationBar.tintColor = [AppColor navigationBarTintColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
            NSForegroundColorAttributeName: [AppColor navigationBarTintColor],
            NSFontAttributeName: [AppFont navigationBarTitleFont]
    }];
}

@end
