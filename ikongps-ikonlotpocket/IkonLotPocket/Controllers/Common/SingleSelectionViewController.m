
#import "SingleSelectionViewController.h"
#import "SectionTableView.h"
#import "CheckMarkTableCell.h"

static NSString *const kCellIdentifier = @"checkmarktablecell";
static NSString *const kCellNibName = @"CheckMarkTableCell";

@interface SingleSelectionViewController () <SectionTableViewDelegate>

@property (weak, nonatomic) IBOutlet SectionTableView *_tableView;

@end

@implementation SingleSelectionViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        _data = @[];
        _selectedValue = kEmptyString;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [self.navTitle uppercaseString];
    [self._tableView initWithCellPrototypes:self._prototypes
                           hasHeaderSection:NO
                           hasFooterSection:NO
                              enableRefresh:NO
                                enableFetch:NO
                              emptyDataText:kEmptyString
                                   delegate:self];

    [self _loadData];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[@{kCellIdentifierKey: kCellIdentifier, kCellNibNameKey: kCellNibName, kCellClassKey: CheckMarkTableCell.class}];
}

- (void)_loadData
{
    NSMutableArray *cellSectionData = [NSMutableArray array];
    NSMutableArray *cellData = [NSMutableArray array];
    for (NSString *value in self.data) {
        [cellData addObject:@{
                kCellIdentifierKey: kCellIdentifier,
                kCellClassKey: CheckMarkTableCell.class,
                kCellInformationKey: value,
                kCellHeightKey: @([CheckMarkTableCell height])
        }];
    }
    NSDictionary *filterSection = @{kSectionTitleKey: kEmptyString, kSectionDataKey: cellData};
    [cellSectionData addObject:filterSection];

    [self._tableView loadData:cellSectionData];
}

///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------

- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *value = cellDict[kCellInformationKey];
    [(CheckMarkTableCell *) cell setupCellWithTitle:LOCALIZED(value) colorCode:nil isSelected:[self.selectedValue isEqualToString:value]];
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle
       withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *value = cellDict[kCellInformationKey];
    self.selectedValue = value;
    [self._tableView reload];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (UIButton *)setupRightBarButton
{
    return [UITools createBarButtonWithTitle:[LOCALIZED(@"btn_reset") uppercaseString]];
}

- (void)handleLeftButtonEvent:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(updateSelectedValue:forKey:)]) {
        [self.delegate updateSelectedValue:self.selectedValue forKey:self.key];
    }
    [super handleLeftButtonEvent:sender];
}

- (void)handleRightButtonEvent:(id)sender
{
    self.selectedValue = kEmptyString;
    [self._tableView reload];
}

@end
