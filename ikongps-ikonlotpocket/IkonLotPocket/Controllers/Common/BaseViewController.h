
#import <UIKit/UIKit.h>
#import <AlertViewBlocks/AlertViewBlocks.h>

NS_ASSUME_NONNULL_BEGIN;

@interface BaseViewController : UIViewController

@property (nonatomic, assign) BOOL isModal;

- (BOOL)isVisible;

@end

NS_ASSUME_NONNULL_END;

