
#import "FormViewController.h"

@implementation FormViewController

- (void)resignAllTextFields
{
    // No Implementation. Will be implemented at subclass.
}

- (void)clearPasswordField
{
    // No Implementation. Will be implemented at subclass.
}

- (void)disableForm
{
    // No Implementation. Will be implemented at subclass.
}

- (void)enableForm
{
    // No Implementation. Will be implemented at subclass.
}

@end
