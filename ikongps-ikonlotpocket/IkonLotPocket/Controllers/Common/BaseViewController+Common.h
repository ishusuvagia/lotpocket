
#import "BaseViewController.h"

@class SectionTableView;

@interface BaseViewController (Common)

- (void)startLoading:(nullable SectionTableView *)tableView loadingText:(nullable NSString *)text;
- (void)endLoading:(nullable SectionTableView *)tableView;
- (void)endLoading:(nullable SectionTableView *)tableView error:(nullable NSError *)error;

@end
