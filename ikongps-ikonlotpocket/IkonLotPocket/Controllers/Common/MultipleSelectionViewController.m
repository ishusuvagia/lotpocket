
#import "MultipleSelectionViewController.h"
#import "SectionTableView.h"
#import "CheckMarkTableCell.h"

static NSString *const kCellIdentifier = @"checkmarktablecell";
static NSString *const kCellNibName = @"CheckMarkTableCell";

@interface MultipleSelectionViewController () <SectionTableViewDelegate>

@property (weak, nonatomic) IBOutlet SectionTableView *_tableView;

@end

@implementation MultipleSelectionViewController

///--------------------------------------
#pragma mark - Life Cycle
///--------------------------------------

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        _data = @[];
        _selectedData = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [self.navTitle uppercaseString];
    [self._tableView initWithCellPrototypes:self._prototypes
                           hasHeaderSection:NO
                           hasFooterSection:NO
                              enableRefresh:NO
                                enableFetch:NO
                              emptyDataText:kEmptyString
                                   delegate:self];

    [self _loadData];
}

///--------------------------------------
#pragma mark - Private
///--------------------------------------

- (NSArray *)_prototypes
{
    return @[@{kCellIdentifierKey: kCellIdentifier, kCellNibNameKey: kCellNibName, kCellClassKey: CheckMarkTableCell.class}];
}

- (void)_loadData
{
    NSMutableArray *cellSectionData = [NSMutableArray array];
    NSMutableArray *cellData = [NSMutableArray array];
    for (NSString *value in self.data) {
        [cellData addObject:@{
                kCellIdentifierKey: kCellIdentifier,
                kCellClassKey: CheckMarkTableCell.class,
                kCellInformationKey: value,
                kCellHeightKey: @([CheckMarkTableCell height])
        }];
    }
    NSDictionary *filterSection = @{kSectionTitleKey: kEmptyString, kSectionDataKey: cellData};
    [cellSectionData addObject:filterSection];

    [self._tableView loadData:cellSectionData];
}

///--------------------------------------
#pragma mark - <SectionTableViewDelegate>
///--------------------------------------

- (void)setupCell:(UITableViewCell *)cell withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *value = cellDict[kCellInformationKey];
    BOOL isSelected = [self.selectedData containsObject:value];
    [(CheckMarkTableCell *) cell setupCellWithTitle:LOCALIZED(value) colorCode:nil isSelected:isSelected];
}

- (void)didSelectCell:(UITableViewCell *)cell sectionTitle:(NSString *)sectionTitle
       withDictionary:(NSDictionary *)cellDict atIndexPath:(NSIndexPath *)indexPath
{
    NSString *value = cellDict[kCellInformationKey];
    if ([self.selectedData containsObject:value]) {
        [self.selectedData removeObject:value];
    } else {
        [self.selectedData addObject:value];
    }
    [self._tableView reload];
}

///--------------------------------------
#pragma mark - Overrides
///--------------------------------------

- (UIButton *)setupRightBarButton
{
    return [UITools createBarButtonWithTitle:[LOCALIZED(@"btn_reset") uppercaseString]];
}

- (void)handleLeftButtonEvent:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(updateSelectedValues:forKey:)]) {
        [self.delegate updateSelectedValues:self.selectedData forKey:self.key];
    }
    [super handleLeftButtonEvent:sender];
}

- (void)handleRightButtonEvent:(id)sender
{
    [self.selectedData removeAllObjects];
    [self._tableView reload];
}

@end
