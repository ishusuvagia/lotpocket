
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FormViewController : UITableViewController

@property (nonatomic, weak) id<FormDelegate> delegate;

- (void)resignAllTextFields;
- (void)clearPasswordField;
- (void)disableForm;
- (void)enableForm;

@end

NS_ASSUME_NONNULL_END
