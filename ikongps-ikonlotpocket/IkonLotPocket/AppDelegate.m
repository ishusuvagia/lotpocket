
#import "AppDelegate.h"
#import "ThemeManager.h"
#import "PersistenceService.h"
#import "NavigationController.h"
#import <NMAKit/NMAKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
        [NSThread sleepForTimeInterval:2.0];
    // Logging
    TRACE("%@", @"/* -------------------------------------------------------- */");
    TRACE("/* %@", DEVICE.description);
    TRACE("/* %@", APPLICATION.description);
    TRACE("/* Model: < %@ >", DEVICE.detailedModel);
    TRACE("/* Home directory: %@", HOMEDIRECTORY);
    TRACE("%@", @"/* -------------------------------------------------------- */");
    
    // Initializations
    [ThemeManager setDefaultTheme];
    [PersistenceService configLocalDb];
    [NMAApplicationContext setAppId:@"IryUp2gombCGoPBM4KW3" appCode:@"ygDfHahwp_qaSd7hgF8xpw" licenseKey:kEmptyString];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMain bundle:nil];
    NSString *username = [Utils getObjectFromUserDefaults:kPrefUsernameKey];
    NSString *password = [Utils getObjectFromUserDefaults:kPrefPasswordKey];
    if (![Utils stringIsNullOrEmpty:username] && ![Utils stringIsNullOrEmpty:password]) {
        [AUTHMANAGER loginWithUsername:username password:password callback:^(BOOL success, NSError *error) {
            UIViewController *initialViewController = nil;
            if (success) {
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:kSceneLanding];
                initialViewController = [[NavigationController alloc] initWithRootViewController:viewController];
            } else {
                initialViewController = [storyboard instantiateViewControllerWithIdentifier:kSceneLogin];
            }
            self.window.rootViewController = initialViewController;
            [self.window makeKeyAndVisible];
        }];
    } else {
        UIViewController *initialViewController = [storyboard instantiateViewControllerWithIdentifier:kSceneLogin];
        self.window.rootViewController = initialViewController;
        [self.window makeKeyAndVisible];
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
